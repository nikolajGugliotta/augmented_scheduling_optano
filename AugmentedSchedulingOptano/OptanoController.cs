﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AugmentedSchedulingOptano.Library.DAO;
using AugmentedSchedulingOptano.Library.Models;
using AugmentedSchedulingOptano.Library.ModelTools;
namespace optano.control
{
    class OptanoController
    {
        static void Main(string[] args)
        {
            DataObject dO = new DataObject();
            Console.WriteLine("Welcome to Augmented Scheduling Optano edition.");
          //  Console.WriteLine("s for Simon or n for Nikolaj (nw for nikolaj windows ).");
            string pathInuse = "nwin";
            Tuple<string, string, string, DataObject> nameNdate;
            Console.WriteLine("Press 1 to start execution.");
            var input = Console.ReadLine();
            if (input == "1")
            {
                ////  TestCaseBuilder testCase = new TestCaseBuilder();
                //  Tuple<List<TimeSheetObject>, List<EmployeeObject>> data = testCase.CreateTestCase();
                TimeSheetDAO ti = new TimeSheetDAO(dO, pathInuse);
                nameNdate = ti.RunThis();


                ModelBuilder model = new ModelBuilder();
                model.StartOptano(nameNdate.Item4, nameNdate.Item2);

            }
            else
            {
                Console.WriteLine("Wrong!");
            }

            Console.ReadLine();

        }


    }
}