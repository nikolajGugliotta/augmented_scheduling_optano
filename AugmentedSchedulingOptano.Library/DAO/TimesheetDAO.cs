﻿
using AugmentedSchedulingOptano.Library.Models;
using AugmentedSchedulingOptano.Library.OldTools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace AugmentedSchedulingOptano.Library.DAO
{
    public class TimeSheetDAO
    {
        readonly ToolsOld tool;
        readonly string pathInUse;

        DataObject theDataObject;
        DataTable dataTableTwo, dataTableThree, dataTableFour, dataTableFive, dataTableSix;

        string startRange, endRange;

        public TimeSheetDAO(DataObject dataObject, string pathInUse)
        {
            tool = new ToolsOld();
            theDataObject = dataObject;
            this.pathInUse = pathInUse;
        }

        public Tuple<string, string, string, DataObject> RunThis()
        {

            string stagingName;
            Console.WriteLine("Timesheet generation:");
            Console.WriteLine("Please enter staging name ie \"42raw\": ");

            stagingName = Console.ReadLine();
            theDataObject.PortalName = stagingName;

            Console.WriteLine("Thanks your input is: " + stagingName);

            string connectionString = tool.GenerateConnectionString(stagingName);

            Console.WriteLine("Connected.");

            Tuple<string, DataObject> depNDo = ConnectToDatabase(connectionString, stagingName);
            EmployeeDAO ei = new EmployeeDAO(this.pathInUse, theDataObject);

            theDataObject = ei.RunThis(stagingName, depNDo.Item1, connectionString);
            AddAvailabilityInformationToEmployeeObjects();
            AddVacationInformationToEmployeeObjects();
            CreateShiftRequestObjects();

            FixDaySetRange();
            tool.FixAvailabilities(theDataObject.EmployeeObjects, theDataObject.PortalName, startRange, endRange);
            tool.FixPriorities(theDataObject.EmployeeObjects, theDataObject.PortalName);
            tool.FixNomialHours(theDataObject.EmployeeObjects, theDataObject.PortalName, theDataObject.TimesheetObjects);

            Tuple<string, string, string, DataObject> returning = new Tuple<string, string, string, DataObject>(stagingName, startRange, endRange, theDataObject);

            return returning;
        }

        public Tuple<string, string, string, DataObject> RunThisQuick()
        {
            string stagingName;
            Console.WriteLine("Timesheet generation:");
            Console.WriteLine("Please enter staging name ie \"42raw\": ");

            stagingName = "viktoriagade";
            theDataObject.PortalName = stagingName;

            Console.WriteLine("Thanks your input is: " + stagingName);

            string connectionString = tool.GenerateConnectionString(stagingName);

            Console.WriteLine("Connected.");

            Tuple<string, DataObject> depNDo = ConnectToDatabaseQuick(connectionString, stagingName);
            EmployeeDAO ei = new EmployeeDAO(this.pathInUse, theDataObject);

            theDataObject = ei.RunThis(stagingName, depNDo.Item1, connectionString);
            AddAvailabilityInformationToEmployeeObjects();
            AddVacationInformationToEmployeeObjects();
            CreateShiftRequestObjects();

            FixDaySetRange();
            tool.FixAvailabilities(theDataObject.EmployeeObjects, theDataObject.PortalName, startRange, endRange);
            tool.FixPriorities(theDataObject.EmployeeObjects, theDataObject.PortalName);
            tool.FixNomialHours(theDataObject.EmployeeObjects, theDataObject.PortalName, theDataObject.TimesheetObjects);

            Tuple<string, string, string, DataObject> returning = new Tuple<string, string, string, DataObject>(stagingName, startRange, endRange, theDataObject);

            return returning;
        }

        internal void FixDaySetRange()
        {

            //List<DateTime> datesInRange = theDataObject.SetObjects.TrueDaySet;

            DateTime trueStartDayIncludingHistoric = theDataObject.SetObjects.TrueDaySet[0];


            if (!trueStartDayIncludingHistoric.DayOfWeek.Equals(DayOfWeek.Monday))
            {
                while (!trueStartDayIncludingHistoric.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    trueStartDayIncludingHistoric = trueStartDayIncludingHistoric.AddDays(-1);
                }
            }

            for (DateTime date = trueStartDayIncludingHistoric; date < DateTime.Parse(endRange); date = date.AddDays(1))
            {
                if (!theDataObject.SetObjects.TrueDaySet.Contains(date))
                {
                    theDataObject.SetObjects.TrueDaySet.Add(date);
                    theDataObject.SetObjects.DaySet.Add("d_" + date.Year + "_" + date.Month + "_" + date.Day);
                }
            }
            theDataObject.SetObjects.TrueDaySet.Sort();
            theDataObject.SetObjects.DaySet.Sort();
            for (int i = 0; i < theDataObject.SetObjects.TrueDaySet.Count; i++)
            {
                if (theDataObject.SetObjects.TrueDaySet[i].Date.Equals(DateTime.Parse(startRange)))
                {
                    theDataObject.FirstPlanDay = (i + 1).ToString();
                    break;
                }
            }

            theDataObject.FirstPlanWeek = ((int.Parse(theDataObject.FirstPlanDay) - 1) / 7 + 1).ToString();

            theDataObject.DayCount = theDataObject.SetObjects.TrueDaySet.Count;

        }



        internal Tuple<string, DataObject> ConnectToDatabase(string connectionString, string stagingName)
        {
            Tuple<string, DataObject> toBeReturned;
            using (SqlConnection conn = new SqlConnection())
            {
                Console.WriteLine("Connecting to database using connectionstring:");
                Console.WriteLine("\"" + connectionString + "\"");

                conn.ConnectionString = connectionString;

                conn.Open();

                int portalID = 0;
                portalID = tool.SQLFindPortalID(stagingName, conn);
                if (portalID == 0)
                {
                    Console.WriteLine("Wrong ID (0) returned. Terminating..");
                    Environment.Exit(0);
                }
                Console.WriteLine("Portal ID: " + portalID);
                ArrayList activeDepartments = new ArrayList();
                ArrayList departmentNames = new ArrayList();
                ArrayList fallBackSalary = new ArrayList();

                Tuple<ArrayList, ArrayList, ArrayList> info = tool.GetAllDepartmentsOfPortalFromDataBase(portalID, conn);

                activeDepartments = info.Item1;
                departmentNames = info.Item2;
                fallBackSalary = info.Item3;

                bool hasWorkRules = GetUnionRulesFromDataBase(conn, portalID);

                toBeReturned = GetShiftsInDepartmentFromDataBase(activeDepartments, departmentNames, fallBackSalary, conn, portalID, hasWorkRules);
            }
            return toBeReturned;
        }

        internal Tuple<string, DataObject> ConnectToDatabaseQuick(string connectionString, string stagingName)
        {
            Tuple<string, DataObject> toBeReturned;
            using (SqlConnection conn = new SqlConnection())
            {
                Console.WriteLine("Connecting to database using connectionstring:");
                Console.WriteLine("\"" + connectionString + "\"");

                conn.ConnectionString = connectionString;
                conn.Open();

                int portalID = 0;
                portalID = tool.SQLFindPortalID(stagingName, conn);
                if (portalID == 0)
                {
                    Console.WriteLine("Wrong ID (0) returned. Terminating..");
                    Environment.Exit(0);
                }
                Console.WriteLine("Portal ID: " + portalID);
                ArrayList activeDepartments = new ArrayList();
                ArrayList departmentNames = new ArrayList();
                ArrayList fallBackSalary = new ArrayList();
                Tuple<ArrayList, ArrayList, ArrayList> info = tool.GetAllDepartmentsOfPortalFromDataBase(portalID, conn);

                activeDepartments = info.Item1;
                departmentNames = info.Item2;
                fallBackSalary = info.Item3;

                bool hasWorkRules = GetUnionRulesFromDataBase(conn, portalID);

                toBeReturned = SQLgetShiftsForDepartmentInRangeQuick(activeDepartments, departmentNames, fallBackSalary, conn, portalID, hasWorkRules);
            }
            return toBeReturned;
        }

        internal bool GetUnionRulesFromDataBase(SqlConnection conn, int portalID)
        {
            SqlCommand command = new SqlCommand(
                "select Title, TypeEnum, Value, TimeConditionValue, TimeConditionTypeEnum, NormalTimesheetTypeExcluded, DaysEnum " +
                "from [DutySchedule.UnionRule] " +
                "where PortalId like '" + portalID + "'", conn);
            bool hasWorkRules = true;

            var dataTable = tool.CreateDataTable(command);

            DataCSVExportUnion(dataTable);
            CreateUnionRuleObjects(dataTable);

            return hasWorkRules;
        }


        internal void CreateUnionRuleObjects(DataTable dataTable)
        {
            string title = "";
            string typeEnum = "";
            string val = "";
            string timeConVal = "";
            string timeConTypeEn = "";
            string timesheetTypeExcluded = "";
            string daysEnum = "";

            foreach (DataRow dataRow in dataTable.Rows)
            {
                timesheetTypeExcluded = dataRow.ItemArray[5].ToString();

                if (!timesheetTypeExcluded.Equals("True"))
                {
                    title = dataRow.ItemArray[0].ToString();
                    typeEnum = dataRow.ItemArray[1].ToString();
                    val = dataRow.ItemArray[2].ToString();
                    timeConVal = dataRow.ItemArray[3].ToString();
                    timeConTypeEn = dataRow.ItemArray[4].ToString();
                    daysEnum = dataRow.ItemArray[6].ToString();

                    UnionRuleObject newRule = new UnionRuleObject()
                    {
                        Title = title,
                        TypeEnum = typeEnum,
                        Value = val,
                        TimeConditionValue = timeConVal,
                        TimeConditionTypeEnum = timeConTypeEn,
                        UsedForScalars = false,
                        DaysEnum = daysEnum
                    };

                    theDataObject.UnionRuleObjects.Add(newRule);
                }
            }
        }

        internal void CreateBreakObjects()
        {
            string id = "";
            string title = "";
            string description = "";
            int dutyLength;
            int pauseLength;
            int weekDaysEnum;
            int secondaryWeekDaysEnum;
            int typeEnum;
            int pauseStart;
            bool onlyForecast;
            List<string> daysActive;

            foreach (DataRow dataRow in dataTableFive.Rows)
            {
                id = dataRow.ItemArray[0].ToString();
                title = dataRow.ItemArray[1].ToString();
                description = dataRow.ItemArray[2].ToString();
                dutyLength = (int)dataRow.ItemArray[3];
                pauseLength = (int)dataRow.ItemArray[4];
                weekDaysEnum = (int)dataRow.ItemArray[5];
                daysActive = FindBreakDays(weekDaysEnum);
                if (!dataRow.ItemArray[6].ToString().Equals(""))
                {
                    secondaryWeekDaysEnum = (int)dataRow.ItemArray[6];
                }
                else
                {
                    secondaryWeekDaysEnum = 999;
                }

                if (dataRow.ItemArray[7] != DBNull.Value)
                {
                    typeEnum = (int)dataRow.ItemArray[7];
                }
                else
                {
                    typeEnum = 0;
                }
                if (dataRow.ItemArray[8] != DBNull.Value)
                {
                    pauseStart = (int)dataRow.ItemArray[8];
                }
                else
                {
                    pauseStart = 0;
                }
                if (dataRow.ItemArray[9] != DBNull.Value)
                {
                    onlyForecast = (bool)dataRow.ItemArray[9];
                }
                else
                {
                    onlyForecast = false;
                }


                BreakObject newBreak = new BreakObject()
                {
                    Id = id,
                    Description = description,
                    DutyLength = dutyLength,
                    OnlyForecast = onlyForecast,
                    PauseStart = pauseStart,
                    SecondaryWeekDaysEnum = secondaryWeekDaysEnum,
                    Title = title,
                    TypeEnum = typeEnum,
                    PauseLength = pauseLength,
                    WeekDaysEnum = weekDaysEnum,
                    DaysActive = daysActive
                };

                theDataObject.BreakObjects.Add(newBreak);
            }
        }
        //Mon 1
        //Tue 2
        //Wed 3
        //Thu 4
        //Fri 5
        //Sat 6
        //Sun 0
        internal List<string> FindBreakDays(int weekDaysEnum)
        {
            int weekDays = weekDaysEnum;
            List<int> daysInt = new List<int>();
            List<string> days = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                var mask = (1 << i);
                if ((mask & weekDays) == mask)
                {
                    daysInt.Add(i);
                }
            }
            List<DateTime> daysInData = theDataObject.SetObjects.TrueDaySet;

            for (int i = 0; i < daysInData.Count; i++)
            {
                DateTime currentDate = daysInData[i];
                if (daysInt.Contains((int)currentDate.DayOfWeek))
                {
                    days.Add(daysInData[i].ToString());
                }
            }
            return days;
        }

        internal void DataCSVExportUnion(DataTable dataTable)
        {
            StringBuilder sb = tool.CSVexportPreparation(dataTable);
            string path = "C:\\Users\\Gugli\\Desktop\\DataCollection\\union.csv";
            File.WriteAllText(path, sb.ToString());
            //if (pathInUse.Equals("s"))
            //{
            //    File.WriteAllText("/Users/Simonkloeve/Documents/DTU/specialeGIT/augmentedscheduling/csvFiles/union.csv", sb.ToString());
            //}
            //else if (pathInUse.Equals("nw"))
            //{
            //    File.WriteAllText("E:\\Gugli\\Dropbox\\#DTU\\1Kandidat\\4.Semester\\Speciale\\Exports\\ForImport\\union.csv", sb.ToString());
            //}
            //else
            //{
            //    File.WriteAllText("/Users/Gugli/Drive/#DTU/2Kandidat/4Semester/Speciale-Planday/Database/Exports/ForImport/union.csv", sb.ToString());
            //}

            Console.WriteLine("Union export done.");
        }

        internal Tuple<string, DataObject> GetShiftsInDepartmentFromDataBase(ArrayList activeDepartments, ArrayList departmentNames, ArrayList fallBackSalary,
                                                      SqlConnection conn, int portalID, bool hasWorkRules)
        {
            Console.WriteLine("The following departments are available:");

            for (int i = 0; i < activeDepartments.Count; i++)
            {
                Console.WriteLine("Department number: " + activeDepartments[i] + ". From department: " + departmentNames[i]);
            }

            Console.WriteLine("Enter number of the department you wish to generate data from.");

            var depNumber = Console.ReadLine();

            var validDep = activeDepartments.Contains(depNumber);
            var salaryIndex = 99;
            if (validDep)
            {
                Console.WriteLine(depNumber + " read.");
                salaryIndex = activeDepartments.IndexOf(depNumber);
            }
            else
            {
                Console.WriteLine("Invalid department number entered. Exiting.");
                Environment.Exit(0);
            }
            string fallBackSalaryVal = fallBackSalary[salaryIndex].ToString();
            if (fallBackSalaryVal.Equals(""))
            {
                fallBackSalaryVal = "0.0";
            }

            Console.WriteLine("Enter start date for range, ie \"2017-05-30\"");
            startRange = Console.ReadLine();
            Console.WriteLine(startRange + " read.");
            Console.WriteLine("Enter number of weeks you are generating schedules from ie \"1\"");
            string nrWeeks = Console.ReadLine();
            endRange = tool.CalcEndRange(startRange, nrWeeks);
            Console.WriteLine(endRange + " read.");

            SqlCommand command = new SqlCommand(
                "select  t.Id, t.EmployeeId, t.EmployeeGroupId, t.FunctionId, t.StartTime, t.EndTime " +
                "from[DutySchedule.TimeSheet] t " +
                "join[DutySchedule.DaySchedule] d " +
                "on t.DayScheduleId = d.Id " +
                "where DepartmentId = " + depNumber +
                " and t.StartTime > '" + startRange + "' and t.StartTime < '" + endRange + "'", conn);

            var dataTable = tool.CreateDataTable(command);

            dataTable.Columns.Add("FallBackSalary", typeof(string));
            dataTable.Columns.Add(fallBackSalaryVal, typeof(string));


            ExportToCSVFile(dataTable, "timesheet");
            CreateTimesheetObjects(dataTable);

            if (hasWorkRules)
            {
                GetHistoricDataFromDataBase(depNumber, conn);
            }

            DataSetUpWeeksNDays();

            //Finding the availability in DB
            SqlCommand command2 = new SqlCommand(
                "SELECT bi.[EmployeeId], bi.[Status], bi.[StartTime], bi.[Description], bi.[EndTime] " +
                "FROM[DutySchedule.BookingIntention] bi " +
                "join[Hrm.Employee_rel_Department] eg " +
                "on bi.[EmployeeId] = eg.EmployeeId " +
                "where bi.PortalId like '" + portalID + "' and bi.StartTime > '" + startRange + "' and " +
                "bi.StartTime < '" + endRange + "' and " +
                "eg.DepartmentId like '" + depNumber + "' and eg.IsDeleted like 0", conn);

            dataTableTwo = tool.CreateDataTable(command2);
            ExportToCSVFile(dataTableTwo, "avail");

            //Vacation/absence 
            SqlCommand command3 = new SqlCommand(
                "/****** Vacation/absence. Status 1 for requested, status 2 for denied status 3 for approved  ******/ " +
                "SELECT ar.Id, ap.EmployeeId, ar.Date, ar.AbsenceType, ap.Status " +
                "FROM[Absence.Period] ap " +
                "Join[Absence.Registration] ar " +
                "on ap.Id = ar.PeriodId " +
                "where ar.PortalId like '" + portalID + "' and ar.Date >= '" + startRange + "' and ar.Date < '" + endRange + "' ", conn);

            dataTableThree = tool.CreateDataTable(command3);
            ExportToCSVFile(dataTableThree, "vacation");

            //Shift requests
            SqlCommand command4 = new SqlCommand(
                "/****** Find requestID, TimesheetID and EmployeeId for employees to request a shift  ******/ " +
                "SELECT bi.Id, bi.TimesheetId, bis.EmployeeId " +
                "FROM[DutySchedule.BookingInterest] bi " +
                "join[DutySchedule.BookingInterestSeries] bis " +
                "on bi.BookingInterestSeriesId = bis.Id " +
                "where bis.PortalId like '" + portalID + "' ", conn);

            dataTableFour = tool.CreateDataTable(command4);
            ExportToCSVFile(dataTableFour, "shiftRequests");

            //Break rules 
            SqlCommand command5 = new SqlCommand(
                "/****** Find breaks from portal  ******/ " +
                "SELECT [Id],[Title],[Description],[DutyLength],[PauseLength],[WeekDaysEnum],[SecondaryWeekDaysEnum],[TypeEnum],[PauseStart],[OnlyForecast] " +
                "FROM[DutySchedule.PauseRule] " +
                "where PortalId like '" + portalID + "' and IsDeleted like 0 ", conn);
            dataTableFive = tool.CreateDataTable(command5);

            ExportToCSVFile(dataTableFive, "breaks");
            CreateBreakObjects();

            if (theDataObject.BreakObjects.Count > 0)
            {
                SqlCommand command6 = new SqlCommand(
               "/****** Find rules related to employeegroups  ******/ " +
               "SELECT [PauseRuleId],[EmployeeGroupId] " +
               "FROM [DutySchedule.PauseRule_rel_EmployeeGroup] " +
               "where " + GenerateDataBasePauseRules() + " ", conn);

                dataTableSix = tool.CreateDataTable(command6);
                ExportToCSVFile(dataTableSix, "breaksVsGroups");

                AddEmployeesToBreakObjects();
            }

            return new Tuple<string, DataObject>(depNumber, theDataObject);
        }

        // Quickly access Viktoriagade portal
        internal Tuple<string, DataObject> SQLgetShiftsForDepartmentInRangeQuick(ArrayList activeDepartments, ArrayList departmentNames, ArrayList fallBackSalary,
                                              SqlConnection conn, int portalID, bool hasWorkRules)
        {

            Console.WriteLine("The following departments are available:");

            for (int i = 0; i < activeDepartments.Count; i++)
            {
                Console.WriteLine("Department number: " + activeDepartments[i] + ". From department: " + departmentNames[i]);
            }

            Console.WriteLine("Enter number of the department you wish to generate data from.");

            string depNumber = "52944";

            bool validDep = activeDepartments.Contains(depNumber);
            int salaryIndex = 99;
            if (validDep)
            {
                Console.WriteLine(depNumber + " read.");
                salaryIndex = activeDepartments.IndexOf(depNumber);
            }
            else
            {
                Console.WriteLine("Invalid department number entered. Exiting.");
                Environment.Exit(0);
            }
            string fallBackSalaryVal = fallBackSalary[salaryIndex].ToString();
            if (fallBackSalaryVal.Equals(""))
            {
                fallBackSalaryVal = "0.0";
            }

            Console.WriteLine("Enter start date for range, ie \"2017-05-30\"");
            startRange = "2018-05-28";
            Console.WriteLine(startRange + " read.");
            Console.WriteLine("Enter number of weeks you are generating schedules from ie \"1\"");
            string nrWeeks = "4";
            endRange = tool.CalcEndRange(startRange, nrWeeks);
            Console.WriteLine(endRange + " read.");

            SqlCommand command = new SqlCommand(
                "select  t.Id, t.EmployeeId, t.EmployeeGroupId, t.FunctionId, t.StartTime, t.EndTime " +
                "from[DutySchedule.TimeSheet] t " +
                "join[DutySchedule.DaySchedule] d " +
                "on t.DayScheduleId = d.Id " +
                "where DepartmentId = " + depNumber +
                " and t.StartTime > '" + startRange + "' and t.StartTime < '" + endRange + "'", conn);

            var dataTable = tool.CreateDataTable(command);

            dataTable.Columns.Add("FallBackSalary", typeof(string));
            dataTable.Columns.Add(fallBackSalaryVal, typeof(string));

            ExportToCSVFile(dataTable, "timesheet");
            CreateTimesheetObjects(dataTable);

            if (hasWorkRules)
            {
                GetHistoricDataFromDataBase(depNumber, conn);
            }

            DataSetUpWeeksNDays();

            //Availability
            SqlCommand command2 = new SqlCommand(
                "SELECT bi.[EmployeeId], bi.[Status], bi.[StartTime], bi.[Description], bi.[EndTime] " +
                "FROM[DutySchedule.BookingIntention] bi " +
                "join[Hrm.Employee_rel_Department] eg " +
                "on bi.[EmployeeId] = eg.EmployeeId " +
                "where bi.PortalId like '" + portalID + "' and bi.StartTime > '" + startRange + "' and " +
                "bi.StartTime < '" + endRange + "' and " +
                "eg.DepartmentId like '" + depNumber + "' and eg.IsDeleted like 0", conn);

            dataTableTwo = tool.CreateDataTable(command2);
            ExportToCSVFile(dataTableTwo, "avail");

            //Vacation/absence 
            SqlCommand command3 = new SqlCommand(
                "/****** Vacation/absence. Status 1 for requested, status 2 for denied status 3 for approved  ******/ " +
                "SELECT ar.Id, ap.EmployeeId, ar.Date, ar.AbsenceType, ap.Status " +
                "FROM[Absence.Period] ap " +
                "Join[Absence.Registration] ar " +
                "on ap.Id = ar.PeriodId " +
                "where ar.PortalId like '" + portalID + "' and ar.Date >= '" + startRange + "' and ar.Date < '" + endRange + "' ", conn);

            dataTableThree = tool.CreateDataTable(command3);
            ExportToCSVFile(dataTableThree, "vacation");

            //Shift requests
            SqlCommand command4 = new SqlCommand(
                "/****** Find requestID, TimesheetID and EmployeeId for employees to request a shift  ******/ " +
                "SELECT bi.Id, bi.TimesheetId, bis.EmployeeId " +
                "FROM[DutySchedule.BookingInterest] bi " +
                "join[DutySchedule.BookingInterestSeries] bis " +
                "on bi.BookingInterestSeriesId = bis.Id " +
                "where bis.PortalId like '" + portalID + "' ", conn);

            dataTableFour = tool.CreateDataTable(command4);
            ExportToCSVFile(dataTableFour, "shiftRequests");

            //Break rules 
            SqlCommand command5 = new SqlCommand(
                "/****** Find breaks from portal  ******/ " +
                "SELECT [Id],[Title],[Description],[DutyLength],[PauseLength],[WeekDaysEnum],[SecondaryWeekDaysEnum],[TypeEnum],[PauseStart],[OnlyForecast] " +
                "FROM[DutySchedule.PauseRule] " +
                "where PortalId like '" + portalID + "' and IsDeleted like 0 ", conn);
            dataTableFive = tool.CreateDataTable(command5);

            ExportToCSVFile(dataTableFive, "breaks");
            CreateBreakObjects();

            if (theDataObject.BreakObjects.Count > 0)
            {
                SqlCommand command6 = new SqlCommand(
               "/****** Find rules related to employeegroups  ******/ " +
               "SELECT [PauseRuleId],[EmployeeGroupId] " +
               "FROM [DutySchedule.PauseRule_rel_EmployeeGroup] " +
               "where " + GenerateDataBasePauseRules() + " ", conn);

                dataTableSix = tool.CreateDataTable(command6);
                ExportToCSVFile(dataTableSix, "breaksVsGroups");
                AddEmployeesToBreakObjects();
            }

            return new Tuple<string, DataObject>(depNumber, theDataObject);
        }

        internal void DataSetUpWeeksNDays()
        {

            List<DateTime> days = tool.CloneDaySetList(theDataObject.SetObjects.TrueDaySet);

            days.Sort();

            int lastIndex = days.Count - 1;

            while (days[0].DayOfWeek != DayOfWeek.Monday)
            {

                days[0] = days[0].AddDays(-1);
            }

            while (days[lastIndex].DayOfWeek != DayOfWeek.Sunday)
            {
                days[lastIndex] = days[lastIndex].AddDays(1);
            }

            TimeSpan range = days[lastIndex].Subtract(days[0]);

            int weekCount = (int)Math.Ceiling((double)(range.Days + 1) / 7);

            int dayCount = theDataObject.SetObjects.DaySet.Count;

            theDataObject.DayCount = dayCount;

            theDataObject.WeekCount = weekCount;
        }

        internal void AddEmployeesToBreakObjects()
        {
            string idPauseRule = "";
            string employeeGroupId = "";

            foreach (DataRow datarow in dataTableSix.Rows)
            {
                idPauseRule = datarow.ItemArray[0].ToString();
                employeeGroupId = "eg_" + datarow.ItemArray[1];
                if (theDataObject.SetObjects.EmpGroupSet.Contains(employeeGroupId))
                {
                    bool breakExists = theDataObject.BreakObjects.Exists(x => x.Id == idPauseRule);

                    if (breakExists)
                    {
                        BreakObject curBreak = theDataObject.BreakObjects.Find(x => x.Id == idPauseRule);
                        curBreak.EmployeeGroups.Add(employeeGroupId);
                    }
                }
            }
        }

        internal string GenerateDataBasePauseRules()
        {
            string pauseRelEmpG = "";
            string pauseRuleid = " PauseRuleId like ";
            string sqlOr = " or ";

            List<BreakObject> theObjs = theDataObject.BreakObjects;

            for (int i = 0; i < theObjs.Count; i++)
            {
                pauseRelEmpG += pauseRuleid + theObjs[i].Id;
                if (i + 1 < theObjs.Count)
                {
                    pauseRelEmpG += sqlOr;
                }
            }
            return pauseRelEmpG;
        }

        internal void CreateShiftRequestObjects()
        {
            string id = "";
            string timeSheetId = "";
            string employeeId = "";

            foreach (DataRow datarow in dataTableFour.Rows)
            {
                id = datarow.ItemArray[0].ToString();
                timeSheetId = "Id_" + datarow.ItemArray[1];
                employeeId = "emp_" + datarow.ItemArray[2];

                bool exists = theDataObject.TimesheetObjects.Exists(x => x.SheetId == timeSheetId);

                if (exists)
                {
                    RequestObject requestObj = new RequestObject() { Id = id, TimesheeId = timeSheetId, EmployeeId = employeeId };

                    TimeSheetObject currentTimeSheet = theDataObject.TimesheetObjects.Find(x => x.SheetId == timeSheetId);

                    currentTimeSheet.Requests.Add(requestObj);
                  //  Console.WriteLine("RequestID: " + id + " added");
                }
            }
        }

        internal void AddVacationInformationToEmployeeObjects()
        {
            string id = "";
            string employeeId = "";
            string date = "";
            string absenceType = "";
            string status = "";

            foreach (DataRow datarow in dataTableThree.Rows)
            {
                id = "vId_" + datarow.ItemArray[0];
                employeeId = "emp_" + datarow.ItemArray[1];
                date = tool.FixDayLayout(datarow.ItemArray[2].ToString());
                DateTime trueDate = DateTime.Parse(datarow.ItemArray[2].ToString());
                absenceType = datarow.ItemArray[3].ToString();
                status = datarow.ItemArray[4].ToString();

                VacationObject curVacation = new VacationObject()
                {
                    VacationId = id,
                    EmployeeId = employeeId,
                    Date = date,
                    AbsenceType = absenceType,
                    Status = status,
                    VacaDateTrue = trueDate
                };

                bool empExists = theDataObject.EmployeeObjects.Exists(x => x.EmployeeId == employeeId);
                if (empExists)
                {
                    EmployeeObject curEmployee = theDataObject.EmployeeObjects.Find(x => x.EmployeeId == employeeId);
                    curEmployee.Vacations.Add(curVacation);
                }
            }
        }

        internal void AddAvailabilityInformationToEmployeeObjects()
        {
            string availabilityStatus = "";
            string availDate = "";
            string availStartTime = "";
            string availEndTime = "";
            string availDesciption = "";

            foreach (DataRow dataRow in dataTableTwo.Rows)
            {
                EmployeeObject curEmployee = theDataObject.EmployeeObjects.Find(x => x.EmployeeId == "emp_" + dataRow.ItemArray[0]);
                availabilityStatus = dataRow.ItemArray[1].ToString();
                string startDate = dataRow.ItemArray[2].ToString();
                string endDate = dataRow.ItemArray[4].ToString();
                DateTime trueStartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(startDate), TimeZoneInfo.Local);
                DateTime trueEndTime;
                if (!endDate.Equals(""))
                {
                    trueEndTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(endDate), TimeZoneInfo.Local);
                }
                else
                {
                    trueEndTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(startDate), TimeZoneInfo.Local);
                }

                availDate = tool.FixDayLayout(dataRow.ItemArray[2].ToString());

                availStartTime = tool.GetDateTimeHourMin(startDate);

                if (!endDate.Equals(""))
                {
                    availEndTime = Convert.ToDecimal(TimeSpan.Parse(trueEndTime.Hour + ":" + trueEndTime.Minute).TotalHours).ToString();
                    trueEndTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(endDate), TimeZoneInfo.Local);
                    //TODO HEr der sker noget 
                    //if(trueEndTime < trueStartTime){
                    //    trueEndTime = trueEndTime.AddDays(1);
                    //}
                }
                else
                {
                    availEndTime = tool.GetDateTimeHourMin(startDate);
                    trueStartTime = new DateTime(trueStartTime.Year, trueStartTime.Month, trueStartTime.Day);
                    trueEndTime = trueStartTime + new TimeSpan(24, 0, 0);
                }

                if (availEndTime.Equals(""))
                {
                    availStartTime = "00.00";
                    availEndTime = "24.00";
                }
                availDesciption = dataRow.ItemArray[3].ToString();

                curEmployee.AvailabilitiesGams.Add(new AvailabilityObject()
                {
                    Availability = availabilityStatus,
                    Description = availDesciption,
                    EndTime = availEndTime,
                    StartTime = availStartTime,
                    Date = availDate,
                    TrueStartTime = trueStartTime,
                    TrueEndTime = trueEndTime
                });

                if (availabilityStatus.Equals("0"))
                {
                    curEmployee.Unavailabilities.Add(new AvailabilityObject()
                    {
                        Availability = availabilityStatus,
                        Description = availDesciption,
                        EndTime = availEndTime,
                        StartTime = availStartTime,
                        Date = availDate,
                        TrueStartTime = trueStartTime,
                        TrueEndTime = trueEndTime
                    });
                }
                else if (availabilityStatus.Equals("1"))
                {
                    curEmployee.Availabilities.Add(new AvailabilityObject()
                    {
                        Availability = availabilityStatus,
                        Description = availDesciption,
                        EndTime = availEndTime,
                        StartTime = availStartTime,
                        Date = availDate,
                        TrueStartTime = trueStartTime,
                        TrueEndTime = trueEndTime
                    });
                }
                else
                {
                    throw new Exception("Something wrong when loading data to availabilities lists");
                }
            }
        }

        internal void CreateTimesheetObjects(DataTable dataTable)
        {
            string sheetId = "";
            string employeeId = "";
            string employeeGroupId = "";
            string functionId = "";
            string startYear = "";
            string startMonth = "";
            string startDay = "";
            //string startHour = "";
            //string startMinute = "";
            //string fakeStartMinute = "";
            //string endYear = "";
            //string endMonth = "";
            string endDay = "";
            string startDayTrue = "";
            //string endDayTrue = "";
            string endHour = "";
            //string endMinute = "";
            //string fakeEndMinute = "";
            string rawSheetId = "";
            string rawEmployeeId = "";
            string rawEmployeeGroupId = "";
            string rawPositionId = "";
            string fallbackSalary = "";

            bool firstTimeMet = true;
            string firstTimeMetId = "";
            double duration = 0;
            bool shiftOverlaps = false;

            foreach (DataColumn dataCol in dataTable.Columns)
            {
                fallbackSalary = dataCol.ColumnName;
            }
            theDataObject.FallBackSalary = fallbackSalary;

            foreach (DataRow dataRow in dataTable.Rows)
            {
                rawSheetId = dataRow.ItemArray[0].ToString();
                rawEmployeeId = dataRow.ItemArray[1].ToString();
                rawEmployeeGroupId = dataRow.ItemArray[2].ToString();
                rawPositionId = dataRow.ItemArray[3].ToString();

                if (!rawSheetId.Equals(""))
                {
                    sheetId = "Id_" + rawSheetId;
                }
                else
                {
                    continue;
                }

                if (firstTimeMet)
                {
                    firstTimeMetId = sheetId;
                    firstTimeMet = false;
                }

                if (rawEmployeeId.Equals(""))
                {

                    employeeId = "Open Shift";
                }
                else
                {
                    employeeId = "Open Shift";
                    //  employeeId = "emp_" + rawEmployeeId;
                }
                if (!rawEmployeeGroupId.Equals(""))
                {
                    employeeGroupId = "eg_" + rawEmployeeGroupId;
                    if (!theDataObject.SetObjects.EmpGroupSet.Contains(employeeGroupId))
                    {
                        theDataObject.SetObjects.EmpGroupSet.Add(employeeGroupId);
                    }
                }
                else
                {
                    if (firstTimeMetId.Equals(sheetId) && firstTimeMet == false)
                    {
                        firstTimeMet = true;
                    }
                    continue;
                }

                DateTime tempTime = DateTime.Parse(dataRow.ItemArray[4].ToString());
                if (theDataObject.PortalName.Equals("andersons"))
                {
                    DateTimeOffset do1 = new DateTimeOffset(2008, 8, 22, 1, 0, 0, new TimeSpan(6, 0, 0));
                    tempTime = tempTime.Subtract(do1.Offset);
                }
                DateTime trueStartTime = TimeZoneInfo.ConvertTimeFromUtc(tempTime, TimeZoneInfo.Local);

                if (!rawPositionId.Equals(""))
                {
                    //TODO
                    functionId = "pos_" + rawPositionId;
                    if (!theDataObject.SetObjects.PositionSet.Contains(functionId))
                    {
                        theDataObject.SetObjects.PositionSet.Add(functionId);
                    }
                    else
                    {

                        List<TimeSheetObject> temp = theDataObject.TimesheetObjects.FindAll(x => (x.TrueStartTime.Date == trueStartTime.Date && x.PositionId.Split('-')[0] == functionId));

                        if (temp.Count > 0)
                        {
                            // int counter = 0;
                            int count = temp.Count;
                            functionId = "pos_" + rawPositionId + "-" + count;
                            // Console.WriteLine(functionId);
                            if (!theDataObject.SetObjects.PositionSet.Contains(functionId))
                            {
                                theDataObject.SetObjects.PositionSet.Add(functionId);
                            }
                        }


                    }

                }
                else
                {
                    if (firstTimeMetId.Equals(sheetId) && firstTimeMet == false)
                    {
                        firstTimeMet = true;
                    }
                    continue;
                }
                string startTimeObj = trueStartTime.ToString();

                startYear = tool.FixYearLayout(startTimeObj);
                if (!theDataObject.SetObjects.StartYear.Contains("y_" + startYear))
                {
                    theDataObject.SetObjects.StartYear.Add("y_" + startYear);
                }
                //startMonth = FixMonthLayout(startTimeObj);
                startDay = tool.FixDayLayout(startTimeObj);
                startDayTrue = tool.FixTrueEnd(startTimeObj);

                //startHour = FixHourLayout(startTimeObj);
                //startMinute = FixMinuteLayout(startTimeObj);
                //fakeStartMinute = FixFakeMinuteLayout(startTimeObj);
                bool invalidSheet = false;
                if (dataRow.ItemArray[5] != DBNull.Value)
                {
                    DateTime tempTimeEnd = DateTime.Parse(dataRow.ItemArray[5].ToString());
                    if (theDataObject.PortalName.Equals("andersons"))
                    {
                        DateTimeOffset do1 = new DateTimeOffset(2008, 8, 22, 1, 0, 0, new TimeSpan(6, 0, 0));
                        tempTimeEnd = tempTimeEnd.Subtract(do1.Offset);
                    }
                    DateTime trueEndTime = TimeZoneInfo.ConvertTimeFromUtc(tempTimeEnd, TimeZoneInfo.Local);

                    string endTimeObj = trueEndTime.ToString();
                    endDay = tool.FixDayLayout(endTimeObj);
                    //endDayTrue = FixTrueEnd(endTimeObj);
                    endHour = tool.FixHourLayout(endTimeObj);
                    bool isWeekend = trueStartTime.DayOfWeek == DayOfWeek.Saturday | trueStartTime.DayOfWeek == DayOfWeek.Sunday;

                    duration = tool.FindShiftDuration(trueStartTime, trueEndTime);

                    shiftOverlaps = trueStartTime.Date < trueEndTime.Date;

                    theDataObject.TimesheetObjects.Add(new TimeSheetObject
                    {
                        SheetId = sheetId,
                        EmployeeGroupId = employeeGroupId,
                        EmployeeId = employeeId,
                        EndDay = endDay,
                        PositionId = functionId,
                        StartDay = startDay,
                        EndHour = endHour,
                        //EndMinute = endMinute,
                        //StartHour = startHour,
                        //StartMinute = startMinute,
                        //StartYear = startYear,
                        //StartMonth = startMonth,
                        //EndYear = endYear,
                        //EndMonth = endMonth,
                        //EndDayTrue = endDayTrue,
                        //StartDayTrue = startDayTrue,
                        //FakeEndMinute = fakeEndMinute,
                        //FakeStartMinute = fakeStartMinute,
                        TrueStartTime = trueStartTime,
                        TrueEndTime = trueEndTime,
                        IsWeekend = isWeekend,
                        Duration = duration,
                        ShiftOverlapsMidnight = shiftOverlaps
                    });

                }
                else
                {
                    invalidSheet = true;
                }



                // endYear = FixYearLayout(endTimeObj);
                // endMonth = FixMonthLayout(endTimeObj);

                // endMinute = FixMinuteLayout(endTimeObj);
                //fakeEndMinute = FixFakeMinuteLayout(endTimeObj);

                //TODO Finder ikke rigtige weekend dage pt! 

                string daySetEntry = "d_" + startYear + "_" + startMonth + "_" + startDayTrue;

                if (!theDataObject.SetObjects.DaySet.Contains(daySetEntry) && daySetEntry != "d___")
                {
                    theDataObject.SetObjects.DaySet.Add(daySetEntry);
                }
                if (!theDataObject.SetObjects.TrueDaySet.Contains(trueStartTime.Date))
                {
                    theDataObject.SetObjects.TrueDaySet.Add(trueStartTime.Date);
                }
            }
            theDataObject.SetObjects.TrueDaySet.Sort();

            int lengthOfSet = theDataObject.SetObjects.TrueDaySet.Count;

            if (theDataObject.SetObjects.TrueDaySet.Count > 0)
            {
                DateTime lastDay = theDataObject.SetObjects.TrueDaySet[lengthOfSet - 1];

                if (lastDay.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    theDataObject.SetObjects.LastDayInRange = lastDay;
                }
                else
                {
                    while (!lastDay.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        lastDay = lastDay.AddDays(1);
                    }
                    theDataObject.SetObjects.LastDayInRange = lastDay;
                }

                DateTime firstDay = theDataObject.SetObjects.TrueDaySet[0];

                if (firstDay.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    theDataObject.SetObjects.FirstDayInRange = firstDay;
                }
                else
                {
                    while (!firstDay.DayOfWeek.Equals(DayOfWeek.Monday))
                    {
                        firstDay = firstDay.AddDays(-1);
                    }
                    theDataObject.SetObjects.FirstDayInRange = firstDay;
                }
            }
        }



        //internal string FixFakeMinuteLayout(string timeVal)
        //{
        //    string time = "";
        //    DateTime fixedTime = DateTime.Parse(timeVal);
        //    time = tool.FormatMinutes(fixedTime.Minute.ToString().PadLeft(2, '0'));

        //    return time;
        //}



        internal void GetHistoricDataFromDataBase(string departmentID, SqlConnection connection)
        {
            string newStartRange = FindHistoricStartRange();
            string realStartRange = tool.FindFirstMondayInRange(newStartRange);
            string newEndRange = startRange;

            SqlCommand command = new SqlCommand(
              "select t.Id, t.EmployeeId, t.FunctionId, t.StartTime, t.EndTime " +
              "from[DutySchedule.TimeSheet] t " +
              "join[DutySchedule.DaySchedule] d " +
              "on t.DayScheduleId = d.Id " +
                "where DepartmentId = " + departmentID + " and t.StartTime > '" + realStartRange + "' and t.StartTime < '" + newEndRange + "'", connection);

            var dataTable = tool.CreateDataTable(command);

            StringBuilder sb = tool.CSVexportPreparation(dataTable);

            string path = "C:\\Users\\Gugli\\Desktop\\DataCollection\\historicSheet.csv";
            File.WriteAllText(path, sb.ToString());
            //if (pathInUse.Equals("s"))
            //{
            //    File.WriteAllText("/Users/Simonkloeve/Documents/DTU/specialeGIT/augmentedscheduling/csvFiles/historicSheet.csv", sb.ToString());
            //}
            //else if (pathInUse.Equals("nw"))
            //{
            //    File.WriteAllText("E:\\Gugli\\Dropbox\\#DTU\\1Kandidat\\4.Semester\\Speciale\\Exports\\ForImport\\historicSheet.csv", sb.ToString());
            //}
            //else
            //{
            //    File.WriteAllText("/Users/Gugli/Drive/#DTU/2Kandidat/4Semester/Speciale-Planday/Database/Exports/ForImport/historicSheet.csv", sb.ToString());
            //}

            AddHistoricTimesheetDataToTimeSheetObject(dataTable);

            Console.WriteLine("Historic data export done.");
        }

        internal string FindHistoricStartRange()
        {
            string startRange2 = "";

            List<UnionRuleObject> theObjs = theDataObject.UnionRuleObjects;

            string timeEnum = "";
            int timeVal = 0;
            int tempTimeVal = 0;
            foreach (UnionRuleObject obj in theObjs)
            {
                timeEnum = obj.TimeConditionTypeEnum;

                if (timeEnum.Equals("4"))
                {
                    if (!obj.TimeConditionValue.Equals(""))
                    {
                        timeVal = Int32.Parse(obj.TimeConditionValue) * 7;
                        if (timeVal > tempTimeVal)
                        {
                            tempTimeVal = timeVal;
                        }
                    }
                }
                else if (timeEnum.Equals("3"))
                {
                    if (!obj.TimeConditionValue.Equals(""))
                    {
                        timeVal = Int32.Parse(obj.TimeConditionValue);
                        if (timeVal > tempTimeVal)
                        {
                            tempTimeVal = timeVal;
                        }
                    }
                }
            }

            DateTime temp = DateTime.Parse(startRange);

            if (tempTimeVal == 0 || tempTimeVal < 7)
            {
                tempTimeVal = 7;
            }

            temp = temp.AddDays(-tempTimeVal);
            startRange2 = temp.Year.ToString() + "-" + temp.Month.ToString() + "-" + temp.Day.ToString();

            return startRange2;
        }



        internal void AddHistoricTimesheetDataToTimeSheetObject(DataTable dataTable)
        {
            string sheetId = "";
            string employeeId = "";
            string employeeGroupId = "";
            string functionId = "";
            string startYear = "";
            //string startMonth = "";
            string startDay = "";
            //string startHour = "";
            //string startMinute = "";
            //string fakeStartMinute = "";
            //string endYear = "";
            //string endMonth = "";
            string endDay = "";
            string endHour = "";
            //string endMinute = "";
            //string fakeEndMinute = "";
            string rawEmployeeId = "";
            string rawPositionId = "";
            //string startDayTrue = "";
            //string endDayTrue = "";

            bool firstTimeMet = true;
            string firstTimeMetId = "";
            string lastTimeMetId = "";

            double duration = 0;
            bool shiftOverlaps = false;
            int historicDayCount = 1;
            DateTime trueStartTime = new DateTime();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                rawEmployeeId = dataRow.ItemArray[1].ToString();

                if (!rawEmployeeId.Equals(""))
                {
                    sheetId = "id_" + dataRow.ItemArray[0];
                    if (firstTimeMet)
                    {
                        firstTimeMetId = sheetId;
                        firstTimeMet = false;
                    }
                    lastTimeMetId = sheetId;

                    employeeId = "emp_" + rawEmployeeId;

                    DateTime tempTime = DateTime.Parse(dataRow.ItemArray[3].ToString());
                    if (theDataObject.PortalName.Equals("andersons"))
                    {
                        DateTimeOffset do1 = new DateTimeOffset(2008, 8, 22, 1, 0, 0, new TimeSpan(6, 0, 0));
                        tempTime = tempTime.Subtract(do1.Offset);
                    }
                    trueStartTime = TimeZoneInfo.ConvertTimeFromUtc(tempTime, TimeZoneInfo.Local);

                    rawPositionId = dataRow.ItemArray[2].ToString();

                    if (!rawPositionId.Equals(""))
                    {
                        functionId = "pos_" + rawPositionId;
                        if (!theDataObject.SetObjects.PositionSet.Contains(functionId))
                        {
                            theDataObject.SetObjects.PositionSet.Add(functionId);
                        }
                        else
                        {
                            List<TimeSheetObject> temp = theDataObject.TimesheetObjects.FindAll(x => x.TrueStartTime.Date == trueStartTime.Date);
                            int counter = 1;
                            foreach (TimeSheetObject sheet in temp)
                            {
                                if (sheet.PositionId.Split('/')[0].Equals(sheet.PositionId))
                                {
                                    counter++;
                                }
                            }
                            functionId = "pos_" + rawPositionId + "/" + counter;
                            theDataObject.SetObjects.PositionSet.Add(functionId);
                        }
                    }


                    string startTimeObj = trueStartTime.ToString();

                    startYear = tool.FixYearLayout(startTimeObj);
                    if (!theDataObject.SetObjects.StartYear.Contains("y_" + startYear))
                    {
                        theDataObject.SetObjects.StartYear.Add("y_" + startYear);
                    }
                    //startMonth = FixMonthLayout(startTimeObj);
                    startDay = tool.FixDayLayout(startTimeObj);
                    //startDayTrue = FixTrueEnd(startTimeObj);
                    //startHour = FixHourLayout(startTimeObj);
                    //startMinute = FixMinuteLayout(startTimeObj);
                    //fakeStartMinute = FixFakeMinuteLayout(startTimeObj);


                    DateTime tempTimeEnd = DateTime.Parse(dataRow.ItemArray[4].ToString());
                    if (theDataObject.PortalName.Equals("andersons"))
                    {
                        DateTimeOffset do1 = new DateTimeOffset(2008, 8, 22, 1, 0, 0, new TimeSpan(6, 0, 0));
                        tempTimeEnd = tempTimeEnd.Subtract(do1.Offset);
                    }
                    DateTime trueEndTime = TimeZoneInfo.ConvertTimeFromUtc(tempTimeEnd, TimeZoneInfo.Local);

                    string endTimeObj = trueEndTime.ToString();
                    //endYear = FixYearLayout(endTimeObj);
                    //endMonth = FixMonthLayout(endTimeObj);
                    endDay = tool.FixDayLayout(endTimeObj);
                    //endDayTrue = FixTrueEnd(endTimeObj);
                    endHour = tool.FixHourLayout(endTimeObj);
                    //endMinute = FixMinuteLayout(endTimeObj);
                    //fakeEndMinute = FixFakeMinuteLayout(endTimeObj);

                    bool isWeekend = trueStartTime.DayOfWeek == DayOfWeek.Saturday | trueStartTime.DayOfWeek == DayOfWeek.Sunday;

                    //TODO SKal laves om hvis det skal i production.
                    if (isWeekend && (theDataObject.PortalName.Equals("marienlyst") || theDataObject.PortalName.Equals("admiralhotel")))
                    {
                        employeeId = "Open Shift";
                    }

                    duration = tool.FindShiftDuration(trueStartTime, trueEndTime);

                    shiftOverlaps = trueStartTime.Date < trueEndTime.Date;
                    if (!theDataObject.SetObjects.TrueDaySet.Contains(trueStartTime.Date))
                    {
                        theDataObject.SetObjects.TrueDaySet.Add(trueStartTime.Date);
                    }
                    theDataObject.TimesheetObjects.Add(new TimeSheetObject
                    {
                        SheetId = sheetId,
                        EmployeeGroupId = employeeGroupId,
                        EmployeeId = employeeId,
                        EndDay = endDay,
                        PositionId = functionId,
                        StartDay = startDay,
                        EndHour = endHour,
                        //EndMinute = endMinute,
                        //StartHour = startHour,
                        //StartMinute = startMinute,
                        //StartYear = startYear,
                        //StartMonth = startMonth,
                        //EndYear = endYear,
                        //EndMonth = endMonth,
                        //EndDayTrue = endDayTrue,
                        //StartDayTrue = startDayTrue,
                        //FakeEndMinute = fakeEndMinute,
                        //FakeStartMinute = fakeStartMinute,
                        TrueStartTime = trueStartTime,
                        TrueEndTime = trueEndTime,
                        IsWeekend = isWeekend,
                        Duration = duration,
                        ShiftOverlapsMidnight = shiftOverlaps
                    });
                }
                string dateString = "d_" + trueStartTime.Year + "_" + trueStartTime.Month + "_" + trueStartTime.Day;

                if (!theDataObject.SetObjects.DaySet.Contains(dateString) && dateString != "d_1_1_1")
                {
                    theDataObject.SetObjects.DaySet.Add(dateString);
                    historicDayCount++;
                }
            }
            theDataObject.SetObjects.DaySet.Sort();


            theDataObject.SetObjects.TrueDaySet.Sort();

            if (theDataObject.SetObjects.TrueDaySet.Count > 0)
            {
                DateTime firstDay = theDataObject.SetObjects.TrueDaySet[0];

                if (firstDay.DayOfWeek.Equals(DayOfWeek.Monday))
                {
                    theDataObject.SetObjects.FirstDayInRange = firstDay;
                }
                else
                {
                    while (!firstDay.DayOfWeek.Equals(DayOfWeek.Monday))
                    {
                        firstDay = firstDay.AddDays(-1);
                    }
                    theDataObject.SetObjects.FirstDayInRange = firstDay;
                }
            }
        }

        internal void ExportToCSVFile(DataTable dataTable, string fileName)
        {
            var sb = tool.CSVexportPreparation(dataTable);

            string path = "C:\\Users\\Gugli\\Desktop\\DataCollection\\" + fileName + ".csv";
            File.WriteAllText(path, sb.ToString());

            //if (pathInUse.Equals("s"))
            //{
            //    File.WriteAllText("/Users/Simonkloeve/Documents/DTU/specialeGIT/augmentedscheduling/csvFiles/" + fileName + ".csv", sb.ToString());
            //}
            //else if (pathInUse.Equals("nw"))
            //{
            //    File.WriteAllText("C:\\Users\\Nikolaj\\Google Drive\\#DTU\\2Kandidat\\4Semester\\Speciale-Planday\\Database\\Exports\\ForImport\\" + fileName + ".csv", sb.ToString());
            //}
            //else
            //{
            //    File.WriteAllText("/Users/Gugli/Drive/#DTU/2Kandidat/4Semester/Speciale-Planday/Database/Exports/ForImport/" + fileName + ".csv", sb.ToString());
            //}

            Console.WriteLine(fileName + " export done.");
        }


    }
}
