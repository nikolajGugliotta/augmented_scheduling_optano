﻿using AugmentedSchedulingOptano.Library.Models;
using AugmentedSchedulingOptano.Library.OldTools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace AugmentedSchedulingOptano.Library.DAO
{
    class EmployeeDAO
    {
        readonly ToolsOld tool;
        readonly string pathInUse;
        DataObject theDataObject;

        public EmployeeDAO(string pathInUse, DataObject dataObject)
        {
            tool = new ToolsOld();
            this.pathInUse = pathInUse;
            theDataObject = dataObject;
        }

        public DataObject RunThis(string stagingName, string departmentId, string connectionString)
        {
            GetEmployeeInformationFromDataBase(connectionString, stagingName, departmentId);

            return theDataObject;
        }

        internal void GetEmployeeInformationFromDataBase(string connectionString, string stagingName, string departmentId)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                var portalId = tool.SQLFindPortalID(stagingName, connection);

                GetEmployeesFromDepartment(connection, portalId, departmentId);
            }
        }

        internal void GetEmployeesFromDepartment(SqlConnection conn, int portalId, string depId)
        {
            SqlCommand command = new SqlCommand("" +
                                                "select e.Id, eg.EmployeeGroupId, heg.DefaultHourSalary, e.EmployeeTypeId, et.Name, e.FirstName, e.LastName, e.WorkHours, e.WorkHoursWeek " +
                                                "from [Hrm.Employee] e " +
                                                "join[Hrm.Employee_rel_Department] ed " +
                                                "on e.Id = ed.EmployeeId " +
                                                "join[Hrm.Department] d " +
                                                "on ed.DepartmentId = d.Id " +
                                                "join[Hrm.Employee_rel_EmployeeGroup] eg " +
                                                "on e.Id = eg.EmployeeId " +
                                                "join[Hrm.EmployeeGroup] heg " +
                                                "on eg.EmployeeGroupId = heg.Id " +
                                                "join[Hrm.EmployeeType] et " +
                                                "on e.EmployeeTypeId = et.Id " +
                                                "where d.PortalId = " + portalId + " and d.Id = " + depId +
                                                "", conn);
            //" and e.IsDeleted = 0 and ed.IsDeleted = 0 and d.IsDeleted = 0 and eg.IsDeleted = 0 ", conn);
            var dataTable = tool.CreateDataTable(command);
            ExportEmployeeToCSV(dataTable);
            CreateEmployeeObjects(dataTable);
        }

        internal void CreateEmployeeObjects(DataTable dataTable)
        {
            var employeeId = "";
            var employeeGroupId = "";
            var defaultHourSalary = "";
            var employeeTypeId = "";
            var typeName = "";
            //var nomialHours = "";
            var firstName = "";
            var lastName = "";
            //var tempNorm = "";
            var temp = "";
            var contractedHours = 0.0;

            foreach (DataRow dataRow in dataTable.Rows)
            {
                employeeId = "emp_" + dataRow.ItemArray[0];
                employeeGroupId = "eg_" + dataRow.ItemArray[1];
                if (!theDataObject.SetObjects.EmpGroupSet.Contains(employeeGroupId))
                {
                    theDataObject.SetObjects.EmpGroupSet.Add(employeeGroupId);
                }

                defaultHourSalary = dataRow.ItemArray[2].ToString();
                employeeTypeId = dataRow.ItemArray[3].ToString();
                typeName = dataRow.ItemArray[4].ToString();
                firstName = dataRow.ItemArray[5].ToString();
                lastName = dataRow.ItemArray[6].ToString();

                temp = dataRow.ItemArray[7].ToString();
                if (!temp.Equals(""))
                {
                    //tempNorm = (double.Parse(temp) / 4).ToString();
                    contractedHours = double.Parse(temp) / 4;
                }
                else
                {
                    //tempNorm = "0";
                    contractedHours = 0;
                }

                //nomialHours = tempNorm;

                var metBefore = theDataObject.EmployeeObjects.Exists(x => x.EmployeeId == employeeId);

                if (metBefore)
                {
                    EmployeeObject currentEmployee = theDataObject.EmployeeObjects.Find(x => x.EmployeeId == employeeId);
                    currentEmployee.EmployeeGroups.Add(employeeGroupId);
                }
                else
                {
                    EmployeeObject newEmployee = new EmployeeObject
                    {
                        EmployeeId = employeeId,
                        EmployeeGroups = new List<string> { employeeGroupId },
                        DefaultHourSal = defaultHourSalary,
                        //NominalHours = nomialHours,
                        EmployeeName = firstName + "_" + lastName,
                        ContractedHours = contractedHours
                    };

                    theDataObject.EmployeeObjects.Add(newEmployee);
                    theDataObject.SetObjects.EmpSet.Add(newEmployee.EmployeeId);
                }
            }
        }

        internal void ExportEmployeeToCSV(DataTable dataTable)
        {
            var sb = tool.CSVexportPreparation(dataTable);

            string path = "C:\\Users\\Gugli\\Desktop\\DataCollection\\empInfo.csv";
            File.WriteAllText(path, sb.ToString());
            //if (pathInUse.Equals("s"))
            //{
            //    File.WriteAllText("/Users/Simonkloeve/Documents/DTU/specialeGIT/augmentedscheduling/csvFiles/empInfo.csv", sb.ToString());

            //}
            //else if (pathInUse.Equals("nw"))
            //{
            //    File.WriteAllText("E:\\Gugli\\Dropbox\\#DTU\\1Kandidat\\4.Semester\\Speciale\\Exports\\ForImport\\empInfo.csv", sb.ToString());
            //}
            //else
            //{
            //    File.WriteAllText("/Users/Gugli/Drive/#DTU/2Kandidat/4Semester/Speciale-Planday/Database/Exports/ForImport/empInfo.csv", sb.ToString());
            //}
        }
    }
}
