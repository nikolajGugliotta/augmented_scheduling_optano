﻿using System;

namespace AugmentedSchedulingOptano.Library.ModelTools
{
    internal class DaySet
    {
        // public DateTime DayId { get; set; }
        public string DayId { get; set; }
        public DateTime DateTimeTrue {get;set; }
        public int DayVal { get; set; }
    }
}