﻿using System;
namespace AugmentedSchedulingOptano.Library
{
    public class TuningObjectTabu
    {
        internal double[] TabuLength { get; set; } = { 0.01};
        internal int[] IntesificationInterval { get; set; } = { 50, 75};


    }
}
