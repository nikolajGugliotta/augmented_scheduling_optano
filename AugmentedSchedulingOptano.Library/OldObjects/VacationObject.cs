﻿using System;
namespace AugmentedSchedulingOptano.Library.Models
{
    public class VacationObject
    {
        public string VacationId { get; set; }
        public string EmployeeId { get; set; }
        public string Date { get; set; }
        public string AbsenceType { get; set; }
        public string Status { get; set; }

        //Used for ALNS
        public DateTime VacaDateTrue { get; set; }
    }
}
