﻿using System;
using AugmentedSchedulingOptano.Library.Models;

namespace AugmentedSchedulingOptano.Library.Heuristics.TABU
{
    internal class QueueObject : IEquatable<QueueObject>
    {
        public string EmployeeID { get; internal set; }
        public string TimeSheetID { get; internal set; }
        public bool CurrentlyTabu { get; internal set; }

        public QueueObject()
        {
        }
    
        public QueueObject(string timeSheetId)
        {
            this.TimeSheetID = timeSheetId;
        }

        public QueueObject(string timeSheetId, string employeeId )
        {
            this.TimeSheetID = timeSheetId;
            this.EmployeeID = employeeId;
        }

        public bool Equals(QueueObject other)
        {
            if(this.TimeSheetID == other.TimeSheetID && this.EmployeeID == other.EmployeeID)
            {
                return true;
            }
            return false;
        }
    }

}