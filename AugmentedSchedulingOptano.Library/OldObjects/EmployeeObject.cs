using System;
using System.Collections.Generic;
using System.Linq;

namespace AugmentedSchedulingOptano.Library.Models
{
    public class EmployeeObject : ICloneable
    {
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public string DefaultHourSal { get; set; } // Not used in ALNS
        //public string NominalHours { get; set; } //Not used in ALNS
        public List<string> EmployeeGroups { get; set; } = new List<string>();
        public List<VacationObject> Vacations { get; set; } = new List<VacationObject>();
        public List<AvailabilityObject> AvailabilitiesGams { get; set; } = new List<AvailabilityObject>(); // Not used in ALNS
        public List<AvailabilityObject> Availabilities { get; set; } = new List<AvailabilityObject>();
        public List<AvailabilityObject> Unavailabilities { get; set; } = new List<AvailabilityObject>();


        //public List<DateTime> UnAvailableDates { get; set; } = new List<DateTime>();
        //public double AcceptedRequests { get; set; }
        public double HourlySalary { get; set; }
        public double ContractedHours { get; set; }
        public double Priority { get; set; }

        //key = timesheet.id, value = timesheet
        //key = shift date, value = list of timesheet.id

        //public int ChangeCounter { get; set; }

        //Modified during heuristic runtime:
        public List<double> WeeklyHours { get; set; } = new List<double>();
        public Dictionary<DateTime, List<string>> ShiftsBasedOnDates { get; set; } = new Dictionary<DateTime, List<string>>();
        public Dictionary<string, TimeSheetObject> AssignedShifts { get; set; } = new Dictionary<string, TimeSheetObject>();
        public double AcceptedAvailabilityHours { get; set; }
		public List<int> WorkWeekends = new List<int>();

       

        public object Clone()
        {
            EmployeeObject other = new EmployeeObject();

            //Shallow copy ok
            other.EmployeeName = EmployeeName;
            other.EmployeeId = EmployeeId;
            other.DefaultHourSal = DefaultHourSal;
            other.EmployeeGroups = EmployeeGroups;
            other.Vacations = Vacations;
            other.AvailabilitiesGams = AvailabilitiesGams;
            other.Availabilities = Availabilities;
            other.Unavailabilities = Unavailabilities;

            other.HourlySalary = HourlySalary;
            other.ContractedHours = ContractedHours;
            other.Priority = Priority;

            //Deep Copy required
            other.WeeklyHours = new List<double>(WeeklyHours);

            other.ShiftsBasedOnDates = new Dictionary<DateTime, List<string>>(ShiftsBasedOnDates.Count, ShiftsBasedOnDates.Comparer);
            foreach (KeyValuePair<DateTime, List<string>> entry in ShiftsBasedOnDates)
            {
                List<string> stringList = new List<string>(entry.Value.Count);

                foreach(string shift in entry.Value)
                {
                    stringList.Add(String.Copy(shift));
                }
                other.ShiftsBasedOnDates.Add(entry.Key, stringList);
            }

            other.AssignedShifts = new Dictionary<string, TimeSheetObject>(AssignedShifts.Count, AssignedShifts.Comparer);
            foreach(KeyValuePair<string, TimeSheetObject> entry in AssignedShifts)
            {
                other.AssignedShifts.Add(entry.Key,(TimeSheetObject) entry.Value.Clone());
            }

            other.AcceptedAvailabilityHours = AcceptedAvailabilityHours;

            other.WorkWeekends = new List<int>(WorkWeekends);

            
            return other;
        }

    }
}
