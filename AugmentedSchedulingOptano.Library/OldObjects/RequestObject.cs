﻿namespace AugmentedSchedulingOptano.Library.Models
{

    public class RequestObject
    {
        public string Id { get; set; }
        public string TimesheeId { get; set; }
        public string EmployeeId { get; set; }
    }
}
