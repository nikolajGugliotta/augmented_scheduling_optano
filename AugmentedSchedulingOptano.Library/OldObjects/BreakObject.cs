﻿using System.Collections.Generic;
namespace AugmentedSchedulingOptano.Library.Models
{
    public class BreakObject
    {
        public string Id { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public int DutyLength { set; get; }
        public int PauseLength { set; get; }
        public int WeekDaysEnum { set; get; }
        public int SecondaryWeekDaysEnum { set; get; }
        public int TypeEnum { set; get; }
        public int PauseStart { set; get; }
        public bool OnlyForecast { set; get; }
        public List<string> DaysActive { get; set; } = new List<string>();
        public List<string> EmployeeGroups { get; set; } = new List<string>();
    }
}