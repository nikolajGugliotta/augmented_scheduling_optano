﻿namespace AugmentedSchedulingOptano.Library.Models
{
    public class TuningParametersObject
    {
        public double Alpha { get; set; }
        public double Beta { get; set; }
        public double Gamma { get; set; }
        public double Epsilon { get; set; }
        public double DeltaPlusL1 { get; set; }
        public double DeltaPlusL2 { get; set; }
        public double DeltaMinusL1 { get; set; }
        public double DeltaMinusL2 { get; set; }
        public double LimitPlusL1 { get; set; }
        public double LimitMinusL1 { get; set; }

        public double GoodReward { get; set; }
        public double NormalReward { get; set; }
        public double BadReward { get; set; }

        public double R { get; set; }

        public double Temperature { get; set; }
        public double TempChange { get; set; }
    }
}
