﻿namespace AugmentedSchedulingOptano.Library.Models
{
    public class UnionRuleObject
    {
        public string Title { get; set; }
        public string TypeEnum { get; set; }
        public string Value { get; set; }
        public string TimeConditionValue { get; set; }
        public string TimeConditionTypeEnum { get; set; }
        public string DaysEnum { get; set; }
        public bool UsedForScalars { get; set; }
    }
}
