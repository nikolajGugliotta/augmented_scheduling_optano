﻿using System;
using System.Collections.Generic;
namespace AugmentedSchedulingOptano.Library.Models
{
    public static class EmployeeExtensions
    {
        internal static void CreateUnAvailabilityList(this EmployeeObject employee)
        {
            foreach (VacationObject vaca in employee.Vacations)
            {
                if (vaca.Status.Equals("3"))
                {
                    DateTime date = vaca.VacaDateTrue.Date;
                    employee.Unavailabilities.Add(new AvailabilityObject() { Availability = "0", TrueStartTime = date, TrueEndTime = date.AddDays(1).AddTicks(-1) });
                    //employee.UnAvailableDates.Add(date);
                }
            }
            foreach (AvailabilityObject avail in employee.AvailabilitiesGams)
            {
                if (avail.Availability.Equals("0"))
                {
                    DateTime date = avail.TrueStartTime.Date;
                    //employee.UnAvailableDates.Add(date);
                }
            }
        }

        internal static bool CheckShiftOverlap(this EmployeeObject employee, TimeSheetObject sheet)
        {
            bool overLap = false;

            DateTime startDate = sheet.TrueStartTime.Date;
            DateTime endDate = sheet.TrueEndTime.Date;

            foreach (TimeSheetObject assignedSheet in employee.AssignedShifts.Values)
            {
                overLap = employee.CalcOverlap(assignedSheet.TrueStartTime.Date, sheet);
                if (overLap)
                {
                    return overLap;
                }
                overLap = employee.CalcOverlap(assignedSheet.TrueEndTime.Date, sheet);
                if (overLap)
                {
                    return overLap;
                }
            }

            return overLap;
        }

        internal static bool CalcOverlap(this EmployeeObject employee, DateTime date, TimeSheetObject sheet)
        {
            bool overLap = false;
            if (employee.ShiftsBasedOnDates.ContainsKey(date))
            {
                employee.ShiftsBasedOnDates.TryGetValue(date, out List<string> sheetKeys);

                DateTime assignedSheetStartTime;
                DateTime assignedSheetEndTime;

                for (int i = 0; i < sheetKeys.Count; i++)
                {
                    assignedSheetStartTime = employee.AssignedShifts[sheetKeys[i]].TrueStartTime;
                    assignedSheetEndTime = employee.AssignedShifts[sheetKeys[i]].TrueEndTime;

                    overLap = assignedSheetStartTime < sheet.TrueEndTime && sheet.TrueStartTime < assignedSheetEndTime;

                    if (overLap)
                    {
                        //Console.WriteLine("Found overlap. " + assignedSheetStartTime+ " < " + sheet.TrueEndTime +" and " + sheet.TrueStartTime + " < "+assignedSheetEndTime + " sheet : " + sheet.PositionId + " assign : " +AssignedShifts[sheetKeys[i]].PositionId);
                        return overLap;
                    }
                    overLap = assignedSheetStartTime == sheet.TrueStartTime && sheet.TrueEndTime == assignedSheetEndTime;
                    if (overLap)
                    {
                        //Console.WriteLine("Found overlap. " + assignedSheetStartTime + " == " + sheet.TrueEndTime + " and " + sheet.TrueStartTime + " == " + assignedSheetEndTime+ " : " + sheet.PositionId+ " assign : " + AssignedShifts[sheetKeys[i]].PositionId);
                        return overLap;
                    }
                }
            }
            return overLap;
        }

        internal static bool GetAccumulatedShiftOnDay(this EmployeeObject employee, TimeSheetObject sheet, string item1)
        {
            double allowedHours = double.Parse(item1);
            bool allowed = true;
            double currentlyAssignedHoursDayOne;
            double currentlyAssignedHoursDayTwo = 0;
            if(sheet.ShiftOverlapsMidnight)
            {
                currentlyAssignedHoursDayOne = sheet.TrueEndTime.Date.Subtract(sheet.TrueStartTime).TotalHours;
                currentlyAssignedHoursDayTwo = sheet.TrueEndTime.Subtract(sheet.TrueEndTime.Date).TotalHours;
            }
            else
            {
                currentlyAssignedHoursDayOne = sheet.Duration;
            }
          
            // Shifts starting on the day
            if (employee.ShiftsBasedOnDates.ContainsKey(sheet.TrueStartTime.Date))

            {
                employee.ShiftsBasedOnDates.TryGetValue(sheet.TrueStartTime.Date, out List<string> sheetKeys);

                for (int i = 0; i < sheetKeys.Count; i++)
                {
                    employee.AssignedShifts.TryGetValue(sheetKeys[i], out TimeSheetObject currentSheet);

                    if (currentSheet.ShiftOverlapsMidnight)
                    {
                        currentlyAssignedHoursDayOne += currentSheet.TrueEndTime.Date.Subtract(currentSheet.TrueStartTime).TotalHours;
                        currentlyAssignedHoursDayTwo += currentSheet.TrueEndTime.Subtract(currentSheet.TrueEndTime.Date).TotalHours;
                    }
                    else
                    {
                        currentlyAssignedHoursDayOne += currentSheet.Duration;
                    }
                }

                if (currentlyAssignedHoursDayOne > allowedHours)
                {
                    allowed = false;
                    return allowed;
                }
            }

            // Shifts overlapping midnigth from day before
            if (employee.ShiftsBasedOnDates.ContainsKey(sheet.TrueStartTime.Date.AddDays(-1)))
            {
                employee.ShiftsBasedOnDates.TryGetValue(sheet.TrueStartTime.Date.AddDays(-1), out List<string> sheetKeys);

                for (int i = 0; i < sheetKeys.Count; i++)
                {
                    employee.AssignedShifts.TryGetValue(sheetKeys[i], out TimeSheetObject currentSheet);

                    if (currentSheet.ShiftOverlapsMidnight)
                    {
                        currentlyAssignedHoursDayOne += currentSheet.TrueEndTime.Subtract(currentSheet.TrueEndTime.Date).TotalHours;

                    }
                }


                if (currentlyAssignedHoursDayOne > allowedHours)
                {
                    allowed = false;
                    return allowed;
                }
            }

            if (sheet.ShiftOverlapsMidnight)
            {
                // Shifts starting on the day assing to employee
                if (employee.ShiftsBasedOnDates.ContainsKey(sheet.TrueStartTime.Date.AddDays(1)))
                {
                    employee.ShiftsBasedOnDates.TryGetValue(sheet.TrueStartTime.Date.AddDays(1), out List<string> sheetKeys);

                    for (int i = 0; i < sheetKeys.Count; i++)
                    {
                        employee.AssignedShifts.TryGetValue(sheetKeys[i], out TimeSheetObject currentSheet);

                        if (currentSheet.ShiftOverlapsMidnight)
                        {
                            currentlyAssignedHoursDayTwo += currentSheet.TrueEndTime.Date.Subtract(currentSheet.TrueStartTime).TotalHours;
                        }
                        else
                        {
                            currentlyAssignedHoursDayTwo += currentSheet.Duration;
                        }
                    }

                    if (currentlyAssignedHoursDayTwo > allowedHours)
                    {
                        allowed = false;
                        return allowed;
                    }
                }
            }

            return allowed;
        }

        //TODO needs to be tested 
        internal static bool GetWorkDaysForWeek(this EmployeeObject employee, DateTime trueStartTime, string item1)
        {
            bool tooManyWorkDays = false;

            if (employee.ShiftsBasedOnDates.Count >= int.Parse(item1))
            {
                int workDayCount = 1;

                DayOfWeek day = trueStartTime.DayOfWeek;
                DateTime startTime = trueStartTime;

                while (day != DayOfWeek.Monday)
                {
                    day = startTime.AddDays(-1).DayOfWeek;
                    startTime = startTime.AddDays(-1);

                }
                //Time from day to monday
                DateTime endTime = startTime.AddDays(6);
                for (DateTime i = startTime; i <= endTime; i = i.AddDays(1))
                {
                    if (employee.ShiftsBasedOnDates.ContainsKey(i.Date))
                    {
                        workDayCount++;
                    }
                }
                if (workDayCount > int.Parse(item1))
                {
                    tooManyWorkDays = true;
                }
            }
            return tooManyWorkDays;

        }

        //Method to remove shift from an employee
        //Todo needs to be tested
        internal static bool UnassignSheet(this EmployeeObject employee, TimeSheetObject sheet)
        {
            bool couldBeRemoved = true;
            //Console.WriteLine("Assigned count: " + AssignedShifts.Count);
            //int shiftCount = 0;
            //for (int i = 0; i < ShiftsBasedOnDates.Count; i++){
            //    shiftCount += ShiftsBasedOnDates.ElementAt(i).Value.Count;
            //}
            // Console.WriteLine("ShiftsBased on count: " + shiftCount);



            DateTime workDay = sheet.TrueStartTime;
            DateTime workDayEnd = sheet.TrueEndTime;

            //TODO might be a problem with dublicates
            //Should be tested 
            if (employee.ShiftsBasedOnDates.ContainsKey(workDay.Date))
            {
                // Console.WriteLine("assined shifts count: " +AssignedShifts.Count);
                employee.AssignedShifts.Remove(sheet.SheetId);
                // Console.WriteLine("assined shifts count: " + AssignedShifts.Count);

                int listCount = employee.ShiftsBasedOnDates[workDay.Date].Count;
                if (listCount <= 1)
                {
                    employee.ShiftsBasedOnDates.Remove(workDay.Date);
                }
                else
                {
                    employee.ShiftsBasedOnDates[workDay.Date].Remove(sheet.SheetId);
                }
                //  Console.WriteLine("COUNTER IS :"+ChangeCounter);
                //employee.ChangeCounter--;
                // Console.WriteLine("COUNTER IS now:" + ChangeCounter);
            }
            else
            {
                //Console.WriteLine("ERROR(?) tries to remove shift on date: "+workDay.Date);
                couldBeRemoved = false;
            }
            return couldBeRemoved;

        }
        //Method to assign time sheet to an employee
        internal static bool AssignSheet(this EmployeeObject employee, TimeSheetObject sheet)
        {
            bool couldBeAdded = true;
            if (!employee.AssignedShifts.ContainsKey(sheet.SheetId))
            {
                employee.AssignedShifts.Add(sheet.SheetId, sheet);
                DateTime workDay = sheet.TrueStartTime;
                DateTime workDayEnd = sheet.TrueEndTime;

                if (employee.ShiftsBasedOnDates.ContainsKey(workDay.Date))
                {
                    employee.ShiftsBasedOnDates[workDay.Date].Add(sheet.SheetId);
                }
                else
                {
                    employee.ShiftsBasedOnDates.Add(workDay.Date, new List<string>() { sheet.SheetId });
                }

                //employee.ChangeCounter++;
            }
            else
            {
                couldBeAdded = false;
            }
            return couldBeAdded;
        }
    }
}
