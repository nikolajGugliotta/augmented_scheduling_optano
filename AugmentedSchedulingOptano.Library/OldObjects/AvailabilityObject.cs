﻿using System;
namespace AugmentedSchedulingOptano.Library.Models
{
    public class AvailabilityObject
    {
        public string Availability { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }

        //Used for ALNS
        public DateTime TrueStartTime { get; set; }
        public DateTime TrueEndTime { get; set; }
    }
}
