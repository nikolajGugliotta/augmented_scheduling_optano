﻿namespace AugmentedSchedulingOptano.Library.Models
{
    public static class TuningParametersExtensions
    {
        public static void AssignValues(this TuningParametersObject tun, double alpha, double beta, 
                                        double gamma, double epsilon, double deltaPlusOne, double deltaMinusOne,
                                        double deltaPlusTwo, double deltaMinusTwo, double limitPlusOne, double limitMinusOne,
                                       double goodReward, double normalReward, double badReward, double r, double temperature, double tempChange)
        {
            tun.Alpha = alpha;// 0.5; // Accepted availability
            tun.Beta = beta;//20; // Demand fullfillment

            tun.Gamma = gamma;// 0; // Salary costs

            tun.Epsilon = epsilon;//4; // Shift requests.

            tun.DeltaPlusL1 = deltaPlusOne;//1;
            tun.DeltaMinusL1 = deltaMinusOne;//1;
            tun.DeltaPlusL2 = deltaPlusTwo;//2;
            tun.DeltaMinusL2 = deltaMinusTwo;//2;

            tun.LimitPlusL1 = limitPlusOne;//4;
            tun.LimitMinusL1 = limitMinusOne;//4;

            tun.GoodReward = goodReward;//30;
            tun.NormalReward = normalReward;;//10;
            tun.BadReward = badReward;//1;

            tun.R = r;//0.25;

            tun.Temperature = temperature;//100;
            tun.TempChange = tempChange;// 0.9992;
        }
    }
}
