﻿using System;
using System.Collections.Generic;

namespace AugmentedSchedulingOptano.Library.Models
{
    public class SetObject
    {
        public List<string> EmpSet { get; set; } = new List<string>();
        public List<string> EmpGroupSet { get; set; } = new List<string>();
        public List<string> PositionSet { get; set; } = new List<string>();
        public List<string> DaySet { get; set; } = new List<string>();
        public List<string> EndDaySet { get; set; } = new List<string>();
        public List<string> WeekendSet { get; set; } = new List<string>();
        public List<string> StartYear { get; set; } = new List<string>();
        public List<DateTime> TrueDaySet { get; set; } = new List<DateTime>();
        public DateTime FirstDayInRange { get; set; }
        public DateTime LastDayInRange { get; set; }
    }
}
