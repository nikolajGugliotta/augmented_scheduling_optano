﻿using System;
using System.Collections.Generic;

namespace AugmentedSchedulingOptano.Library.Models
{
    public class DataObject
    {
        public List<TimeSheetObject> TimesheetObjects { set; get; } = new List<TimeSheetObject>();
        public List<EmployeeObject> EmployeeObjects { set; get; } = new List<EmployeeObject>();
        public List<TimeSheetObject> TimesheetALNSObjectsSOL { set; get; } = new List<TimeSheetObject>();
        public List<EmployeeObject> EmployeeALNSObjectsSOL { set; get; } = new List<EmployeeObject>();
        //public List<VacationObject> VacationObjects { set; get; } = new List<VacationObject>();
        public List<UnionRuleObject> UnionRuleObjects { set; get; } = new List<UnionRuleObject>();
        public List<BreakObject> BreakObjects { set; get; } = new List<BreakObject>();
        public SetObject SetObjects { set; get; } = new SetObject();
        public string FallBackSalary { set; get; }
        public string FirstPlanDay { set; get; }
        public string FirstPlanWeek { set; get; }
        public int DayCount { get; set; }
        public int WeekCount { get; set; }
        public string PortalName { get; set; }
        public string StartRange { get; set; }
        public string EndRange { get; set; }
    }
}
