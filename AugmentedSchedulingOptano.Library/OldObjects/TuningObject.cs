﻿using System;

namespace AugmentedSchedulingOptano.Library.Models
{
    public class TuningObject
    {
        internal double[] R { get; set; } = { 0.01, 0.125, 0.25, 0.375, 0.5 };
        internal double[] Temperature { get; set; } = { 50, 75, 100, 125, 150 };
        internal double[] TempChange { get; set; } = { 0.5, 0.75, 0.9992, 1, 1.25 };
        internal int[] MaxNumberRemoved { get; set; } = { 50, 60, 70, 80, 90 };
        internal double[] InitialRemoval { get; set; } = { 0.025, 0.05, 0.1, 0.15, 0.25 };
        internal double[] EndRemoval { get; set; } = { 0.01, 0.02, 0.03, 0.05, 0.1 };
        internal double[] LocalIterationCounterPercentage { get; set; } = { 0.01, 0.025, 0.05, 0.075, 0.1 };
        internal double[] LocalIterationCounter2Percentage { get; set; } = { 0.1, 0.2, 0.3, 0.4, 0.5 };
    }
}