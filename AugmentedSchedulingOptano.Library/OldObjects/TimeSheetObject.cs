using System;
using System.Collections.Generic;
namespace AugmentedSchedulingOptano.Library.Models
{
    public class TimeSheetObject : ICloneable
    {
        private TimeSheetObject sheet;

        public string SheetId { get; set; }
        public string PositionId { get; set; }
        public string EmployeeGroupId { get; set; }
       // public string StartYear { get; set; }
        //public string StartMonth { get; set; }
        public string StartDay { get; set; }
        //public string StartDayTrue { get; set; }
        //public string StartHour { get; set; }
        //public string StartMinute { get; set; }
        //public string FakeStartMinute { get; set; }
        //public string EndYear { get; set; }
        //public string EndMonth { get; set; }
        public string EndDay { get; set; }
        //public string EndDayTrue { get; set; }
        public string EndHour { get; set; }
        //public string EndMinute { get; set; }
        //public string FakeEndMinute { get; set; }
        public bool InUse { get; set; }
        public List<RequestObject> Requests { get; set; } = new List<RequestObject>();

        //Used in ALNS
        public DateTime TrueStartTime { get; set; }
        public DateTime TrueEndTime { get; set; }
        //TODO Fix getting the weekend days from the db
        public bool IsWeekend { get; set; }
        public double Duration { get; set; }
        public bool ShiftOverlapsMidnight { get; set; }

        //Fields modified during heuristic runtime
        public string EmployeeId { get; set; }
		public bool RequestFulfilled { get; set; } //True if this shift is assigned to an emp who requested it.

        public TimeSheetObject()
        {
        }

        public TimeSheetObject(TimeSheetObject sheet)
        {
           
                this.sheet = sheet;
           
        }

        public object Clone()
        {
            TimeSheetObject other = (TimeSheetObject) this.MemberwiseClone();
            other.EmployeeId = String.Copy(EmployeeId);
            return other;
        }
    }
}
