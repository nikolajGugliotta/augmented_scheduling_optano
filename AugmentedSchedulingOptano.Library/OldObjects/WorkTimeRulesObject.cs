﻿using System;
namespace AugmentedSchedulingOptano.Library.Models
{
    internal class WorkTimeRulesObject
    {
        public Tuple<string, string> BreakBetweenShifts { get; internal set; }
        // max consecutive work days, activator
        public Tuple<string, string> MaxConsecWorkDays { get; internal set; }
        // public Tuple<string, string> WorkHoursPrWeek { get; internal set; }
        public Tuple<string, string> ShiftDurationPrDay { get; internal set; }
        public Tuple<string, string> MaxWorkDaysPrwk { get; internal set; }
        // activator, period length, max hours
        public Tuple<string, string, string> MaxHoursInRange { get; internal set; }
        // activator, period length, max days
        public Tuple<string, string, string> MaxDaysInRange { get; internal set; }
        // Nr of weekends off, in period of weekends, activator, dayEnum
        public Tuple<string, string, string, string> FreqWeekendOff { get; internal set; }
        public Tuple<string, string, string> MinHoursInRange { get; internal set; }
    }
}