﻿using AugmentedSchedulingOptano.Library.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;

namespace AugmentedSchedulingOptano.Library.OldTools
{
    class ToolsOld
    {
        internal double GetAvailabilityOverlap(List<AvailabilityObject> availabilityObjs, TimeSheetObject timeSheetObj)
        {

            double overlap = 0;

            foreach (AvailabilityObject availability in availabilityObjs)
            {
                if (availability.TrueStartTime.Ticks < timeSheetObj.TrueEndTime.Ticks && timeSheetObj.TrueStartTime.Ticks < availability.TrueEndTime.Ticks)
                {
                    DateTime maxStart = new DateTime(Math.Max(availability.TrueStartTime.Ticks, timeSheetObj.TrueStartTime.Ticks));
                    DateTime minEnd = new DateTime(Math.Min(availability.TrueEndTime.Ticks, timeSheetObj.TrueEndTime.Ticks));

                    overlap += minEnd.Subtract(maxStart).TotalHours;
                    //TODO Remove when validated
                    if (overlap > timeSheetObj.Duration)
                    {
                        overlap = timeSheetObj.Duration;
                        // throw new Exception("Availability overlap larger than shift duration in");
                    }
                }
            }

            return overlap;
        }
        internal void FixAvailabilities(List<EmployeeObject> employeesObj, string portalName, string startRange, string endRange)
        {
            Random rand = new Random();
            foreach (EmployeeObject emp in employeesObj)
            {
                foreach (AvailabilityObject avail in emp.Availabilities)
                {
                    DateTime startTime = avail.TrueStartTime;
                    DateTime endTime = avail.TrueEndTime;

                    if (endTime < startTime)
                    {
                        avail.TrueEndTime = avail.TrueEndTime.AddDays(1);
                    }
                }
                foreach (AvailabilityObject unAvail in emp.Unavailabilities)
                {
                    DateTime startTime = unAvail.TrueStartTime;
                    DateTime endTime = unAvail.TrueEndTime;

                    if (endTime < startTime)
                    {
                        unAvail.TrueEndTime = unAvail.TrueEndTime.AddDays(1);
                    }
                }
            }

            int counter = 0;
            if (portalName == "olivia")
            {
                DateTime firstPlanDay = Convert.ToDateTime(startRange);
                DateTime lastPlanDay = Convert.ToDateTime(endRange);
                int daysInPeriod = lastPlanDay.Subtract(firstPlanDay).Days;
                int choice = 0;

                foreach (EmployeeObject emp in employeesObj)
                {
                    for (int i = 0; i < daysInPeriod; i++)
                    {
                        DateTime startTime;
                        DateTime endTime;
                        choice = rand.Next(12);
                        switch (choice)
                        {

                            case 0: // 7:00 - 15:00
                                startTime = firstPlanDay + new TimeSpan(i, 7, 0, 0);
                                endTime = firstPlanDay + new TimeSpan(i, 15, 0, 0);
                                emp.Availabilities.Add(new AvailabilityObject() { Availability = "0", TrueStartTime = startTime, TrueEndTime = endTime });
                                counter++;
                                break;
                            case 1: // 12:00 - 20:00
                                startTime = firstPlanDay + new TimeSpan(i, 12, 0, 0);
                                endTime = firstPlanDay + new TimeSpan(i, 20, 0, 0);
                                emp.Availabilities.Add(new AvailabilityObject() { Availability = "0", TrueStartTime = startTime, TrueEndTime = endTime });
                                counter++;
                                break;
                            case 2: // 15 - 23:00
                                startTime = firstPlanDay + new TimeSpan(i, 7, 0, 0);
                                endTime = firstPlanDay + new TimeSpan(i, 15, 0, 0);
                                emp.Availabilities.Add(new AvailabilityObject() { Availability = "0", TrueStartTime = startTime, TrueEndTime = endTime });
                                counter++;
                                break;

                            default:
                                break;
                        }
                    }
                }

            }
            Console.WriteLine("Availability is now fixed!!!!! (" + counter + ")");
        }

        internal void FixNomialHours(List<EmployeeObject> employeeObjects, string portalName, List<TimeSheetObject> sheetObjs)
        {
            Random rand = new Random();
            if (portalName.Equals("koed"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_154988":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_155000":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_155798":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_156955":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_159120":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_159198":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_159668":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_161992":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_169130":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_218318":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_410927":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_471663":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_620796":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_676674":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_679099":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_764305":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_899630":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_914478":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_155799":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_155801":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_155804":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155805":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155806":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155809":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_155812":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155813":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155815":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155816":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_155817":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_155818":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_155822":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_155823":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_156400":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_156401":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_156402":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_156404":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_158111":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_158541":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_158542":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_159202":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_159392":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_159641":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_160119":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_160122":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_160161":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_160952":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_160953":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_160993":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_161031":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_161039":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_162309":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_162942":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_163006":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_164651":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_165648":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_166885":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_167467":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_167471":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_168418":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_170112":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_176349":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_176350":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_176805":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_182566":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_182985":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_187381":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_187868":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_188085":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_188088":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_188094":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_188099":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_188100":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_197167":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_221683":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_221684":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_229278":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_230497":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_232217":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_234709":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_243921":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_245743":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_246019":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_246128":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_250883":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_271665":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_273911":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_276999":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_278190":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_288146":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_288643":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_293900":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_300232":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_300581":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_306457":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_316947":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_322924":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_327578":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_327585":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_358352":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_358820":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_358822":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_364378":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_377464":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_384228":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_384969":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_385830":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_386319":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_407931":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_410923":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_410925":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_410926":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_410931":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_412767":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_426215":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_438433":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_445468":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_469137":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_487241":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_490718":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_493131":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_541663":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_546868":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_564958":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_617158":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_620787":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_667174":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_667180":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_667301":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_680491":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_685859":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_689012":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_697349":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_717546":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_730629":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_732349":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_737180":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_767307":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_803039":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_807655":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_825588":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_829171":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_838290":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_849933":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_865647":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_880053":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_899667":
                                emp.ContractedHours = 36;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("olivia"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_150106":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_175147":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_197421":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_204525":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_288347":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_288378":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_323449":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_330787":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_380746":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_380748":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_380754":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_380769":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_399865":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_400072":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_401474":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_402114":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_407380":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_407432":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_407943":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_408381":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_478636":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_481857":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_481871":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_481889":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_481915":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_482260":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_482264":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_482270":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_482273":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_482293":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_482296":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_482304":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_482324":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_482332":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_482345":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_482347":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_482675":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_482769":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_482797":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_482807":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_482842":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_482852":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_482858":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_483553":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_484320":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_487295":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_494475":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_497343":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_503408":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_506565":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_510525":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_510776":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_512293":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_513531":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_515413":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_520094":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_527796":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_532257":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_533372":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_535196":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_655554":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_674832":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_674923":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_677882":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_682847":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_702502":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_714701":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_721224":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_731413":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_745529":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_746192":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_746345":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_764522":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_766203":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_785018":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_806491":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_807526":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_808490":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_816604":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_821405":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_827006":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_830864":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_841943":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_843261":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_843913":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_846633":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_846901":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_847411":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_859259":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_861307":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_861514":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_862590":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_863918":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_869276":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_871619":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_871709":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_871737":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_873008":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_873639":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_876153":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_882005":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_882044":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_892056":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_895431":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_900347":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_900366":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_915775":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_919489":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_935500":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_941444":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_941718":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_162703":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_162706":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_162726":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_175139":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_175152":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_183455":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_202595":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_206424":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_209403":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_223355":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_226554":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_227535":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_233883":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_240257":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_304330":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_333890":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_370660":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_380750":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_380751":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_380753":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_380755":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_380756":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_380764":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_380770":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_380771":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_380773":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_401479":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_402167":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_406741":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_407236":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_428308":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_481901":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_481912":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_482267":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_482298":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_482306":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_482329":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_482330":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_482335":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_482656":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_482667":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_482715":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_482778":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_482800":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_482866":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_487312":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_487374":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_491839":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_493069":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_495942":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_497524":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_498417":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_501464":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_505527":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_506308":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_510074":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_512296":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_512337":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_516300":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_517907":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_518004":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_519229":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_519240":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_525461":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_526536":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_526792":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_527839":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_527850":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_528704":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_528714":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_536098":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_541519":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_546953":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_552831":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_571382":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_598915":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_599598":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_602579":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_603548":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_605304":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_608849":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_613837":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_677774":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_698506":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_700065":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_708738":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_719742":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_722484":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_725279":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_744740":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_747322":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_752035":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_753910":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_760182":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_760266":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_774378":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_809944":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_809965":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_829228":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_832426":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_832434":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_834775":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_838974":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_841205":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_844257":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_847468":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_854419":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_854884":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_858643":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_863963":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_870483":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_871668":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_873669":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_876142":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_879242":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_883434":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_886568":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_918433":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_935570":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_940832":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_943061":
                                emp.ContractedHours = 28;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("42raw"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_24972":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_24986":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_25262":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_25268":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_25473":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_26110":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_26142":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_26143":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_26598":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_29835":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_32623":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_33646":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_33688":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_35324":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_35365":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_35834":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_35866":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_145226":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_154319":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_154390":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_154443":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_156094":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_156952":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_158312":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_158425":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_158427":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_163171":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_166163":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_168177":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_169255":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_181089":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_181220":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_181440":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_182972":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_183593":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_186009":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_186743":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_191889":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_191904":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_196123":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_202471":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_215036":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_223877":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_224688":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_226523":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_233166":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_233168":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_234198":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_244377":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_246987":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_247630":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_247631":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_249585":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_252714":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_254866":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_264330":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_264339":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_302221":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_303238":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_304548":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_304550":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_304554":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_315382":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_327213":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_346338":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_358633":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_369592":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_407818":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_413929":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_429167":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_439599":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_441914":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_447568":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_447714":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_463446":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_492891":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_492893":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_498281":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_507191":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_672269":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_694540":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_694830":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_698525":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_699405":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_706993":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_709328":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_711966":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_714938":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_717326":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_748045":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_777381":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_808130":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_828132":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_841178":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_850027":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_145224":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_143514":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_140205":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_138281":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_124375":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_110763":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_109999":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_109833":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_109450":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_106351":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_106321":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_106291":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_106073":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_105327":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_97092":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_96941":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_96329":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_95524":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_92784":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_92140":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_90489":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_88884":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_88713":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_87074":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_86888":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_85925":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_85090":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_83178":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_83176":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_82426":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_82251":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_82212":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_82048":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_81979":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_81396":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_80353":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_80352":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_80344":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_80009":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_79979":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_79973":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_79919":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_79394":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_79381":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_78394":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_78150":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_78145":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_70613":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_70485":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_64352":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_62091":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_60165":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_59950":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_59491":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_59270":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_58060":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_55572":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_53126":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_52567":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_51744":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_51514":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_50345":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_40674":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_40087":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_39284":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_39243":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_36811":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_36789":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_36683":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_36273":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_36261":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_29820":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_29543":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_27226":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_26599":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_26068":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_25960":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_25893":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_25839":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_6208":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_5672":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_5532":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_5146":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_5145":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_5144":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_5143":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_4783":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_4541":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_4196":
                                emp.ContractedHours = 40;
                                break;
                            case "emp_4064":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_4002":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_3996":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_3936":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_3880":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_3876":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_3850":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_3814":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_3679":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_3676":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_3675":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_3674":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_3673":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_3672":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_3671":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_3670":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_3669":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_3668":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_3667":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_3665":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_33647":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_3650":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_908655":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_890497":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_879907":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_864194":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_704761":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_678807":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_562108":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_557013":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_517550":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_461195":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_455151":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_455107":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_444441":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_417727":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_349771":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_344146":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_307534":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_261101":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_260407":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_259749":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_259748":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_258224":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_253340":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_243776":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_210026":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_208568":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_207927":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_202757":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_201315":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_192442":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_185030":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_185015":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_184926":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_184705":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_182229":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_172377":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_151306":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_151124":
                                emp.ContractedHours = 41;
                                break;
                            case "emp_150917":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_149968":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_149938":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_147741":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_146817":
                                emp.ContractedHours = 19;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("andersons"))
            {

                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_261542":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_261550":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_261554":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_261584":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_261587":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_261614":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_261617":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_261621":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_261652":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_261669":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_261675":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_261679":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_261682":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_261689":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_261701":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_261703":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_261720":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_261725":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_261756":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_261785":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_261791":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_308768":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_388134":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_388135":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_388136":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_391775":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_397960":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_430500":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_430502":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_430503":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_533823":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_533824":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_533891":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_559349":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_639685":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_750780":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_750782":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_750785":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_750787":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_844941":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_844996":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_844997":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_846701":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_846702":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_846703":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_846704":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_847236":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_847237":
                                emp.ContractedHours = 39;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("admiralhotel"))
            {

                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_43354":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_43358":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_43369":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_43379":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_43380":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_43422":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_43425":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_61518":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_70464":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_77894":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_82020":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_104912":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_138350":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_147235":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_281052":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_43351":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_270072":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_233326":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_233327":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_737489":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_555826":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_549074":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_549072":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_543782":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_528360":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_528277":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_495750":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_467657":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_441817":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_394841":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_347246":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_340771":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_316897":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_315537":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_311974":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_309981":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_286885":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_917368":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_913828":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_897595":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_897590":
                                emp.ContractedHours = 20;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("falck-globalassistance"))
            {

                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_39743":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_39749":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_49921":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_72149":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_90443":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_100138":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_100151":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_141683":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_147040":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_160516":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_177949":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_195193":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_196898":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_199369":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_200090":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_205434":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_262687":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_262693":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_289527":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_315443":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_322875":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_339909":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_339912":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_339919":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_339923":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_339928":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_372990":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_372998":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_372999":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_373000":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_373001":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_377360":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_377365":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_426450":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_427988":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_428064":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_428419":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_450445":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_484865":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_509816":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_509819":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_509827":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_516651":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_517372":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_517453":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_517480":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_517525":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_517534":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_518051":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_518070":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_518085":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_523779":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_529583":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_535085":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_541324":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_541481":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_802270":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_810864":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_874528":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_874547":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_874555":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_874566":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_874579":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_874587":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_874601":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_874675":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_874683":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_906169":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_906171":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_906172":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_906178":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_906180":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_906183":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_906190":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_906196":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_906425":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_906427":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_907534":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_907537":
                                emp.ContractedHours = 39;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("helenekilde"))
            {

                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_906096":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_951615":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_950080":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_943878":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_943864":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_937367":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_937360":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_935978":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_929184":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_929142":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_926056":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_921057":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_835518":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_709713":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_604021":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_503925":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_440647":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_218489":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_208148":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_142565":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_57211":
                                emp.ContractedHours = 23;
                                break;
                        }
                    }
                }
            }
            else if (portalName.Equals("marienlyst"))
            {

                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (sheetObjs.Exists(x => x.EmployeeId == emp.EmployeeId))
                    {
                        switch (emp.EmployeeId)
                        {
                            case "emp_516550":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_534135":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_769074":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_769082":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_916592":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_944066":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_382123":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_705734":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_769052":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_739301":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_717517":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_714674":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_701754":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_699889":
                                emp.ContractedHours = 20;
                                break;
                            case "emp_671709":
                                emp.ContractedHours = 38;
                                break;
                            case "emp_611178":
                                emp.ContractedHours = 29;
                                break;
                            case "emp_569624":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_555033":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_552360":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_551379":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_539375":
                                emp.ContractedHours = 36;
                                break;
                            case "emp_532244":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_525399":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_525394":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_505157":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_488233":
                                emp.ContractedHours = 34;
                                break;
                            case "emp_453890":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_453006":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_421583":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_414649":
                                emp.ContractedHours = 25;
                                break;
                            case "emp_382146":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_382138":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_382136":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_382135":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_382134":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_382132":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_382130":
                                emp.ContractedHours = 30;
                                break;
                            case "emp_382128":
                                emp.ContractedHours = 15;
                                break;
                            case "emp_382127":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_382125":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_382122":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_382116":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_382109":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_382108":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_382107":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_382106":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_382104":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_382097":
                                emp.ContractedHours = 23;
                                break;
                            case "emp_382096":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_382094":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_382092":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_382087":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_382083":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_889526":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_882528":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_882463":
                                emp.ContractedHours = 31;
                                break;
                            case "emp_873354":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_873344":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_854058":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_853614":
                                emp.ContractedHours = 28;
                                break;
                            case "emp_851369":
                                emp.ContractedHours = 37;
                                break;
                            case "emp_850836":
                                emp.ContractedHours = 19;
                                break;
                            case "emp_846384":
                                emp.ContractedHours = 24;
                                break;
                            case "emp_803761":
                                emp.ContractedHours = 22;
                                break;
                            case "emp_791948":
                                emp.ContractedHours = 21;
                                break;
                            case "emp_778687":
                                emp.ContractedHours = 16;
                                break;
                            case "emp_796517":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_722254":
                                emp.ContractedHours = 18;
                                break;
                            case "emp_389218":
                                emp.ContractedHours = 17;
                                break;
                            case "emp_940697":
                                emp.ContractedHours = 26;
                                break;
                            case "emp_936530":
                                emp.ContractedHours = 39;
                                break;
                            case "emp_934412":
                                emp.ContractedHours = 33;
                                break;
                            case "emp_931704":
                                emp.ContractedHours = 35;
                                break;
                            case "emp_929613":
                                emp.ContractedHours = 32;
                                break;
                            case "emp_911423":
                                emp.ContractedHours = 27;
                                break;
                            case "emp_906695":
                                emp.ContractedHours = 32;
                                break;
                        }
                    }
                }
            }


        }

        internal void FixPriorities(List<EmployeeObject> employeeObjects, string portalName)
        {
            Random rand = new Random();

            if (portalName.Equals("viktoriagade"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    switch (emp.EmployeeId)
                    {
                        case "emp_459910":
                            emp.Priority = 1.35;
                            break;
                        case "emp_459913":
                            emp.Priority = 0.65;
                            break;
                        case "emp_468839":
                            emp.Priority = 1.35;
                            break;
                        case "emp_501458":
                            emp.Priority = 1.5;
                            break;
                        case "emp_685653":
                            emp.Priority = 1;
                            break;
                        case "emp_815797":
                            emp.Priority = 0.65;
                            break;
                        case "emp_906758":
                            emp.Priority = 0.55;
                            break;
                        case "emp_887563":
                            emp.Priority = 0.55;
                            break;
                        case "emp_459902":
                            emp.Priority = 1.65;
                            break;
                        case "emp_540252":
                            emp.Priority = 1.65;
                            break;
                    }
                }
            }
            else if (portalName.Equals("olivia"))
            {
                //double priority = 1;
                foreach (EmployeeObject emp in employeeObjects)
                {


                    //priority = 0.5 + rand.NextDouble();
                    //emp.Priority = priority;
                    //Console.WriteLine("case \"" + emp.EmployeeId + "\":");
                    //Console.WriteLine("\temp.Priority = " + priority + ";");
                    //Console.WriteLine("\tbreak;");

                    switch (emp.EmployeeId)
                    {
                        case "emp_150106":
                            emp.Priority = 0.849177146493074;
                            break;
                        case "emp_162736":
                            emp.Priority = 0.674384507431828;
                            break;
                        case "emp_197421":
                            emp.Priority = 1.05342492440409;
                            break;
                        case "emp_204525":
                            emp.Priority = 1.34307603530729;
                            break;
                        case "emp_223383":
                            emp.Priority = 0.645383221630651;
                            break;
                        case "emp_288347":
                            emp.Priority = 1.13836491743027;
                            break;
                        case "emp_288378":
                            emp.Priority = 1.42400867767819;
                            break;
                        case "emp_323449":
                            emp.Priority = 1.47156473387571;
                            break;
                        case "emp_330787":
                            emp.Priority = 1.29794094934964;
                            break;
                        case "emp_346697":
                            emp.Priority = 1.22252718765406;
                            break;
                        case "emp_357996":
                            emp.Priority = 1.03422713770262;
                            break;
                        case "emp_380746":
                            emp.Priority = 1.1123001941537;
                            break;
                        case "emp_380748":
                            emp.Priority = 0.825909808895509;
                            break;
                        case "emp_380754":
                            emp.Priority = 0.648439809283446;
                            break;
                        case "emp_380769":
                            emp.Priority = 1.48558931657373;
                            break;
                        case "emp_399865":
                            emp.Priority = 0.89649170376197;
                            break;
                        case "emp_400072":
                            emp.Priority = 1.06860155452443;
                            break;
                        case "emp_401474":
                            emp.Priority = 1.09695634739332;
                            break;
                        case "emp_402114":
                            emp.Priority = 1.17369922700976;
                            break;
                        case "emp_407380":
                            emp.Priority = 1.29136141705856;
                            break;
                        case "emp_407432":
                            emp.Priority = 0.884604489609881;
                            break;
                        case "emp_407943":
                            emp.Priority = 0.672352606045246;
                            break;
                        case "emp_408381":
                            emp.Priority = 0.628554435972382;
                            break;
                        case "emp_408872":
                            emp.Priority = 1.41695324886448;
                            break;
                        case "emp_478636":
                            emp.Priority = 1.1665476810497;
                            break;
                        case "emp_480739":
                            emp.Priority = 0.995926332890953;
                            break;
                        case "emp_481832":
                            emp.Priority = 1.14661898633773;
                            break;
                        case "emp_481857":
                            emp.Priority = 1.16150891625393;
                            break;
                        case "emp_481871":
                            emp.Priority = 1.25810300175012;
                            break;
                        case "emp_481889":
                            emp.Priority = 1.18781674266226;
                            break;
                        case "emp_481915":
                            emp.Priority = 1.14976785734751;
                            break;
                        case "emp_482260":
                            emp.Priority = 0.993398125513177;
                            break;
                        case "emp_482264":
                            emp.Priority = 1.34759753143769;
                            break;
                        case "emp_482270":
                            emp.Priority = 0.697931211068263;
                            break;
                        case "emp_175147":
                            emp.Priority = 0.815332968866142;
                            break;
                        case "emp_158728":
                            emp.Priority = 0.782475284432282;
                            break;
                        case "emp_482273":
                            emp.Priority = 1.10826488705737;
                            break;
                        case "emp_482293":
                            emp.Priority = 0.78181121790866;
                            break;
                        case "emp_482296":
                            emp.Priority = 1.10457388013814;
                            break;
                        case "emp_482304":
                            emp.Priority = 1.39457193431192;
                            break;
                        case "emp_482324":
                            emp.Priority = 1.01748996810871;
                            break;
                        case "emp_482332":
                            emp.Priority = 1.44973286425217;
                            break;
                        case "emp_482345":
                            emp.Priority = 1.07855789483458;
                            break;
                        case "emp_482347":
                            emp.Priority = 0.725267875578845;
                            break;
                        case "emp_482675":
                            emp.Priority = 0.632533437634135;
                            break;
                        case "emp_482769":
                            emp.Priority = 0.648141534602335;
                            break;
                        case "emp_482797":
                            emp.Priority = 0.726862687723182;
                            break;
                        case "emp_482807":
                            emp.Priority = 0.595112467228953;
                            break;
                        case "emp_482842":
                            emp.Priority = 1.18457745094065;
                            break;
                        case "emp_482852":
                            emp.Priority = 0.937698991241725;
                            break;
                        case "emp_482858":
                            emp.Priority = 1.278969638878;
                            break;
                        case "emp_483553":
                            emp.Priority = 0.878676406749839;
                            break;
                        case "emp_484320":
                            emp.Priority = 1.49590784031707;
                            break;
                        case "emp_487295":
                            emp.Priority = 0.800613311259362;
                            break;
                        case "emp_494475":
                            emp.Priority = 1.35957830299604;
                            break;
                        case "emp_497343":
                            emp.Priority = 0.676824540447828;
                            break;
                        case "emp_503408":
                            emp.Priority = 0.545830071459445;
                            break;
                        case "emp_506565":
                            emp.Priority = 1.13647167553961;
                            break;
                        case "emp_510525":
                            emp.Priority = 0.676528354257591;
                            break;
                        case "emp_510776":
                            emp.Priority = 1.1494568887397;
                            break;
                        case "emp_512293":
                            emp.Priority = 1.49174593109253;
                            break;
                        case "emp_512827":
                            emp.Priority = 0.76249976142426;
                            break;
                        case "emp_513531":
                            emp.Priority = 0.71346173212559;
                            break;
                        case "emp_515413":
                            emp.Priority = 0.610124206687382;
                            break;
                        case "emp_520094":
                            emp.Priority = 0.572759330306556;
                            break;
                        case "emp_527796":
                            emp.Priority = 0.540829012189446;
                            break;
                        case "emp_532257":
                            emp.Priority = 1.26470266271602;
                            break;
                        case "emp_533372":
                            emp.Priority = 0.627978597827246;
                            break;
                        case "emp_535196":
                            emp.Priority = 1.3331068404173;
                            break;
                        case "emp_655554":
                            emp.Priority = 1.20311403214145;
                            break;
                        case "emp_674832":
                            emp.Priority = 1.2882268167046;
                            break;
                        case "emp_674923":
                            emp.Priority = 0.786790336615774;
                            break;
                        case "emp_677882":
                            emp.Priority = 1.49238246725517;
                            break;
                        case "emp_682847":
                            emp.Priority = 1.27912729269784;
                            break;
                        case "emp_702502":
                            emp.Priority = 0.773871448949851;
                            break;
                        case "emp_714701":
                            emp.Priority = 0.934871625357713;
                            break;
                        case "emp_721224":
                            emp.Priority = 1.09379471121067;
                            break;
                        case "emp_731413":
                            emp.Priority = 1.40328656039354;
                            break;
                        case "emp_745529":
                            emp.Priority = 1.28441981123035;
                            break;
                        case "emp_746192":
                            emp.Priority = 1.01840614644736;
                            break;
                        case "emp_746345":
                            emp.Priority = 0.769063645167771;
                            break;
                        case "emp_764522":
                            emp.Priority = 1.05150651910878;
                            break;
                        case "emp_766203":
                            emp.Priority = 1.47693146531327;
                            break;
                        case "emp_785018":
                            emp.Priority = 0.820404010508398;
                            break;
                        case "emp_790844":
                            emp.Priority = 1.40884710378426;
                            break;
                        case "emp_806491":
                            emp.Priority = 0.771091450597668;
                            break;
                        case "emp_807526":
                            emp.Priority = 0.997490285196104;
                            break;
                        case "emp_808490":
                            emp.Priority = 1.04698422017832;
                            break;
                        case "emp_816604":
                            emp.Priority = 0.838352908072226;
                            break;
                        case "emp_821405":
                            emp.Priority = 0.638508428418314;
                            break;
                        case "emp_827006":
                            emp.Priority = 0.736645212972837;
                            break;
                        case "emp_830864":
                            emp.Priority = 1.47179321151776;
                            break;
                        case "emp_841943":
                            emp.Priority = 0.605282863651068;
                            break;
                        case "emp_843261":
                            emp.Priority = 1.45511699139844;
                            break;
                        case "emp_843913":
                            emp.Priority = 1.40282600321939;
                            break;
                        case "emp_846633":
                            emp.Priority = 0.754990206684447;
                            break;
                        case "emp_846901":
                            emp.Priority = 1.23627113212658;
                            break;
                        case "emp_847411":
                            emp.Priority = 0.968433688147196;
                            break;
                        case "emp_859259":
                            emp.Priority = 0.652508545272289;
                            break;
                        case "emp_861307":
                            emp.Priority = 0.591704425444689;
                            break;
                        case "emp_861514":
                            emp.Priority = 0.88343887188632;
                            break;
                        case "emp_862590":
                            emp.Priority = 0.598884089895936;
                            break;
                        case "emp_863918":
                            emp.Priority = 0.762005626811649;
                            break;
                        case "emp_869276":
                            emp.Priority = 1.4814634187992;
                            break;
                        case "emp_871619":
                            emp.Priority = 1.14947217453712;
                            break;
                        case "emp_871709":
                            emp.Priority = 0.992179302262226;
                            break;
                        case "emp_871737":
                            emp.Priority = 0.886293939494665;
                            break;
                        case "emp_873008":
                            emp.Priority = 0.716780547619229;
                            break;
                        case "emp_873639":
                            emp.Priority = 0.526741862309511;
                            break;
                        case "emp_876153":
                            emp.Priority = 0.924706677638323;
                            break;
                        case "emp_882005":
                            emp.Priority = 1.08302982923716;
                            break;
                        case "emp_882044":
                            emp.Priority = 0.642543511065908;
                            break;
                        case "emp_892056":
                            emp.Priority = 1.35205186430926;
                            break;
                        case "emp_895431":
                            emp.Priority = 1.15812220781023;
                            break;
                        case "emp_900347":
                            emp.Priority = 0.880393243571926;
                            break;
                        case "emp_900366":
                            emp.Priority = 0.94023941198375;
                            break;
                        case "emp_915775":
                            emp.Priority = 0.785568296110988;
                            break;
                        case "emp_919489":
                            emp.Priority = 1.39305772161719;
                            break;
                        case "emp_935500":
                            emp.Priority = 0.701277102903126;
                            break;
                        case "emp_941444":
                            emp.Priority = 1.30166787970889;
                            break;
                        case "emp_941718":
                            emp.Priority = 1.04333872699334;
                            break;
                        case "emp_158688":
                            emp.Priority = 0.717718442537691;
                            break;
                        case "emp_158695":
                            emp.Priority = 1.28962568975502;
                            break;
                        case "emp_162703":
                            emp.Priority = 1.19459841199899;
                            break;
                        case "emp_162706":
                            emp.Priority = 0.966468819168615;
                            break;
                        case "emp_162710":
                            emp.Priority = 1.31643360518684;
                            break;
                        case "emp_162726":
                            emp.Priority = 0.681507472964706;
                            break;
                        case "emp_162730":
                            emp.Priority = 0.53726547585673;
                            break;
                        case "emp_162732":
                            emp.Priority = 1.37630128947846;
                            break;
                        case "emp_175047":
                            emp.Priority = 0.518881242265404;
                            break;
                        case "emp_175082":
                            emp.Priority = 1.19860049323114;
                            break;
                        case "emp_175121":
                            emp.Priority = 0.625361023063474;
                            break;
                        case "emp_175127":
                            emp.Priority = 1.25077801512125;
                            break;
                        case "emp_175136":
                            emp.Priority = 1.19271538578566;
                            break;
                        case "emp_175139":
                            emp.Priority = 0.634967274561044;
                            break;
                        case "emp_175152":
                            emp.Priority = 0.670179555271836;
                            break;
                        case "emp_175155":
                            emp.Priority = 0.789500892297132;
                            break;
                        case "emp_175168":
                            emp.Priority = 1.49546804651407;
                            break;
                        case "emp_175763":
                            emp.Priority = 1.17093183597128;
                            break;
                        case "emp_183455":
                            emp.Priority = 0.91666780152203;
                            break;
                        case "emp_185835":
                            emp.Priority = 1.384797511103;
                            break;
                        case "emp_192261":
                            emp.Priority = 0.780709737576875;
                            break;
                        case "emp_193326":
                            emp.Priority = 1.02024235786881;
                            break;
                        case "emp_197213":
                            emp.Priority = 1.4136462304339;
                            break;
                        case "emp_202595":
                            emp.Priority = 1.05547859918116;
                            break;
                        case "emp_202603":
                            emp.Priority = 0.59410170190693;
                            break;
                        case "emp_204023":
                            emp.Priority = 0.619741347208499;
                            break;
                        case "emp_204183":
                            emp.Priority = 0.947160655840841;
                            break;
                        case "emp_206424":
                            emp.Priority = 1.07472374782652;
                            break;
                        case "emp_209403":
                            emp.Priority = 0.962586591235635;
                            break;
                        case "emp_223355":
                            emp.Priority = 1.46942191057346;
                            break;
                        case "emp_224020":
                            emp.Priority = 1.34321341050938;
                            break;
                        case "emp_226554":
                            emp.Priority = 0.767156585244069;
                            break;
                        case "emp_227535":
                            emp.Priority = 0.850840665563401;
                            break;
                        case "emp_233883":
                            emp.Priority = 1.04836569845135;
                            break;
                        case "emp_234326":
                            emp.Priority = 0.665720429348629;
                            break;
                        case "emp_237001":
                            emp.Priority = 0.809258400140916;
                            break;
                        case "emp_237314":
                            emp.Priority = 1.06740721481266;
                            break;
                        case "emp_240257":
                            emp.Priority = 1.01499459963059;
                            break;
                        case "emp_292355":
                            emp.Priority = 1.33303856935028;
                            break;
                        case "emp_295880":
                            emp.Priority = 0.81067182929752;
                            break;
                        case "emp_296416":
                            emp.Priority = 0.849028463637935;
                            break;
                        case "emp_298613":
                            emp.Priority = 0.840479258140772;
                            break;
                        case "emp_298614":
                            emp.Priority = 0.507860620044107;
                            break;
                        case "emp_302586":
                            emp.Priority = 1.22610618440719;
                            break;
                        case "emp_303752":
                            emp.Priority = 0.957668806173684;
                            break;
                        case "emp_304330":
                            emp.Priority = 0.891765495944659;
                            break;
                        case "emp_304922":
                            emp.Priority = 0.659336478523601;
                            break;
                        case "emp_308037":
                            emp.Priority = 1.02315493324918;
                            break;
                        case "emp_308730":
                            emp.Priority = 0.710213688300091;
                            break;
                        case "emp_321041":
                            emp.Priority = 0.650738519686618;
                            break;
                        case "emp_323456":
                            emp.Priority = 0.790100249596918;
                            break;
                        case "emp_333890":
                            emp.Priority = 0.722125885645918;
                            break;
                        case "emp_343400":
                            emp.Priority = 1.2846093013811;
                            break;
                        case "emp_370660":
                            emp.Priority = 1.41687036860588;
                            break;
                        case "emp_380750":
                            emp.Priority = 0.762628989416468;
                            break;
                        case "emp_380751":
                            emp.Priority = 1.19747608466888;
                            break;
                        case "emp_380753":
                            emp.Priority = 1.37597945932112;
                            break;
                        case "emp_380755":
                            emp.Priority = 0.639119812817834;
                            break;
                        case "emp_380756":
                            emp.Priority = 0.872367117261685;
                            break;
                        case "emp_380764":
                            emp.Priority = 1.19669225797834;
                            break;
                        case "emp_380770":
                            emp.Priority = 1.23434681712387;
                            break;
                        case "emp_380771":
                            emp.Priority = 0.962541728030211;
                            break;
                        case "emp_380773":
                            emp.Priority = 0.913714698242822;
                            break;
                        case "emp_383115":
                            emp.Priority = 0.549459331691945;
                            break;
                        case "emp_383731":
                            emp.Priority = 1.35538708272175;
                            break;
                        case "emp_401479":
                            emp.Priority = 1.3582044378194;
                            break;
                        case "emp_402167":
                            emp.Priority = 0.899937349557847;
                            break;
                        case "emp_406741":
                            emp.Priority = 0.644349687334313;
                            break;
                        case "emp_407236":
                            emp.Priority = 1.46924684521241;
                            break;
                        case "emp_428308":
                            emp.Priority = 1.36092115513092;
                            break;
                        case "emp_463640":
                            emp.Priority = 1.22209367748447;
                            break;
                        case "emp_481901":
                            emp.Priority = 0.980473446883482;
                            break;
                        case "emp_481912":
                            emp.Priority = 1.337893266621;
                            break;
                        case "emp_482267":
                            emp.Priority = 0.605995972224509;
                            break;
                        case "emp_482298":
                            emp.Priority = 1.03576904746507;
                            break;
                        case "emp_482306":
                            emp.Priority = 1.4402304794361;
                            break;
                        case "emp_482329":
                            emp.Priority = 1.01238173782471;
                            break;
                        case "emp_482330":
                            emp.Priority = 0.687540046026716;
                            break;
                        case "emp_482335":
                            emp.Priority = 0.597809793007472;
                            break;
                        case "emp_482656":
                            emp.Priority = 1.20233620596227;
                            break;
                        case "emp_482667":
                            emp.Priority = 1.4604048686849;
                            break;
                        case "emp_482715":
                            emp.Priority = 1.42400572259166;
                            break;
                        case "emp_482778":
                            emp.Priority = 0.864510059526428;
                            break;
                        case "emp_482800":
                            emp.Priority = 0.811848071549017;
                            break;
                        case "emp_482866":
                            emp.Priority = 1.17932166097654;
                            break;
                        case "emp_487312":
                            emp.Priority = 1.12108752486347;
                            break;
                        case "emp_487374":
                            emp.Priority = 0.982547283862972;
                            break;
                        case "emp_488620":
                            emp.Priority = 0.933970296957516;
                            break;
                        case "emp_491839":
                            emp.Priority = 0.785736709034879;
                            break;
                        case "emp_493069":
                            emp.Priority = 0.968244344679753;
                            break;
                        case "emp_495942":
                            emp.Priority = 0.933278940819799;
                            break;
                        case "emp_496900":
                            emp.Priority = 0.928287401994824;
                            break;
                        case "emp_497524":
                            emp.Priority = 0.642627482368903;
                            break;
                        case "emp_498417":
                            emp.Priority = 0.636346311371935;
                            break;
                        case "emp_499145":
                            emp.Priority = 1.07632501217365;
                            break;
                        case "emp_501464":
                            emp.Priority = 1.38648673560772;
                            break;
                        case "emp_503993":
                            emp.Priority = 1.42676455989795;
                            break;
                        case "emp_504043":
                            emp.Priority = 1.45840128835216;
                            break;
                        case "emp_505527":
                            emp.Priority = 1.37071910168543;
                            break;
                        case "emp_505584":
                            emp.Priority = 1.09946436835428;
                            break;
                        case "emp_506308":
                            emp.Priority = 1.49182814638681;
                            break;
                        case "emp_510074":
                            emp.Priority = 0.514986791189288;
                            break;
                        case "emp_510571":
                            emp.Priority = 1.05390808803677;
                            break;
                        case "emp_512296":
                            emp.Priority = 0.849292533169171;
                            break;
                        case "emp_512337":
                            emp.Priority = 0.928644842202144;
                            break;
                        case "emp_513125":
                            emp.Priority = 1.30962680271344;
                            break;
                        case "emp_514930":
                            emp.Priority = 0.884232619024921;
                            break;
                        case "emp_516300":
                            emp.Priority = 1.17861332915659;
                            break;
                        case "emp_517907":
                            emp.Priority = 0.881101321140817;
                            break;
                        case "emp_518004":
                            emp.Priority = 0.822398509980365;
                            break;
                        case "emp_519077":
                            emp.Priority = 0.685094346844169;
                            break;
                        case "emp_519229":
                            emp.Priority = 1.1884394132944;
                            break;
                        case "emp_519240":
                            emp.Priority = 0.541310019810363;
                            break;
                        case "emp_525461":
                            emp.Priority = 1.17003091129941;
                            break;
                        case "emp_526536":
                            emp.Priority = 1.23628738929345;
                            break;
                        case "emp_526792":
                            emp.Priority = 1.31034109453221;
                            break;
                        case "emp_527839":
                            emp.Priority = 0.598031668503784;
                            break;
                        case "emp_527850":
                            emp.Priority = 0.601866626693805;
                            break;
                        case "emp_528332":
                            emp.Priority = 0.870137670715404;
                            break;
                        case "emp_528704":
                            emp.Priority = 0.734299557858286;
                            break;
                        case "emp_528714":
                            emp.Priority = 0.875657153956432;
                            break;
                        case "emp_532208":
                            emp.Priority = 1.46596705260033;
                            break;
                        case "emp_536098":
                            emp.Priority = 1.35861297829943;
                            break;
                        case "emp_541519":
                            emp.Priority = 1.00100250053266;
                            break;
                        case "emp_546953":
                            emp.Priority = 0.92764221431112;
                            break;
                        case "emp_552831":
                            emp.Priority = 0.79380627548965;
                            break;
                        case "emp_552840":
                            emp.Priority = 0.837845964514579;
                            break;
                        case "emp_571382":
                            emp.Priority = 1.20154695524906;
                            break;
                        case "emp_598915":
                            emp.Priority = 1.02967096005085;
                            break;
                        case "emp_599598":
                            emp.Priority = 1.14928231185734;
                            break;
                        case "emp_602579":
                            emp.Priority = 0.513465919538152;
                            break;
                        case "emp_603548":
                            emp.Priority = 1.05398044947254;
                            break;
                        case "emp_605304":
                            emp.Priority = 0.816820944341282;
                            break;
                        case "emp_608849":
                            emp.Priority = 0.998345424653192;
                            break;
                        case "emp_613837":
                            emp.Priority = 1.21050805957546;
                            break;
                        case "emp_677774":
                            emp.Priority = 1.44541807749561;
                            break;
                        case "emp_698506":
                            emp.Priority = 0.870097634554886;
                            break;
                        case "emp_700065":
                            emp.Priority = 0.515217526357257;
                            break;
                        case "emp_703169":
                            emp.Priority = 1.38320322934687;
                            break;
                        case "emp_708738":
                            emp.Priority = 1.3696948582631;
                            break;
                        case "emp_719742":
                            emp.Priority = 0.736854905838545;
                            break;
                        case "emp_722484":
                            emp.Priority = 1.30393395470638;
                            break;
                        case "emp_725279":
                            emp.Priority = 0.552868975816699;
                            break;
                        case "emp_744740":
                            emp.Priority = 1.46333819905451;
                            break;
                        case "emp_747322":
                            emp.Priority = 0.783149997835583;
                            break;
                        case "emp_752035":
                            emp.Priority = 1.2448395275254;
                            break;
                        case "emp_753910":
                            emp.Priority = 0.886977382184461;
                            break;
                        case "emp_760182":
                            emp.Priority = 0.972596571069489;
                            break;
                        case "emp_760266":
                            emp.Priority = 0.900058922078488;
                            break;
                        case "emp_774378":
                            emp.Priority = 1.26598391764145;
                            break;
                        case "emp_809944":
                            emp.Priority = 1.28845506710394;
                            break;
                        case "emp_809965":
                            emp.Priority = 1.32489793320415;
                            break;
                        case "emp_829228":
                            emp.Priority = 1.08826361763676;
                            break;
                        case "emp_832426":
                            emp.Priority = 1.13641954382715;
                            break;
                        case "emp_832434":
                            emp.Priority = 0.723807214397847;
                            break;
                        case "emp_834775":
                            emp.Priority = 0.52586109378648;
                            break;
                        case "emp_838974":
                            emp.Priority = 0.656373812889854;
                            break;
                        case "emp_841205":
                            emp.Priority = 0.552905587504108;
                            break;
                        case "emp_844257":
                            emp.Priority = 1.42165031885805;
                            break;
                        case "emp_847468":
                            emp.Priority = 0.634838566712494;
                            break;
                        case "emp_854419":
                            emp.Priority = 0.971780838198858;
                            break;
                        case "emp_854884":
                            emp.Priority = 1.18268566377586;
                            break;
                        case "emp_858643":
                            emp.Priority = 0.648942369105733;
                            break;
                        case "emp_863963":
                            emp.Priority = 1.23181900928347;
                            break;
                        case "emp_870483":
                            emp.Priority = 0.808932590442213;
                            break;
                        case "emp_871668":
                            emp.Priority = 1.13111389737162;
                            break;
                        case "emp_873669":
                            emp.Priority = 0.871618468953119;
                            break;
                        case "emp_876142":
                            emp.Priority = 1.04296459515717;
                            break;
                        case "emp_879242":
                            emp.Priority = 1.45952285172396;
                            break;
                        case "emp_883434":
                            emp.Priority = 1.29086931179784;
                            break;
                        case "emp_886568":
                            emp.Priority = 0.940243459977323;
                            break;
                        case "emp_918433":
                            emp.Priority = 0.582814142146527;
                            break;
                        case "emp_935570":
                            emp.Priority = 0.718663397346932;
                            break;
                        case "emp_940832":
                            emp.Priority = 1.0004428124523;
                            break;
                        case "emp_943061":
                            emp.Priority = 1.49744465201974;
                            break;
                    }
                }

            }
            else if (portalName.Equals("koed"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    switch (emp.EmployeeId)
                    {
                        case "emp_154988":
                            emp.Priority = 0.684727016922425;
                            break;
                        case "emp_155000":
                            emp.Priority = 1.02106412757238;
                            break;
                        case "emp_155798":
                            emp.Priority = 1.43683866874168;
                            break;
                        case "emp_155803":
                            emp.Priority = 0.800209461385482;
                            break;
                        case "emp_155808":
                            emp.Priority = 0.886287139908544;
                            break;
                        case "emp_155820":
                            emp.Priority = 1.45252103682259;
                            break;
                        case "emp_156955":
                            emp.Priority = 1.33024121906154;
                            break;
                        case "emp_159120":
                            emp.Priority = 1.38346091047091;
                            break;
                        case "emp_159198":
                            emp.Priority = 1.30416456461147;
                            break;
                        case "emp_159668":
                            emp.Priority = 1.03451975413343;
                            break;
                        case "emp_160659":
                            emp.Priority = 1.31022492414817;
                            break;
                        case "emp_161992":
                            emp.Priority = 1.03700572789507;
                            break;
                        case "emp_163608":
                            emp.Priority = 0.836867029469864;
                            break;
                        case "emp_169130":
                            emp.Priority = 0.698367486800238;
                            break;
                        case "emp_215921":
                            emp.Priority = 0.641654023035268;
                            break;
                        case "emp_218318":
                            emp.Priority = 0.546710593647654;
                            break;
                        case "emp_221686":
                            emp.Priority = 1.04068652938152;
                            break;
                        case "emp_250881":
                            emp.Priority = 0.724522604245936;
                            break;
                        case "emp_296855":
                            emp.Priority = 1.48801226261445;
                            break;
                        case "emp_393278":
                            emp.Priority = 0.923445031244049;
                            break;
                        case "emp_400496":
                            emp.Priority = 0.91307096155969;
                            break;
                        case "emp_410922":
                            emp.Priority = 1.14869986690986;
                            break;
                        case "emp_410927":
                            emp.Priority = 0.804299575884035;
                            break;
                        case "emp_410930":
                            emp.Priority = 1.10380834136336;
                            break;
                        case "emp_433472":
                            emp.Priority = 0.950212115165876;
                            break;
                        case "emp_433958":
                            emp.Priority = 0.733945791718525;
                            break;
                        case "emp_471663":
                            emp.Priority = 1.42132302928778;
                            break;
                        case "emp_494926":
                            emp.Priority = 0.996680251553971;
                            break;
                        case "emp_507814":
                            emp.Priority = 0.879941499037641;
                            break;
                        case "emp_537429":
                            emp.Priority = 1.48256286745079;
                            break;
                        case "emp_543915":
                            emp.Priority = 0.786477915610409;
                            break;
                        case "emp_564985":
                            emp.Priority = 1.24491987179263;
                            break;
                        case "emp_569713":
                            emp.Priority = 0.605947834488911;
                            break;
                        case "emp_589144":
                            emp.Priority = 0.86147149296546;
                            break;
                        case "emp_590984":
                            emp.Priority = 0.67320302788783;
                            break;
                        case "emp_620796":
                            emp.Priority = 1.12527481262818;
                            break;
                        case "emp_645919":
                            emp.Priority = 1.47250716433511;
                            break;
                        case "emp_667161":
                            emp.Priority = 1.02897227300749;
                            break;
                        case "emp_667178":
                            emp.Priority = 0.923069128963663;
                            break;
                        case "emp_676674":
                            emp.Priority = 0.841736776913394;
                            break;
                        case "emp_679099":
                            emp.Priority = 0.546217471382682;
                            break;
                        case "emp_696318":
                            emp.Priority = 1.02299515787652;
                            break;
                        case "emp_702563":
                            emp.Priority = 1.17271079387176;
                            break;
                        case "emp_713067":
                            emp.Priority = 1.12126772227756;
                            break;
                        case "emp_723334":
                            emp.Priority = 0.638416158565514;
                            break;
                        case "emp_723514":
                            emp.Priority = 1.10585133806143;
                            break;
                        case "emp_724299":
                            emp.Priority = 0.614233297814724;
                            break;
                        case "emp_727524":
                            emp.Priority = 1.29405699567593;
                            break;
                        case "emp_728492":
                            emp.Priority = 0.822604563703111;
                            break;
                        case "emp_732343":
                            emp.Priority = 0.929804573967031;
                            break;
                        case "emp_747066":
                            emp.Priority = 1.20303571443215;
                            break;
                        case "emp_760314":
                            emp.Priority = 1.08710233754809;
                            break;
                        case "emp_764305":
                            emp.Priority = 1.15047847975533;
                            break;
                        case "emp_780718":
                            emp.Priority = 1.39427407732898;
                            break;
                        case "emp_783722":
                            emp.Priority = 1.01410885318839;
                            break;
                        case "emp_786813":
                            emp.Priority = 1.03602715001257;
                            break;
                        case "emp_847171":
                            emp.Priority = 0.716764551688342;
                            break;
                        case "emp_899630":
                            emp.Priority = 0.833030327378321;
                            break;
                        case "emp_910500":
                            emp.Priority = 1.34999734621961;
                            break;
                        case "emp_914478":
                            emp.Priority = 0.65234134819002;
                            break;
                        case "emp_917394":
                            emp.Priority = 0.531198007534816;
                            break;
                        case "emp_951982":
                            emp.Priority = 0.833560967507568;
                            break;
                        case "emp_155799":
                            emp.Priority = 1.00351941143326;
                            break;
                        case "emp_155801":
                            emp.Priority = 1.32160169716068;
                            break;
                        case "emp_155804":
                            emp.Priority = 0.74804183852302;
                            break;
                        case "emp_155805":
                            emp.Priority = 0.56530505235554;
                            break;
                        case "emp_155806":
                            emp.Priority = 0.931057893406161;
                            break;
                        case "emp_155809":
                            emp.Priority = 1.4753955365044;
                            break;
                        case "emp_155812":
                            emp.Priority = 0.525164458912408;
                            break;
                        case "emp_155813":
                            emp.Priority = 1.01637921040709;
                            break;
                        case "emp_155815":
                            emp.Priority = 0.57420342931254;
                            break;
                        case "emp_155816":
                            emp.Priority = 0.511714256374032;
                            break;
                        case "emp_155817":
                            emp.Priority = 1.30145347528227;
                            break;
                        case "emp_155818":
                            emp.Priority = 1.14627548570106;
                            break;
                        case "emp_155822":
                            emp.Priority = 0.877227559861367;
                            break;
                        case "emp_155823":
                            emp.Priority = 1.39007580368317;
                            break;
                        case "emp_156400":
                            emp.Priority = 1.4759890730381;
                            break;
                        case "emp_156401":
                            emp.Priority = 1.18303185360647;
                            break;
                        case "emp_156402":
                            emp.Priority = 0.965392182797842;
                            break;
                        case "emp_156404":
                            emp.Priority = 1.34436077710444;
                            break;
                        case "emp_158111":
                            emp.Priority = 0.619712493903801;
                            break;
                        case "emp_158541":
                            emp.Priority = 0.62726603361185;
                            break;
                        case "emp_158542":
                            emp.Priority = 0.674075687850861;
                            break;
                        case "emp_159202":
                            emp.Priority = 1.45013692507061;
                            break;
                        case "emp_159392":
                            emp.Priority = 0.779527153018642;
                            break;
                        case "emp_159641":
                            emp.Priority = 1.19937557806232;
                            break;
                        case "emp_160119":
                            emp.Priority = 0.594441392037292;
                            break;
                        case "emp_160122":
                            emp.Priority = 0.711673757159931;
                            break;
                        case "emp_160161":
                            emp.Priority = 1.34736263977707;
                            break;
                        case "emp_160952":
                            emp.Priority = 1.13717587787526;
                            break;
                        case "emp_160953":
                            emp.Priority = 0.908510260939836;
                            break;
                        case "emp_160993":
                            emp.Priority = 1.13947683695679;
                            break;
                        case "emp_161031":
                            emp.Priority = 1.17897492678788;
                            break;
                        case "emp_161039":
                            emp.Priority = 0.770727780773643;
                            break;
                        case "emp_162309":
                            emp.Priority = 0.810538769378578;
                            break;
                        case "emp_162942":
                            emp.Priority = 1.21265650387511;
                            break;
                        case "emp_163006":
                            emp.Priority = 0.519475746443251;
                            break;
                        case "emp_164651":
                            emp.Priority = 1.35110909671109;
                            break;
                        case "emp_165648":
                            emp.Priority = 0.873225883754541;
                            break;
                        case "emp_166885":
                            emp.Priority = 0.573111106209974;
                            break;
                        case "emp_167467":
                            emp.Priority = 0.674793444655274;
                            break;
                        case "emp_167471":
                            emp.Priority = 0.638837761310319;
                            break;
                        case "emp_168164":
                            emp.Priority = 1.26889253676352;
                            break;
                        case "emp_168418":
                            emp.Priority = 1.30622535329602;
                            break;
                        case "emp_170112":
                            emp.Priority = 0.855601144654491;
                            break;
                        case "emp_176349":
                            emp.Priority = 1.19132145805812;
                            break;
                        case "emp_176350":
                            emp.Priority = 1.28564886226582;
                            break;
                        case "emp_176805":
                            emp.Priority = 0.504202994054278;
                            break;
                        case "emp_182566":
                            emp.Priority = 1.01704651746761;
                            break;
                        case "emp_182985":
                            emp.Priority = 1.12403304950522;
                            break;
                        case "emp_187381":
                            emp.Priority = 1.06003807697447;
                            break;
                        case "emp_187868":
                            emp.Priority = 1.03373269808187;
                            break;
                        case "emp_188085":
                            emp.Priority = 1.36763814458048;
                            break;
                        case "emp_188088":
                            emp.Priority = 0.505636569115164;
                            break;
                        case "emp_188094":
                            emp.Priority = 0.532628854286218;
                            break;
                        case "emp_188099":
                            emp.Priority = 1.40393197392297;
                            break;
                        case "emp_188100":
                            emp.Priority = 0.659485279656707;
                            break;
                        case "emp_197167":
                            emp.Priority = 1.05338248636265;
                            break;
                        case "emp_221683":
                            emp.Priority = 1.04207454414203;
                            break;
                        case "emp_221684":
                            emp.Priority = 1.0486662604607;
                            break;
                        case "emp_229278":
                            emp.Priority = 1.47086366031825;
                            break;
                        case "emp_230497":
                            emp.Priority = 0.719384136246231;
                            break;
                        case "emp_232217":
                            emp.Priority = 0.628032896727339;
                            break;
                        case "emp_234709":
                            emp.Priority = 0.887988581037144;
                            break;
                        case "emp_243921":
                            emp.Priority = 0.607868949467255;
                            break;
                        case "emp_245743":
                            emp.Priority = 0.934726592355746;
                            break;
                        case "emp_246019":
                            emp.Priority = 0.83273932958615;
                            break;
                        case "emp_246128":
                            emp.Priority = 1.03072569450863;
                            break;
                        case "emp_250883":
                            emp.Priority = 0.835736716322478;
                            break;
                        case "emp_271665":
                            emp.Priority = 1.16457105598625;
                            break;
                        case "emp_273911":
                            emp.Priority = 1.37060005723992;
                            break;
                        case "emp_276999":
                            emp.Priority = 0.62487997632701;
                            break;
                        case "emp_278190":
                            emp.Priority = 0.809805969851932;
                            break;
                        case "emp_288146":
                            emp.Priority = 0.892281076587867;
                            break;
                        case "emp_288643":
                            emp.Priority = 1.16956733244917;
                            break;
                        case "emp_293900":
                            emp.Priority = 1.48087473259348;
                            break;
                        case "emp_300232":
                            emp.Priority = 0.858373496848333;
                            break;
                        case "emp_300581":
                            emp.Priority = 0.867850334554841;
                            break;
                        case "emp_306457":
                            emp.Priority = 1.09453578041612;
                            break;
                        case "emp_316947":
                            emp.Priority = 1.08820569496053;
                            break;
                        case "emp_322924":
                            emp.Priority = 1.4137267157965;
                            break;
                        case "emp_327578":
                            emp.Priority = 0.590238397983014;
                            break;
                        case "emp_327585":
                            emp.Priority = 1.19462723969232;
                            break;
                        case "emp_358352":
                            emp.Priority = 0.723329590271846;
                            break;
                        case "emp_358820":
                            emp.Priority = 0.577137800900795;
                            break;
                        case "emp_358822":
                            emp.Priority = 1.37477756285797;
                            break;
                        case "emp_364378":
                            emp.Priority = 1.27183869237631;
                            break;
                        case "emp_377464":
                            emp.Priority = 1.17333835767272;
                            break;
                        case "emp_384228":
                            emp.Priority = 0.738098926487425;
                            break;
                        case "emp_384969":
                            emp.Priority = 0.906606795455612;
                            break;
                        case "emp_385830":
                            emp.Priority = 1.05317122421841;
                            break;
                        case "emp_386319":
                            emp.Priority = 0.966093260080597;
                            break;
                        case "emp_407931":
                            emp.Priority = 0.809034552569052;
                            break;
                        case "emp_410923":
                            emp.Priority = 1.32455962329384;
                            break;
                        case "emp_410925":
                            emp.Priority = 0.602247445891727;
                            break;
                        case "emp_410926":
                            emp.Priority = 1.45540930840904;
                            break;
                        case "emp_410931":
                            emp.Priority = 0.51080486458298;
                            break;
                        case "emp_412767":
                            emp.Priority = 0.880903955726374;
                            break;
                        case "emp_426215":
                            emp.Priority = 1.19835640382876;
                            break;
                        case "emp_438433":
                            emp.Priority = 1.42087455229874;
                            break;
                        case "emp_445468":
                            emp.Priority = 0.858582128471966;
                            break;
                        case "emp_469137":
                            emp.Priority = 0.754923167757189;
                            break;
                        case "emp_487241":
                            emp.Priority = 1.1684662777318;
                            break;
                        case "emp_490718":
                            emp.Priority = 1.35247546148136;
                            break;
                        case "emp_493131":
                            emp.Priority = 1.2534329922653;
                            break;
                        case "emp_541663":
                            emp.Priority = 0.93515810064746;
                            break;
                        case "emp_546868":
                            emp.Priority = 0.723926728229936;
                            break;
                        case "emp_564958":
                            emp.Priority = 0.975357067992611;
                            break;
                        case "emp_617158":
                            emp.Priority = 0.836069236665996;
                            break;
                        case "emp_620787":
                            emp.Priority = 0.551754121692737;
                            break;
                        case "emp_667174":
                            emp.Priority = 1.04555847707463;
                            break;
                        case "emp_667180":
                            emp.Priority = 1.29163494510187;
                            break;
                        case "emp_667301":
                            emp.Priority = 1.45884670594653;
                            break;
                        case "emp_680491":
                            emp.Priority = 1.45386884918151;
                            break;
                        case "emp_685859":
                            emp.Priority = 1.1349395446642;
                            break;
                        case "emp_689012":
                            emp.Priority = 1.38062526233523;
                            break;
                        case "emp_697349":
                            emp.Priority = 1.02475689655391;
                            break;
                        case "emp_717546":
                            emp.Priority = 1.40470330645549;
                            break;
                        case "emp_730629":
                            emp.Priority = 0.810850780136348;
                            break;
                        case "emp_732349":
                            emp.Priority = 0.733091386609288;
                            break;
                        case "emp_737180":
                            emp.Priority = 1.16288789997943;
                            break;
                        case "emp_767307":
                            emp.Priority = 1.15940097191343;
                            break;
                        case "emp_803039":
                            emp.Priority = 0.792626768021205;
                            break;
                        case "emp_807655":
                            emp.Priority = 1.42912992086687;
                            break;
                        case "emp_825588":
                            emp.Priority = 0.611399831767846;
                            break;
                        case "emp_829171":
                            emp.Priority = 0.904506797159327;
                            break;
                        case "emp_838290":
                            emp.Priority = 1.31584542375796;
                            break;
                        case "emp_849933":
                            emp.Priority = 0.985246346558093;
                            break;
                        case "emp_865647":
                            emp.Priority = 0.790033630696141;
                            break;
                        case "emp_880053":
                            emp.Priority = 1.21415802404012;
                            break;
                        case "emp_899667":
                            emp.Priority = 1.4700698680105;
                            break;
                    }
                }
            }
            else if (portalName.Equals("marienlyst"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    switch (emp.EmployeeId)
                    {
                        case "emp_382081":
                            emp.Priority = 0.706909589565783;
                            break;
                        case "emp_382083":
                            emp.Priority = 1.1668031083731;
                            break;
                        case "emp_382085":
                            emp.Priority = 1.44450602398464;
                            break;
                        case "emp_382087":
                            emp.Priority = 0.576008672395725;
                            break;
                        case "emp_382092":
                            emp.Priority = 1.32359065386634;
                            break;
                        case "emp_382094":
                            emp.Priority = 1.43921320696325;
                            break;
                        case "emp_382096":
                            emp.Priority = 1.34813799515746;
                            break;
                        case "emp_382097":
                            emp.Priority = 1.32304791865081;
                            break;
                        case "emp_382104":
                            emp.Priority = 0.732239531461261;
                            break;
                        case "emp_382106":
                            emp.Priority = 1.33253378087307;
                            break;
                        case "emp_382107":
                            emp.Priority = 1.49566157674215;
                            break;
                        case "emp_382108":
                            emp.Priority = 0.529869652832798;
                            break;
                        case "emp_382109":
                            emp.Priority = 0.515958103358726;
                            break;
                        case "emp_382112":
                            emp.Priority = 0.55614493370808;
                            break;
                        case "emp_382116":
                            emp.Priority = 1.42345766300497;
                            break;
                        case "emp_382122":
                            emp.Priority = 1.23288569773216;
                            break;
                        case "emp_382124":
                            emp.Priority = 1.31753023379367;
                            break;
                        case "emp_382125":
                            emp.Priority = 0.66502824433382;
                            break;
                        case "emp_382127":
                            emp.Priority = 0.750559541979134;
                            break;
                        case "emp_382128":
                            emp.Priority = 1.47082404837516;
                            break;
                        case "emp_382130":
                            emp.Priority = 0.569559956467506;
                            break;
                        case "emp_382132":
                            emp.Priority = 0.951978288335716;
                            break;
                        case "emp_382134":
                            emp.Priority = 1.08975559127971;
                            break;
                        case "emp_382135":
                            emp.Priority = 1.45632950540461;
                            break;
                        case "emp_382136":
                            emp.Priority = 1.35735719551209;
                            break;
                        case "emp_382138":
                            emp.Priority = 1.13936750061781;
                            break;
                        case "emp_382140":
                            emp.Priority = 0.976182608155619;
                            break;
                        case "emp_382146":
                            emp.Priority = 0.779573300517897;
                            break;
                        case "emp_383817":
                            emp.Priority = 1.49140001460509;
                            break;
                        case "emp_412928":
                            emp.Priority = 1.34438383804838;
                            break;
                        case "emp_413653":
                            emp.Priority = 0.90573209543048;
                            break;
                        case "emp_414649":
                            emp.Priority = 0.619842711426244;
                            break;
                        case "emp_421551":
                            emp.Priority = 0.741336050090071;
                            break;
                        case "emp_421583":
                            emp.Priority = 0.586098659823695;
                            break;
                        case "emp_453006":
                            emp.Priority = 1.12836843292619;
                            break;
                        case "emp_453890":
                            emp.Priority = 0.998981923562932;
                            break;
                        case "emp_456323":
                            emp.Priority = 0.82083439003715;
                            break;
                        case "emp_488233":
                            emp.Priority = 0.620821645539637;
                            break;
                        case "emp_505157":
                            emp.Priority = 0.621576178875554;
                            break;
                        case "emp_516550":
                            emp.Priority = 0.812727892451327;
                            break;
                        case "emp_525394":
                            emp.Priority = 0.861608991102133;
                            break;
                        case "emp_525399":
                            emp.Priority = 0.659185873884329;
                            break;
                        case "emp_532244":
                            emp.Priority = 0.572147311210701;
                            break;
                        case "emp_534135":
                            emp.Priority = 1.23317212226483;
                            break;
                        case "emp_539375":
                            emp.Priority = 1.07939743696684;
                            break;
                        case "emp_548899":
                            emp.Priority = 0.654975419004902;
                            break;
                        case "emp_551379":
                            emp.Priority = 0.929693665089874;
                            break;
                        case "emp_552360":
                            emp.Priority = 0.895028689594487;
                            break;
                        case "emp_555033":
                            emp.Priority = 0.65503092769302;
                            break;
                        case "emp_569624":
                            emp.Priority = 0.68942892886206;
                            break;
                        case "emp_611178":
                            emp.Priority = 0.870452656117479;
                            break;
                        case "emp_648951":
                            emp.Priority = 1.01173951919737;
                            break;
                        case "emp_671709":
                            emp.Priority = 0.835534154128066;
                            break;
                        case "emp_699889":
                            emp.Priority = 0.664264778217424;
                            break;
                        case "emp_701754":
                            emp.Priority = 0.566993363232814;
                            break;
                        case "emp_705466":
                            emp.Priority = 1.25493130123007;
                            break;
                        case "emp_714674":
                            emp.Priority = 0.577047517093386;
                            break;
                        case "emp_717517":
                            emp.Priority = 1.48817651858003;
                            break;
                        case "emp_739301":
                            emp.Priority = 0.718651476883633;
                            break;
                        case "emp_769052":
                            emp.Priority = 0.684223153248533;
                            break;
                        case "emp_769074":
                            emp.Priority = 0.963030598807629;
                            break;
                        case "emp_769082":
                            emp.Priority = 1.06856469463956;
                            break;
                        case "emp_778687":
                            emp.Priority = 1.33164790404572;
                            break;
                        case "emp_781401":
                            emp.Priority = 0.887855693412877;
                            break;
                        case "emp_791948":
                            emp.Priority = 0.92680168544259;
                            break;
                        case "emp_803761":
                            emp.Priority = 1.3758188653159;
                            break;
                        case "emp_846384":
                            emp.Priority = 1.28853360274273;
                            break;
                        case "emp_846772":
                            emp.Priority = 1.42985944353503;
                            break;
                        case "emp_850836":
                            emp.Priority = 0.927776500781894;
                            break;
                        case "emp_851369":
                            emp.Priority = 0.924475739442034;
                            break;
                        case "emp_853614":
                            emp.Priority = 0.91205130769501;
                            break;
                        case "emp_854058":
                            emp.Priority = 1.19670858825404;
                            break;
                        case "emp_873344":
                            emp.Priority = 0.543452065458266;
                            break;
                        case "emp_873354":
                            emp.Priority = 1.43783164952781;
                            break;
                        case "emp_882463":
                            emp.Priority = 1.10921505727303;
                            break;
                        case "emp_882528":
                            emp.Priority = 1.41037408258318;
                            break;
                        case "emp_889526":
                            emp.Priority = 0.879830977125015;
                            break;
                        case "emp_898082":
                            emp.Priority = 1.35658346901489;
                            break;
                        case "emp_906695":
                            emp.Priority = 0.876932068437772;
                            break;
                        case "emp_911423":
                            emp.Priority = 1.20238177650719;
                            break;
                        case "emp_913882":
                            emp.Priority = 0.709673835527931;
                            break;
                        case "emp_916592":
                            emp.Priority = 0.581153918561132;
                            break;
                        case "emp_929613":
                            emp.Priority = 0.624542372824877;
                            break;
                        case "emp_930344":
                            emp.Priority = 1.30197108574303;
                            break;
                        case "emp_931704":
                            emp.Priority = 0.973931181930905;
                            break;
                        case "emp_934412":
                            emp.Priority = 1.39399257623311;
                            break;
                        case "emp_936530":
                            emp.Priority = 1.28430855729818;
                            break;
                        case "emp_940697":
                            emp.Priority = 0.577071271872647;
                            break;
                        case "emp_943464":
                            emp.Priority = 0.51910529659088;
                            break;
                        case "emp_944066":
                            emp.Priority = 1.37343713169612;
                            break;
                        case "emp_945504":
                            emp.Priority = 0.921934406469545;
                            break;
                        case "emp_951476":
                            emp.Priority = 0.832657871457123;
                            break;
                        case "emp_952168":
                            emp.Priority = 1.402170168656;
                            break;
                        case "emp_952429":
                            emp.Priority = 1.43735302562702;
                            break;
                        case "emp_953492":
                            emp.Priority = 1.3496972936437;
                            break;
                        case "emp_953500":
                            emp.Priority = 1.29304429646258;
                            break;
                        case "emp_367553":
                            emp.Priority = 0.827537969838613;
                            break;
                        case "emp_382079":
                            emp.Priority = 1.18429161779782;
                            break;
                        case "emp_382080":
                            emp.Priority = 0.806370436822237;
                            break;
                        case "emp_382082":
                            emp.Priority = 1.20357857165093;
                            break;
                        case "emp_382084":
                            emp.Priority = 0.866441816262175;
                            break;
                        case "emp_382086":
                            emp.Priority = 0.999834221554843;
                            break;
                        case "emp_382088":
                            emp.Priority = 1.46725218881259;
                            break;
                        case "emp_382089":
                            emp.Priority = 1.23055518825099;
                            break;
                        case "emp_382090":
                            emp.Priority = 1.27737762116705;
                            break;
                        case "emp_382095":
                            emp.Priority = 1.17374406786344;
                            break;
                        case "emp_382098":
                            emp.Priority = 0.968287453739106;
                            break;
                        case "emp_382100":
                            emp.Priority = 0.89770250460026;
                            break;
                        case "emp_382105":
                            emp.Priority = 1.05504972094439;
                            break;
                        case "emp_382110":
                            emp.Priority = 0.656619280649637;
                            break;
                        case "emp_382113":
                            emp.Priority = 0.875100324105052;
                            break;
                        case "emp_382114":
                            emp.Priority = 0.7204640480785;
                            break;
                        case "emp_382115":
                            emp.Priority = 1.11124445014226;
                            break;
                        case "emp_382117":
                            emp.Priority = 1.01626970037644;
                            break;
                        case "emp_382118":
                            emp.Priority = 1.4745493177206;
                            break;
                        case "emp_382119":
                            emp.Priority = 0.881876680246497;
                            break;
                        case "emp_382123":
                            emp.Priority = 0.944022321814681;
                            break;
                        case "emp_382126":
                            emp.Priority = 0.529676818302682;
                            break;
                        case "emp_382129":
                            emp.Priority = 1.41392451148197;
                            break;
                        case "emp_382137":
                            emp.Priority = 1.03280910920948;
                            break;
                        case "emp_382142":
                            emp.Priority = 0.591510308017726;
                            break;
                        case "emp_382143":
                            emp.Priority = 1.21146233087008;
                            break;
                        case "emp_382144":
                            emp.Priority = 1.41075414694415;
                            break;
                        case "emp_382145":
                            emp.Priority = 1.05433936908578;
                            break;
                        case "emp_382179":
                            emp.Priority = 0.502541332972488;
                            break;
                        case "emp_382182":
                            emp.Priority = 0.579393436237887;
                            break;
                        case "emp_382183":
                            emp.Priority = 1.29453841959803;
                            break;
                        case "emp_382184":
                            emp.Priority = 0.606099039831245;
                            break;
                        case "emp_382185":
                            emp.Priority = 0.588134355884108;
                            break;
                        case "emp_382186":
                            emp.Priority = 1.31617076081045;
                            break;
                        case "emp_389218":
                            emp.Priority = 1.08283611274456;
                            break;
                        case "emp_389995":
                            emp.Priority = 1.19553935932719;
                            break;
                        case "emp_392881":
                            emp.Priority = 1.05021303219265;
                            break;
                        case "emp_396944":
                            emp.Priority = 1.17335349678684;
                            break;
                        case "emp_396951":
                            emp.Priority = 0.835939960245015;
                            break;
                        case "emp_396953":
                            emp.Priority = 1.20983961397309;
                            break;
                        case "emp_396955":
                            emp.Priority = 0.613901729748539;
                            break;
                        case "emp_396962":
                            emp.Priority = 0.89398718457389;
                            break;
                        case "emp_396985":
                            emp.Priority = 0.524593464575984;
                            break;
                        case "emp_399526":
                            emp.Priority = 1.30018711406746;
                            break;
                        case "emp_399847":
                            emp.Priority = 0.925705122494001;
                            break;
                        case "emp_403769":
                            emp.Priority = 0.886606052697918;
                            break;
                        case "emp_403900":
                            emp.Priority = 1.02202155092825;
                            break;
                        case "emp_403920":
                            emp.Priority = 1.36248601594124;
                            break;
                        case "emp_405012":
                            emp.Priority = 0.998336807591066;
                            break;
                        case "emp_407035":
                            emp.Priority = 0.701470358391046;
                            break;
                        case "emp_408298":
                            emp.Priority = 1.22141342131487;
                            break;
                        case "emp_410340":
                            emp.Priority = 0.885900468279561;
                            break;
                        case "emp_412921":
                            emp.Priority = 1.46280370790642;
                            break;
                        case "emp_413755":
                            emp.Priority = 0.967820613397202;
                            break;
                        case "emp_414282":
                            emp.Priority = 0.849021974647894;
                            break;
                        case "emp_417992":
                            emp.Priority = 0.797861151535931;
                            break;
                        case "emp_419246":
                            emp.Priority = 1.27036710631585;
                            break;
                        case "emp_420123":
                            emp.Priority = 1.27356132761275;
                            break;
                        case "emp_424418":
                            emp.Priority = 1.11206826363321;
                            break;
                        case "emp_429165":
                            emp.Priority = 1.1549794853921;
                            break;
                        case "emp_431639":
                            emp.Priority = 1.08908007461069;
                            break;
                        case "emp_435221":
                            emp.Priority = 0.912912819726818;
                            break;
                        case "emp_439513":
                            emp.Priority = 1.2280138552785;
                            break;
                        case "emp_439514":
                            emp.Priority = 1.19798418492916;
                            break;
                        case "emp_442101":
                            emp.Priority = 1.37920564826541;
                            break;
                        case "emp_446049":
                            emp.Priority = 0.862188413907861;
                            break;
                        case "emp_446221":
                            emp.Priority = 0.809568148716152;
                            break;
                        case "emp_510496":
                            emp.Priority = 1.23887896013394;
                            break;
                        case "emp_511984":
                            emp.Priority = 1.07378316790507;
                            break;
                        case "emp_527751":
                            emp.Priority = 1.17956096477786;
                            break;
                        case "emp_531630":
                            emp.Priority = 1.17025101588585;
                            break;
                        case "emp_531738":
                            emp.Priority = 1.43789095335542;
                            break;
                        case "emp_531739":
                            emp.Priority = 0.680329740131427;
                            break;
                        case "emp_532235":
                            emp.Priority = 0.764709703747514;
                            break;
                        case "emp_537280":
                            emp.Priority = 0.767974950497958;
                            break;
                        case "emp_539377":
                            emp.Priority = 0.550035137240791;
                            break;
                        case "emp_539379":
                            emp.Priority = 0.505083353726698;
                            break;
                        case "emp_539823":
                            emp.Priority = 0.61373739741451;
                            break;
                        case "emp_551343":
                            emp.Priority = 0.607103986715481;
                            break;
                        case "emp_551345":
                            emp.Priority = 1.20490425531981;
                            break;
                        case "emp_569658":
                            emp.Priority = 0.689440779941827;
                            break;
                        case "emp_602498":
                            emp.Priority = 0.548268131002909;
                            break;
                        case "emp_639007":
                            emp.Priority = 0.556002561494709;
                            break;
                        case "emp_646245":
                            emp.Priority = 1.30107097458144;
                            break;
                        case "emp_649071":
                            emp.Priority = 0.857980014923019;
                            break;
                        case "emp_650090":
                            emp.Priority = 0.908637951318472;
                            break;
                        case "emp_653363":
                            emp.Priority = 0.643295331924826;
                            break;
                        case "emp_685070":
                            emp.Priority = 1.12031374248691;
                            break;
                        case "emp_685806":
                            emp.Priority = 0.967148786162561;
                            break;
                        case "emp_703372":
                            emp.Priority = 0.784974961208634;
                            break;
                        case "emp_704700":
                            emp.Priority = 1.42517225301134;
                            break;
                        case "emp_705714":
                            emp.Priority = 1.2766517045799;
                            break;
                        case "emp_705719":
                            emp.Priority = 0.561285233153629;
                            break;
                        case "emp_705734":
                            emp.Priority = 1.18096047485292;
                            break;
                        case "emp_714624":
                            emp.Priority = 0.620759539362397;
                            break;
                        case "emp_719571":
                            emp.Priority = 1.20098891002172;
                            break;
                        case "emp_719756":
                            emp.Priority = 1.16597332929539;
                            break;
                        case "emp_722254":
                            emp.Priority = 0.826609279646822;
                            break;
                        case "emp_769057":
                            emp.Priority = 1.42098146580205;
                            break;
                        case "emp_769064":
                            emp.Priority = 0.56351670858614;
                            break;
                        case "emp_781403":
                            emp.Priority = 0.577037903981767;
                            break;
                        case "emp_792076":
                            emp.Priority = 1.28314259079431;
                            break;
                        case "emp_792091":
                            emp.Priority = 0.78870284803617;
                            break;
                        case "emp_796517":
                            emp.Priority = 1.31877584281321;
                            break;
                        case "emp_796546":
                            emp.Priority = 1.0312193425052;
                            break;
                        case "emp_849335":
                            emp.Priority = 1.28352246795945;
                            break;
                        case "emp_873351":
                            emp.Priority = 0.705570728148134;
                            break;
                        case "emp_877055":
                            emp.Priority = 1.19809400415891;
                            break;
                        case "emp_889510":
                            emp.Priority = 0.699845662899244;
                            break;
                    }
                }
            }
            else if (portalName.Equals("falck-globalassistance"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    switch (emp.EmployeeId)
                    {
                        case "emp_160516":
                            emp.Priority = 1.05671706821617;
                            break;
                        case "emp_177949":
                            emp.Priority = 0.800534283882256;
                            break;
                        case "emp_195193":
                            emp.Priority = 1.37472614779823;
                            break;
                        case "emp_196898":
                            emp.Priority = 1.35637575520965;
                            break;
                        case "emp_199369":
                            emp.Priority = 1.28393152578917;
                            break;
                        case "emp_200090":
                            emp.Priority = 0.560587184066226;
                            break;
                        case "emp_200093":
                            emp.Priority = 0.810041468269211;
                            break;
                        case "emp_205434":
                            emp.Priority = 0.851000555954408;
                            break;
                        case "emp_262685":
                            emp.Priority = 0.628037693504262;
                            break;
                        case "emp_262686":
                            emp.Priority = 1.48062779893197;
                            break;
                        case "emp_262687":
                            emp.Priority = 0.693115010947508;
                            break;
                        case "emp_262693":
                            emp.Priority = 0.665046294296648;
                            break;
                        case "emp_289527":
                            emp.Priority = 0.962485817010741;
                            break;
                        case "emp_290644":
                            emp.Priority = 1.44886170092451;
                            break;
                        case "emp_315443":
                            emp.Priority = 1.20486986623372;
                            break;
                        case "emp_315445":
                            emp.Priority = 0.802593058116079;
                            break;
                        case "emp_315446":
                            emp.Priority = 0.904025811424491;
                            break;
                        case "emp_315451":
                            emp.Priority = 0.789130373526891;
                            break;
                        case "emp_322875":
                            emp.Priority = 0.949913215567317;
                            break;
                        case "emp_339909":
                            emp.Priority = 1.30640694629699;
                            break;
                        case "emp_339912":
                            emp.Priority = 0.721890513422848;
                            break;
                        case "emp_339916":
                            emp.Priority = 1.24100363754714;
                            break;
                        case "emp_339919":
                            emp.Priority = 1.30178117835977;
                            break;
                        case "emp_339923":
                            emp.Priority = 1.32542180867187;
                            break;
                        case "emp_339928":
                            emp.Priority = 1.25645573286175;
                            break;
                        case "emp_372990":
                            emp.Priority = 1.20124220321944;
                            break;
                        case "emp_372998":
                            emp.Priority = 0.671869118312313;
                            break;
                        case "emp_372999":
                            emp.Priority = 1.47125711104425;
                            break;
                        case "emp_373000":
                            emp.Priority = 1.20116133927422;
                            break;
                        case "emp_373001":
                            emp.Priority = 1.3365567959084;
                            break;
                        case "emp_377360":
                            emp.Priority = 0.832978382396036;
                            break;
                        case "emp_377365":
                            emp.Priority = 0.725673581578617;
                            break;
                        case "emp_147041":
                            emp.Priority = 0.565459138744259;
                            break;
                        case "emp_147040":
                            emp.Priority = 1.13913538848941;
                            break;
                        case "emp_141683":
                            emp.Priority = 0.51861346979561;
                            break;
                        case "emp_100151":
                            emp.Priority = 1.42304100185774;
                            break;
                        case "emp_100138":
                            emp.Priority = 1.04364649511112;
                            break;
                        case "emp_90443":
                            emp.Priority = 0.572137870859419;
                            break;
                        case "emp_72149":
                            emp.Priority = 1.24234864476246;
                            break;
                        case "emp_49927":
                            emp.Priority = 0.94679549729768;
                            break;
                        case "emp_49921":
                            emp.Priority = 1.00061044027126;
                            break;
                        case "emp_39749":
                            emp.Priority = 0.52277656878474;
                            break;
                        case "emp_39743":
                            emp.Priority = 0.589673888911341;
                            break;
                        case "emp_39737":
                            emp.Priority = 0.923651935264306;
                            break;
                        case "emp_39735":
                            emp.Priority = 0.657125800455513;
                            break;
                        case "emp_426450":
                            emp.Priority = 1.08521838979107;
                            break;
                        case "emp_427988":
                            emp.Priority = 1.24761877057497;
                            break;
                        case "emp_428064":
                            emp.Priority = 1.42023167895164;
                            break;
                        case "emp_428419":
                            emp.Priority = 0.736661960481043;
                            break;
                        case "emp_450445":
                            emp.Priority = 0.594849512956501;
                            break;
                        case "emp_484865":
                            emp.Priority = 1.10549674583855;
                            break;
                        case "emp_509737":
                            emp.Priority = 0.558806553044732;
                            break;
                        case "emp_509816":
                            emp.Priority = 0.662613724899764;
                            break;
                        case "emp_509819":
                            emp.Priority = 1.17021463796041;
                            break;
                        case "emp_509824":
                            emp.Priority = 1.15332252655799;
                            break;
                        case "emp_509827":
                            emp.Priority = 1.31571343066903;
                            break;
                        case "emp_516651":
                            emp.Priority = 0.998753105522484;
                            break;
                        case "emp_517372":
                            emp.Priority = 0.54930433912636;
                            break;
                        case "emp_517453":
                            emp.Priority = 0.599920022347905;
                            break;
                        case "emp_517480":
                            emp.Priority = 0.582689322569728;
                            break;
                        case "emp_517525":
                            emp.Priority = 1.38871806575391;
                            break;
                        case "emp_517534":
                            emp.Priority = 0.838784357224956;
                            break;
                        case "emp_518051":
                            emp.Priority = 1.14983921668019;
                            break;
                        case "emp_518070":
                            emp.Priority = 0.791480897595864;
                            break;
                        case "emp_518085":
                            emp.Priority = 1.14764941653593;
                            break;
                        case "emp_523779":
                            emp.Priority = 1.46744142936889;
                            break;
                        case "emp_529583":
                            emp.Priority = 0.599587155552389;
                            break;
                        case "emp_535085":
                            emp.Priority = 1.32335042852133;
                            break;
                        case "emp_541324":
                            emp.Priority = 1.43024823112891;
                            break;
                        case "emp_541453":
                            emp.Priority = 1.28182886437598;
                            break;
                        case "emp_541481":
                            emp.Priority = 1.25894656300496;
                            break;
                        case "emp_735176":
                            emp.Priority = 0.831887940565072;
                            break;
                        case "emp_736245":
                            emp.Priority = 1.04678172876443;
                            break;
                        case "emp_736265":
                            emp.Priority = 0.503117718269638;
                            break;
                        case "emp_736269":
                            emp.Priority = 0.805796506025734;
                            break;
                        case "emp_736272":
                            emp.Priority = 0.699113944638108;
                            break;
                        case "emp_802270":
                            emp.Priority = 1.1513297486358;
                            break;
                        case "emp_805377":
                            emp.Priority = 0.878129243095466;
                            break;
                        case "emp_810864":
                            emp.Priority = 1.16829600821635;
                            break;
                        case "emp_874528":
                            emp.Priority = 0.671237343070674;
                            break;
                        case "emp_874547":
                            emp.Priority = 1.45362343264447;
                            break;
                        case "emp_874555":
                            emp.Priority = 0.751637439360673;
                            break;
                        case "emp_874566":
                            emp.Priority = 1.23459515056321;
                            break;
                        case "emp_874571":
                            emp.Priority = 1.10631182631772;
                            break;
                        case "emp_874579":
                            emp.Priority = 0.731060050069848;
                            break;
                        case "emp_874587":
                            emp.Priority = 0.774171829351304;
                            break;
                        case "emp_874601":
                            emp.Priority = 0.563059856678853;
                            break;
                        case "emp_874675":
                            emp.Priority = 0.895244500783852;
                            break;
                        case "emp_874683":
                            emp.Priority = 1.48581286193142;
                            break;
                        case "emp_906169":
                            emp.Priority = 0.702900039126584;
                            break;
                        case "emp_906171":
                            emp.Priority = 0.924287896335259;
                            break;
                        case "emp_906172":
                            emp.Priority = 0.994342155984762;
                            break;
                        case "emp_906178":
                            emp.Priority = 1.47221784851151;
                            break;
                        case "emp_906180":
                            emp.Priority = 1.15965932219273;
                            break;
                        case "emp_906183":
                            emp.Priority = 1.05807743154377;
                            break;
                        case "emp_906190":
                            emp.Priority = 0.661826083046303;
                            break;
                        case "emp_906196":
                            emp.Priority = 0.872937352104549;
                            break;
                        case "emp_906425":
                            emp.Priority = 1.29819299131548;
                            break;
                        case "emp_906427":
                            emp.Priority = 1.27600251872838;
                            break;
                        case "emp_907534":
                            emp.Priority = 0.689684371086622;
                            break;
                        case "emp_907537":
                            emp.Priority = 0.985631234238684;
                            break;
                        case "emp_39080":
                            emp.Priority = 1.42426834205364;
                            break;
                        case "emp_39102":
                            emp.Priority = 1.48998344782273;
                            break;
                        case "emp_39731":
                            emp.Priority = 0.954833096105062;
                            break;
                        case "emp_39732":
                            emp.Priority = 0.835902949951544;
                            break;
                        case "emp_39734":
                            emp.Priority = 0.773608805273477;
                            break;
                        case "emp_39736":
                            emp.Priority = 1.0120248242803;
                            break;
                        case "emp_39738":
                            emp.Priority = 0.659496006630126;
                            break;
                        case "emp_39740":
                            emp.Priority = 0.864418131934674;
                            break;
                        case "emp_39741":
                            emp.Priority = 0.954208581919879;
                            break;
                        case "emp_39742":
                            emp.Priority = 0.664383682033226;
                            break;
                        case "emp_39744":
                            emp.Priority = 0.620623862427019;
                            break;
                        case "emp_39746":
                            emp.Priority = 0.881008330910005;
                            break;
                        case "emp_39748":
                            emp.Priority = 1.42868267927723;
                            break;
                        case "emp_39753":
                            emp.Priority = 0.629065889925261;
                            break;
                        case "emp_39754":
                            emp.Priority = 1.13708062639324;
                            break;
                        case "emp_39755":
                            emp.Priority = 1.10418920666174;
                            break;
                        case "emp_39756":
                            emp.Priority = 0.543527390362475;
                            break;
                        case "emp_39757":
                            emp.Priority = 0.560420847526016;
                            break;
                        case "emp_39758":
                            emp.Priority = 0.873477587184625;
                            break;
                        case "emp_39759":
                            emp.Priority = 1.40438157269004;
                            break;
                        case "emp_39760":
                            emp.Priority = 1.20434265476854;
                            break;
                        case "emp_39761":
                            emp.Priority = 1.33753756658991;
                            break;
                        case "emp_39763":
                            emp.Priority = 1.22734819200232;
                            break;
                        case "emp_39764":
                            emp.Priority = 0.857540968040722;
                            break;
                        case "emp_40769":
                            emp.Priority = 0.764604407020195;
                            break;
                        case "emp_42104":
                            emp.Priority = 0.859670092053558;
                            break;
                        case "emp_42149":
                            emp.Priority = 1.38712240657169;
                            break;
                        case "emp_42484":
                            emp.Priority = 0.945040286725871;
                            break;
                        case "emp_42702":
                            emp.Priority = 0.643970422979431;
                            break;
                        case "emp_42720":
                            emp.Priority = 1.32617659253356;
                            break;
                        case "emp_43831":
                            emp.Priority = 1.35313675732032;
                            break;
                        case "emp_44289":
                            emp.Priority = 1.10212672436709;
                            break;
                        case "emp_48927":
                            emp.Priority = 0.978611637129733;
                            break;
                        case "emp_48928":
                            emp.Priority = 1.18560610883199;
                            break;
                        case "emp_48929":
                            emp.Priority = 0.529355090590825;
                            break;
                        case "emp_48931":
                            emp.Priority = 0.761653991537939;
                            break;
                        case "emp_49922":
                            emp.Priority = 0.77976205445815;
                            break;
                        case "emp_49923":
                            emp.Priority = 0.770408876366172;
                            break;
                        case "emp_49924":
                            emp.Priority = 1.45745124479637;
                            break;
                        case "emp_49925":
                            emp.Priority = 1.262147005071;
                            break;
                        case "emp_49926":
                            emp.Priority = 1.40356385004873;
                            break;
                        case "emp_52017":
                            emp.Priority = 0.530826368849178;
                            break;
                        case "emp_55538":
                            emp.Priority = 1.03160428001154;
                            break;
                        case "emp_63301":
                            emp.Priority = 0.538516357093359;
                            break;
                        case "emp_67480":
                            emp.Priority = 0.803664033908241;
                            break;
                        case "emp_69869":
                            emp.Priority = 0.613333825074757;
                            break;
                        case "emp_71861":
                            emp.Priority = 0.543535169234283;
                            break;
                        case "emp_71862":
                            emp.Priority = 1.03059343226747;
                            break;
                        case "emp_71865":
                            emp.Priority = 1.42099680515053;
                            break;
                        case "emp_71866":
                            emp.Priority = 1.05763687638456;
                            break;
                        case "emp_72140":
                            emp.Priority = 0.829409961742074;
                            break;
                        case "emp_72147":
                            emp.Priority = 1.23777214378946;
                            break;
                        case "emp_72152":
                            emp.Priority = 0.902524931543751;
                            break;
                        case "emp_87275":
                            emp.Priority = 0.785302798396583;
                            break;
                        case "emp_87276":
                            emp.Priority = 1.28128857947015;
                            break;
                        case "emp_87277":
                            emp.Priority = 0.586730775463735;
                            break;
                        case "emp_88733":
                            emp.Priority = 0.762635255820414;
                            break;
                        case "emp_90562":
                            emp.Priority = 0.59729212806434;
                            break;
                        case "emp_91219":
                            emp.Priority = 0.571298542931349;
                            break;
                        case "emp_92388":
                            emp.Priority = 1.41393871321992;
                            break;
                        case "emp_92453":
                            emp.Priority = 1.12490241770861;
                            break;
                        case "emp_97847":
                            emp.Priority = 1.21445571990425;
                            break;
                        case "emp_100114":
                            emp.Priority = 0.720447708955243;
                            break;
                        case "emp_100116":
                            emp.Priority = 1.12803198938632;
                            break;
                        case "emp_109252":
                            emp.Priority = 0.811246924712903;
                            break;
                        case "emp_120836":
                            emp.Priority = 1.01849713805993;
                            break;
                        case "emp_141635":
                            emp.Priority = 1.40239669378027;
                            break;
                        case "emp_147034":
                            emp.Priority = 0.743076570445242;
                            break;
                        case "emp_147039":
                            emp.Priority = 0.599710799334436;
                            break;
                        case "emp_147045":
                            emp.Priority = 0.875426634855301;
                            break;
                        case "emp_148231":
                            emp.Priority = 0.824427152203595;
                            break;
                        case "emp_160402":
                            emp.Priority = 1.2731185139963;
                            break;
                        case "emp_160576":
                            emp.Priority = 0.602969602729645;
                            break;
                        case "emp_160578":
                            emp.Priority = 1.11133058211362;
                            break;
                        case "emp_160579":
                            emp.Priority = 0.500817722641312;
                            break;
                        case "emp_160584":
                            emp.Priority = 1.17351628591936;
                            break;
                        case "emp_160591":
                            emp.Priority = 0.805933286578363;
                            break;
                        case "emp_160595":
                            emp.Priority = 1.18883183490896;
                            break;
                        case "emp_160677":
                            emp.Priority = 0.553876934132481;
                            break;
                        case "emp_160678":
                            emp.Priority = 0.651270581945437;
                            break;
                        case "emp_177448":
                            emp.Priority = 0.816134922819275;
                            break;
                        case "emp_178160":
                            emp.Priority = 0.856528974304222;
                            break;
                        case "emp_182573":
                            emp.Priority = 1.02404348157535;
                            break;
                        case "emp_183286":
                            emp.Priority = 1.08633354659487;
                            break;
                        case "emp_183287":
                            emp.Priority = 0.996766630791485;
                            break;
                        case "emp_183288":
                            emp.Priority = 0.615364613530861;
                            break;
                        case "emp_183485":
                            emp.Priority = 0.699601792823338;
                            break;
                        case "emp_183926":
                            emp.Priority = 0.69330883873315;
                            break;
                        case "emp_184813":
                            emp.Priority = 1.40431752936184;
                            break;
                        case "emp_192223":
                            emp.Priority = 1.44262431512709;
                            break;
                        case "emp_195452":
                            emp.Priority = 1.49901873571753;
                            break;
                        case "emp_196899":
                            emp.Priority = 0.68246992639381;
                            break;
                        case "emp_199368":
                            emp.Priority = 0.699110333434823;
                            break;
                        case "emp_199934":
                            emp.Priority = 0.543512531576451;
                            break;
                        case "emp_199946":
                            emp.Priority = 0.637244587362392;
                            break;
                        case "emp_200084":
                            emp.Priority = 0.689108130144471;
                            break;
                        case "emp_200085":
                            emp.Priority = 1.31037865989393;
                            break;
                        case "emp_200089":
                            emp.Priority = 1.40357229062522;
                            break;
                        case "emp_200092":
                            emp.Priority = 1.22726943238046;
                            break;
                        case "emp_200094":
                            emp.Priority = 1.28516689584831;
                            break;
                        case "emp_200095":
                            emp.Priority = 0.710937131294486;
                            break;
                        case "emp_200096":
                            emp.Priority = 1.30045859878904;
                            break;
                        case "emp_200321":
                            emp.Priority = 0.930882632933037;
                            break;
                        case "emp_200868":
                            emp.Priority = 1.04557017029522;
                            break;
                        case "emp_205432":
                            emp.Priority = 0.733209724180964;
                            break;
                        case "emp_205433":
                            emp.Priority = 1.05629144774577;
                            break;
                        case "emp_211557":
                            emp.Priority = 1.13480254105982;
                            break;
                        case "emp_217899":
                            emp.Priority = 1.29119434943013;
                            break;
                        case "emp_217900":
                            emp.Priority = 0.784485075755271;
                            break;
                        case "emp_217901":
                            emp.Priority = 0.607772293550788;
                            break;
                        case "emp_218075":
                            emp.Priority = 1.28079748888537;
                            break;
                        case "emp_218076":
                            emp.Priority = 1.07380342091145;
                            break;
                        case "emp_218077":
                            emp.Priority = 0.543415193931859;
                            break;
                        case "emp_218078":
                            emp.Priority = 1.42002796098591;
                            break;
                        case "emp_218079":
                            emp.Priority = 1.09780379040064;
                            break;
                        case "emp_218080":
                            emp.Priority = 0.768373443404387;
                            break;
                        case "emp_218081":
                            emp.Priority = 0.69041223832891;
                            break;
                        case "emp_218083":
                            emp.Priority = 1.13411416236037;
                            break;
                        case "emp_220632":
                            emp.Priority = 0.631265358594835;
                            break;
                        case "emp_244304":
                            emp.Priority = 0.695882311182042;
                            break;
                        case "emp_247500":
                            emp.Priority = 0.818895345236592;
                            break;
                        case "emp_250492":
                            emp.Priority = 1.20908785504712;
                            break;
                        case "emp_250494":
                            emp.Priority = 0.838759041083399;
                            break;
                        case "emp_262688":
                            emp.Priority = 0.657086484207346;
                            break;
                        case "emp_262689":
                            emp.Priority = 0.876407899137776;
                            break;
                        case "emp_262690":
                            emp.Priority = 0.641957225809785;
                            break;
                        case "emp_262691":
                            emp.Priority = 1.07400818056148;
                            break;
                        case "emp_262692":
                            emp.Priority = 0.559457071153194;
                            break;
                        case "emp_262782":
                            emp.Priority = 0.974085994751233;
                            break;
                        case "emp_263784":
                            emp.Priority = 1.31170959249684;
                            break;
                        case "emp_285461":
                            emp.Priority = 1.36313762602542;
                            break;
                        case "emp_285463":
                            emp.Priority = 0.902360995953139;
                            break;
                        case "emp_285466":
                            emp.Priority = 1.46156240252851;
                            break;
                        case "emp_285467":
                            emp.Priority = 0.768710038284171;
                            break;
                        case "emp_286111":
                            emp.Priority = 1.44033345065095;
                            break;
                        case "emp_286238":
                            emp.Priority = 1.01567632403023;
                            break;
                        case "emp_286239":
                            emp.Priority = 1.42564634137119;
                            break;
                        case "emp_289039":
                            emp.Priority = 1.47847331128012;
                            break;
                        case "emp_289043":
                            emp.Priority = 0.853123822413908;
                            break;
                        case "emp_289045":
                            emp.Priority = 1.44047518304571;
                            break;
                        case "emp_289046":
                            emp.Priority = 0.980562072471046;
                            break;
                        case "emp_289526":
                            emp.Priority = 0.908407443393212;
                            break;
                        case "emp_289528":
                            emp.Priority = 1.40882376297788;
                            break;
                        case "emp_289529":
                            emp.Priority = 1.29654523581106;
                            break;
                        case "emp_309898":
                            emp.Priority = 0.661826826241718;
                            break;
                        case "emp_311347":
                            emp.Priority = 0.925215314806074;
                            break;
                        case "emp_315444":
                            emp.Priority = 0.639054732461951;
                            break;
                        case "emp_315447":
                            emp.Priority = 0.779082372448911;
                            break;
                        case "emp_315448":
                            emp.Priority = 0.945708741175806;
                            break;
                        case "emp_315449":
                            emp.Priority = 1.36887114395801;
                            break;
                        case "emp_315450":
                            emp.Priority = 1.49869589181556;
                            break;
                        case "emp_318148":
                            emp.Priority = 0.676264497533564;
                            break;
                        case "emp_328720":
                            emp.Priority = 1.27230693203039;
                            break;
                        case "emp_328722":
                            emp.Priority = 1.03138712119841;
                            break;
                        case "emp_329873":
                            emp.Priority = 0.966271550611719;
                            break;
                        case "emp_333858":
                            emp.Priority = 1.00184927624736;
                            break;
                        case "emp_333863":
                            emp.Priority = 0.961699557705642;
                            break;
                        case "emp_339907":
                            emp.Priority = 0.77379614872569;
                            break;
                        case "emp_339911":
                            emp.Priority = 0.669162271157448;
                            break;
                        case "emp_339913":
                            emp.Priority = 0.591252498371179;
                            break;
                        case "emp_339915":
                            emp.Priority = 1.48228326718429;
                            break;
                        case "emp_339918":
                            emp.Priority = 1.07534546990662;
                            break;
                        case "emp_339920":
                            emp.Priority = 0.817108354678894;
                            break;
                        case "emp_339921":
                            emp.Priority = 0.97277548325843;
                            break;
                        case "emp_339922":
                            emp.Priority = 0.744634667525363;
                            break;
                        case "emp_339924":
                            emp.Priority = 0.878436492932232;
                            break;
                        case "emp_339926":
                            emp.Priority = 1.11224101838294;
                            break;
                        case "emp_339927":
                            emp.Priority = 1.27470515564769;
                            break;
                        case "emp_342276":
                            emp.Priority = 1.47969451033496;
                            break;
                        case "emp_372995":
                            emp.Priority = 0.582127466370411;
                            break;
                        case "emp_372996":
                            emp.Priority = 0.842727102033201;
                            break;
                        case "emp_377363":
                            emp.Priority = 0.711938927048789;
                            break;
                        case "emp_377366":
                            emp.Priority = 0.780990339946463;
                            break;
                        case "emp_387759":
                            emp.Priority = 0.690790175549123;
                            break;
                        case "emp_389058":
                            emp.Priority = 1.215320238711;
                            break;
                        case "emp_413664":
                            emp.Priority = 1.41048790184338;
                            break;
                        case "emp_425497":
                            emp.Priority = 1.30026409206924;
                            break;
                        case "emp_425502":
                            emp.Priority = 1.04221380527234;
                            break;
                        case "emp_426316":
                            emp.Priority = 1.49525965796563;
                            break;
                        case "emp_426442":
                            emp.Priority = 1.4511925843317;
                            break;
                        case "emp_426447":
                            emp.Priority = 0.502902493347834;
                            break;
                        case "emp_426455":
                            emp.Priority = 0.794925808112568;
                            break;
                        case "emp_426461":
                            emp.Priority = 1.11374832997739;
                            break;
                        case "emp_427984":
                            emp.Priority = 1.10521485079323;
                            break;
                        case "emp_428070":
                            emp.Priority = 1.31301370068128;
                            break;
                        case "emp_430078":
                            emp.Priority = 1.18687312849186;
                            break;
                        case "emp_430086":
                            emp.Priority = 1.13005406392275;
                            break;
                        case "emp_433921":
                            emp.Priority = 0.930175281330093;
                            break;
                        case "emp_450443":
                            emp.Priority = 1.30243848767245;
                            break;
                        case "emp_472259":
                            emp.Priority = 0.938484174403587;
                            break;
                        case "emp_472260":
                            emp.Priority = 0.553976766324591;
                            break;
                        case "emp_485559":
                            emp.Priority = 1.15185019264549;
                            break;
                        case "emp_509805":
                            emp.Priority = 1.30931104012267;
                            break;
                        case "emp_509832":
                            emp.Priority = 0.76187132404273;
                            break;
                        case "emp_509838":
                            emp.Priority = 1.45819191586142;
                            break;
                        case "emp_509854":
                            emp.Priority = 1.40521660256442;
                            break;
                        case "emp_517374":
                            emp.Priority = 0.591299088714318;
                            break;
                        case "emp_517380":
                            emp.Priority = 0.93604827971945;
                            break;
                        case "emp_517386":
                            emp.Priority = 1.05191056828569;
                            break;
                        case "emp_517448":
                            emp.Priority = 1.28339033330949;
                            break;
                        case "emp_517455":
                            emp.Priority = 1.31297429642313;
                            break;
                        case "emp_517458":
                            emp.Priority = 0.864349576814263;
                            break;
                        case "emp_517462":
                            emp.Priority = 0.799387862113951;
                            break;
                        case "emp_517474":
                            emp.Priority = 0.863581274805395;
                            break;
                        case "emp_517482":
                            emp.Priority = 1.0261440419248;
                            break;
                        case "emp_517529":
                            emp.Priority = 1.28675696476677;
                            break;
                        case "emp_517539":
                            emp.Priority = 1.3952741575871;
                            break;
                        case "emp_523760":
                            emp.Priority = 1.08151675648127;
                            break;
                        case "emp_535079":
                            emp.Priority = 1.31606688248742;
                            break;
                        case "emp_541455":
                            emp.Priority = 1.05578364876834;
                            break;
                        case "emp_541457":
                            emp.Priority = 1.20158518417812;
                            break;
                        case "emp_562945":
                            emp.Priority = 1.4194857524333;
                            break;
                        case "emp_703434":
                            emp.Priority = 0.778536490760062;
                            break;
                        case "emp_874712":
                            emp.Priority = 0.717969686825746;
                            break;
                    }
                }
            }
            else if (portalName.Equals("admiralhotel"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    switch (emp.EmployeeId)
                    {
                        case "emp_43354":
                            emp.Priority = 0.844246933397021;
                            break;
                        case "emp_43358":
                            emp.Priority = 1.07200819233992;
                            break;
                        case "emp_43369":
                            emp.Priority = 1.18168594533656;
                            break;
                        case "emp_43374":
                            emp.Priority = 1.36188223485923;
                            break;
                        case "emp_43379":
                            emp.Priority = 1.08940350384889;
                            break;
                        case "emp_43380":
                            emp.Priority = 0.732405776731859;
                            break;
                        case "emp_43421":
                            emp.Priority = 0.829938393239835;
                            break;
                        case "emp_43422":
                            emp.Priority = 0.977666701878266;
                            break;
                        case "emp_43423":
                            emp.Priority = 0.625835410843527;
                            break;
                        case "emp_43425":
                            emp.Priority = 1.40757356533202;
                            break;
                        case "emp_61518":
                            emp.Priority = 1.40978655494274;
                            break;
                        case "emp_70464":
                            emp.Priority = 1.08688436708734;
                            break;
                        case "emp_77894":
                            emp.Priority = 0.810878207586183;
                            break;
                        case "emp_82020":
                            emp.Priority = 1.28571512493571;
                            break;
                        case "emp_84288":
                            emp.Priority = 1.43667958720433;
                            break;
                        case "emp_104912":
                            emp.Priority = 1.37081926449706;
                            break;
                        case "emp_138350":
                            emp.Priority = 0.628399868089892;
                            break;
                        case "emp_147235":
                            emp.Priority = 1.36909090674906;
                            break;
                        case "emp_192280":
                            emp.Priority = 0.610451684384817;
                            break;
                        case "emp_223976":
                            emp.Priority = 0.647967816865057;
                            break;
                        case "emp_233326":
                            emp.Priority = 1.35266963711598;
                            break;
                        case "emp_270072":
                            emp.Priority = 1.0603085158208;
                            break;
                        case "emp_281052":
                            emp.Priority = 0.587452178396029;
                            break;
                        case "emp_286885":
                            emp.Priority = 1.09540360029573;
                            break;
                        case "emp_309981":
                            emp.Priority = 1.49313987511822;
                            break;
                        case "emp_311974":
                            emp.Priority = 0.921352432305623;
                            break;
                        case "emp_315537":
                            emp.Priority = 0.941201694980823;
                            break;
                        case "emp_316897":
                            emp.Priority = 1.07358914407603;
                            break;
                        case "emp_340771":
                            emp.Priority = 0.775173599959898;
                            break;
                        case "emp_347246":
                            emp.Priority = 1.18433358179607;
                            break;
                        case "emp_381299":
                            emp.Priority = 0.754139170168964;
                            break;
                        case "emp_394841":
                            emp.Priority = 1.3383133862346;
                            break;
                        case "emp_441817":
                            emp.Priority = 1.30807176037136;
                            break;
                        case "emp_467657":
                            emp.Priority = 0.793758568956404;
                            break;
                        case "emp_495750":
                            emp.Priority = 1.14462606964848;
                            break;
                        case "emp_528277":
                            emp.Priority = 1.09323318330256;
                            break;
                        case "emp_528360":
                            emp.Priority = 1.07215063021153;
                            break;
                        case "emp_543782":
                            emp.Priority = 1.22571330737589;
                            break;
                        case "emp_549070":
                            emp.Priority = 0.627535420529328;
                            break;
                        case "emp_549072":
                            emp.Priority = 1.40237056412844;
                            break;
                        case "emp_549074":
                            emp.Priority = 0.983478378729652;
                            break;
                        case "emp_555826":
                            emp.Priority = 0.958674659234786;
                            break;
                        case "emp_737489":
                            emp.Priority = 1.02227561200144;
                            break;
                        case "emp_897590":
                            emp.Priority = 1.10913648391568;
                            break;
                        case "emp_897595":
                            emp.Priority = 1.38440144615453;
                            break;
                        case "emp_913828":
                            emp.Priority = 1.48013246291323;
                            break;
                        case "emp_917368":
                            emp.Priority = 0.717073241349809;
                            break;
                        case "emp_43104":
                            emp.Priority = 0.819024408384703;
                            break;
                        case "emp_43114":
                            emp.Priority = 1.064462750016;
                            break;
                        case "emp_43346":
                            emp.Priority = 1.22570689661694;
                            break;
                        case "emp_43348":
                            emp.Priority = 1.22551020315174;
                            break;
                        case "emp_43349":
                            emp.Priority = 0.991459080712618;
                            break;
                        case "emp_43350":
                            emp.Priority = 1.03050647980091;
                            break;
                        case "emp_43351":
                            emp.Priority = 0.92567339605916;
                            break;
                        case "emp_43352":
                            emp.Priority = 1.43488481125556;
                            break;
                        case "emp_43353":
                            emp.Priority = 1.28393841757622;
                            break;
                        case "emp_43355":
                            emp.Priority = 0.984556013943886;
                            break;
                        case "emp_43356":
                            emp.Priority = 0.586282345040833;
                            break;
                        case "emp_43357":
                            emp.Priority = 1.36874235974101;
                            break;
                        case "emp_43359":
                            emp.Priority = 0.66805107154327;
                            break;
                        case "emp_43360":
                            emp.Priority = 1.29120408175104;
                            break;
                        case "emp_43361":
                            emp.Priority = 1.25634924916381;
                            break;
                        case "emp_43362":
                            emp.Priority = 0.702493101918368;
                            break;
                        case "emp_43363":
                            emp.Priority = 0.941501829047455;
                            break;
                        case "emp_43364":
                            emp.Priority = 1.15343439516306;
                            break;
                        case "emp_43365":
                            emp.Priority = 0.571473168708139;
                            break;
                        case "emp_43366":
                            emp.Priority = 1.27881260671598;
                            break;
                        case "emp_43367":
                            emp.Priority = 0.517119638629779;
                            break;
                        case "emp_43368":
                            emp.Priority = 0.641089055287227;
                            break;
                        case "emp_43370":
                            emp.Priority = 0.843446403901766;
                            break;
                        case "emp_43371":
                            emp.Priority = 0.79866863428553;
                            break;
                        case "emp_43372":
                            emp.Priority = 0.902686560714006;
                            break;
                        case "emp_43373":
                            emp.Priority = 1.24155548621973;
                            break;
                        case "emp_43375":
                            emp.Priority = 0.708081120256372;
                            break;
                        case "emp_43376":
                            emp.Priority = 1.1644894381354;
                            break;
                        case "emp_43377":
                            emp.Priority = 0.893994977881198;
                            break;
                        case "emp_43378":
                            emp.Priority = 0.53803290381936;
                            break;
                        case "emp_43381":
                            emp.Priority = 0.97831569448035;
                            break;
                        case "emp_43382":
                            emp.Priority = 1.2110021541412;
                            break;
                        case "emp_43383":
                            emp.Priority = 0.513007412204988;
                            break;
                        case "emp_43384":
                            emp.Priority = 0.704279190955814;
                            break;
                        case "emp_43417":
                            emp.Priority = 0.62217728659612;
                            break;
                        case "emp_43418":
                            emp.Priority = 0.509126394060034;
                            break;
                        case "emp_43419":
                            emp.Priority = 1.04946670334296;
                            break;
                        case "emp_43420":
                            emp.Priority = 1.45882337864434;
                            break;
                        case "emp_43424":
                            emp.Priority = 1.26268008945635;
                            break;
                        case "emp_43769":
                            emp.Priority = 0.807806906433686;
                            break;
                        case "emp_44232":
                            emp.Priority = 0.882398364312201;
                            break;
                        case "emp_45975":
                            emp.Priority = 0.858873757700843;
                            break;
                        case "emp_45976":
                            emp.Priority = 1.36068765207226;
                            break;
                        case "emp_46550":
                            emp.Priority = 0.608677169358673;
                            break;
                        case "emp_50650":
                            emp.Priority = 0.985868285170695;
                            break;
                        case "emp_51869":
                            emp.Priority = 1.35697094763488;
                            break;
                        case "emp_51878":
                            emp.Priority = 1.45948434898606;
                            break;
                        case "emp_52864":
                            emp.Priority = 0.611166482377409;
                            break;
                        case "emp_55608":
                            emp.Priority = 1.22712912956585;
                            break;
                        case "emp_58797":
                            emp.Priority = 0.756181557316418;
                            break;
                        case "emp_60822":
                            emp.Priority = 0.58077378295398;
                            break;
                        case "emp_61998":
                            emp.Priority = 1.45570208875262;
                            break;
                        case "emp_62057":
                            emp.Priority = 1.31292827744639;
                            break;
                        case "emp_62593":
                            emp.Priority = 0.701319856197257;
                            break;
                        case "emp_62595":
                            emp.Priority = 0.69995360272003;
                            break;
                        case "emp_65202":
                            emp.Priority = 0.677935353097476;
                            break;
                        case "emp_70323":
                            emp.Priority = 0.72101634611423;
                            break;
                        case "emp_72202":
                            emp.Priority = 0.92703826233141;
                            break;
                        case "emp_77256":
                            emp.Priority = 0.82282364243773;
                            break;
                        case "emp_77883":
                            emp.Priority = 1.24990359449289;
                            break;
                        case "emp_83040":
                            emp.Priority = 0.822425359544542;
                            break;
                        case "emp_83715":
                            emp.Priority = 1.26118395792375;
                            break;
                        case "emp_92779":
                            emp.Priority = 1.04088983337436;
                            break;
                        case "emp_98848":
                            emp.Priority = 1.24590551375686;
                            break;
                        case "emp_143442":
                            emp.Priority = 0.506240319463536;
                            break;
                        case "emp_150942":
                            emp.Priority = 0.875280190899633;
                            break;
                        case "emp_160182":
                            emp.Priority = 1.35573494753602;
                            break;
                        case "emp_169747":
                            emp.Priority = 1.46377188058746;
                            break;
                        case "emp_177769":
                            emp.Priority = 1.16902679515492;
                            break;
                        case "emp_220215":
                            emp.Priority = 1.24722285510377;
                            break;
                        case "emp_221143":
                            emp.Priority = 1.15302639857541;
                            break;
                        case "emp_221732":
                            emp.Priority = 0.98267845040312;
                            break;
                        case "emp_229376":
                            emp.Priority = 1.39075430570671;
                            break;
                        case "emp_233325":
                            emp.Priority = 1.26366626227445;
                            break;
                        case "emp_233327":
                            emp.Priority = 0.896414242403775;
                            break;
                        case "emp_320843":
                            emp.Priority = 1.15824588092894;
                            break;
                        case "emp_347245":
                            emp.Priority = 0.780401403214969;
                            break;
                        case "emp_350000":
                            emp.Priority = 0.734769234543093;
                            break;
                        case "emp_366028":
                            emp.Priority = 1.31280034911484;
                            break;
                        case "emp_377425":
                            emp.Priority = 1.04571561307912;
                            break;
                        case "emp_529350":
                            emp.Priority = 1.28207113723367;
                            break;
                        case "emp_542922":
                            emp.Priority = 0.596914637878963;
                            break;
                        case "emp_543413":
                            emp.Priority = 1.43736030856956;
                            break;
                        case "emp_593585":
                            emp.Priority = 0.637813420564781;
                            break;
                    }

                }
            }
            else if (portalName.Equals("42raw"))
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    switch (emp.EmployeeId)
                    {
                        case "emp_3650":
                            emp.Priority = 0.855440053323023;
                            break;
                        case "emp_33647":
                            emp.Priority = 0.728709645675826;
                            break;
                        case "emp_172626":
                            emp.Priority = 0.700130474846871;
                            break;
                        case "emp_181109":
                            emp.Priority = 1.3257770607368;
                            break;
                        case "emp_254929":
                            emp.Priority = 0.916726366345178;
                            break;
                        case "emp_296226":
                            emp.Priority = 0.863726116420574;
                            break;
                        case "emp_558302":
                            emp.Priority = 0.648302296245611;
                            break;
                        case "emp_559968":
                            emp.Priority = 0.670109840654819;
                            break;
                        case "emp_648960":
                            emp.Priority = 0.958866413430714;
                            break;
                        case "emp_755188":
                            emp.Priority = 0.815212319751835;
                            break;
                        case "emp_800858":
                            emp.Priority = 1.08851422629716;
                            break;
                        case "emp_825461":
                            emp.Priority = 0.923792776383363;
                            break;
                        case "emp_863185":
                            emp.Priority = 0.970100972554693;
                            break;
                        case "emp_881706":
                            emp.Priority = 1.46607905345321;
                            break;
                        case "emp_902266":
                            emp.Priority = 0.745886967631936;
                            break;
                        case "emp_911485":
                            emp.Priority = 0.507845887917953;
                            break;
                        case "emp_949542":
                            emp.Priority = 0.876271381218113;
                            break;
                        case "emp_951622":
                            emp.Priority = 1.06053479926686;
                            break;
                        case "emp_3665":
                            emp.Priority = 1.37376192252793;
                            break;
                        case "emp_3667":
                            emp.Priority = 1.44780607519104;
                            break;
                        case "emp_3668":
                            emp.Priority = 0.591010510498197;
                            break;
                        case "emp_3669":
                            emp.Priority = 0.622548352052713;
                            break;
                        case "emp_3670":
                            emp.Priority = 0.600774681242544;
                            break;
                        case "emp_3671":
                            emp.Priority = 0.662944279686988;
                            break;
                        case "emp_3672":
                            emp.Priority = 1.2874630306789;
                            break;
                        case "emp_3673":
                            emp.Priority = 1.48313290625025;
                            break;
                        case "emp_3674":
                            emp.Priority = 0.782777553555918;
                            break;
                        case "emp_3675":
                            emp.Priority = 0.678450896487781;
                            break;
                        case "emp_3676":
                            emp.Priority = 0.956608204849348;
                            break;
                        case "emp_3679":
                            emp.Priority = 0.867520297117308;
                            break;
                        case "emp_3814":
                            emp.Priority = 0.846195895851681;
                            break;
                        case "emp_3850":
                            emp.Priority = 0.625197209010458;
                            break;
                        case "emp_3876":
                            emp.Priority = 0.560624250239052;
                            break;
                        case "emp_3880":
                            emp.Priority = 0.604604524143322;
                            break;
                        case "emp_3936":
                            emp.Priority = 0.712918145681228;
                            break;
                        case "emp_3996":
                            emp.Priority = 0.825972095283667;
                            break;
                        case "emp_4002":
                            emp.Priority = 0.919758457420281;
                            break;
                        case "emp_4064":
                            emp.Priority = 0.746136426108953;
                            break;
                        case "emp_4196":
                            emp.Priority = 1.07533595924048;
                            break;
                        case "emp_4541":
                            emp.Priority = 0.513458377222278;
                            break;
                        case "emp_4783":
                            emp.Priority = 1.49082340439354;
                            break;
                        case "emp_5143":
                            emp.Priority = 0.968600721316692;
                            break;
                        case "emp_5144":
                            emp.Priority = 0.803881746858303;
                            break;
                        case "emp_5145":
                            emp.Priority = 1.31676182328573;
                            break;
                        case "emp_5146":
                            emp.Priority = 1.10558450157083;
                            break;
                        case "emp_5532":
                            emp.Priority = 1.0456453513101;
                            break;
                        case "emp_5672":
                            emp.Priority = 1.48988883289969;
                            break;
                        case "emp_6208":
                            emp.Priority = 1.28852398218053;
                            break;
                        case "emp_24972":
                            emp.Priority = 0.722607154968477;
                            break;
                        case "emp_24986":
                            emp.Priority = 0.598224347503029;
                            break;
                        case "emp_25262":
                            emp.Priority = 1.47588409528876;
                            break;
                        case "emp_25268":
                            emp.Priority = 1.30482690446304;
                            break;
                        case "emp_25473":
                            emp.Priority = 0.659110150374058;
                            break;
                        case "emp_25839":
                            emp.Priority = 0.612622308131597;
                            break;
                        case "emp_25893":
                            emp.Priority = 1.2312309624307;
                            break;
                        case "emp_25960":
                            emp.Priority = 0.73289170127031;
                            break;
                        case "emp_26068":
                            emp.Priority = 0.627934964433282;
                            break;
                        case "emp_26110":
                            emp.Priority = 0.537186195159883;
                            break;
                        case "emp_26142":
                            emp.Priority = 0.538314030057897;
                            break;
                        case "emp_26143":
                            emp.Priority = 0.933593460094926;
                            break;
                        case "emp_26598":
                            emp.Priority = 0.580948562864656;
                            break;
                        case "emp_26599":
                            emp.Priority = 1.46985139975783;
                            break;
                        case "emp_27226":
                            emp.Priority = 1.21350163580547;
                            break;
                        case "emp_29543":
                            emp.Priority = 0.591346116313406;
                            break;
                        case "emp_29820":
                            emp.Priority = 1.46901642390015;
                            break;
                        case "emp_29835":
                            emp.Priority = 0.963317017286698;
                            break;
                        case "emp_32623":
                            emp.Priority = 0.863168526144311;
                            break;
                        case "emp_33646":
                            emp.Priority = 0.865496448411372;
                            break;
                        case "emp_33688":
                            emp.Priority = 1.25316090777198;
                            break;
                        case "emp_35324":
                            emp.Priority = 1.41991487234827;
                            break;
                        case "emp_35365":
                            emp.Priority = 1.08808743049767;
                            break;
                        case "emp_35834":
                            emp.Priority = 0.63013495510916;
                            break;
                        case "emp_35866":
                            emp.Priority = 1.48519884002637;
                            break;
                        case "emp_36261":
                            emp.Priority = 1.36030354530565;
                            break;
                        case "emp_36273":
                            emp.Priority = 1.45698267079749;
                            break;
                        case "emp_36683":
                            emp.Priority = 1.12240978918151;
                            break;
                        case "emp_36789":
                            emp.Priority = 1.31866660519441;
                            break;
                        case "emp_36811":
                            emp.Priority = 0.784012857956818;
                            break;
                        case "emp_39243":
                            emp.Priority = 1.05735977811616;
                            break;
                        case "emp_39284":
                            emp.Priority = 0.741817679368806;
                            break;
                        case "emp_40087":
                            emp.Priority = 1.49324407335056;
                            break;
                        case "emp_40674":
                            emp.Priority = 0.994253571375391;
                            break;
                        case "emp_50345":
                            emp.Priority = 1.4558437415193;
                            break;
                        case "emp_51514":
                            emp.Priority = 0.858383857346319;
                            break;
                        case "emp_51744":
                            emp.Priority = 0.891636201828549;
                            break;
                        case "emp_52567":
                            emp.Priority = 1.04136899138865;
                            break;
                        case "emp_53126":
                            emp.Priority = 1.4660870586364;
                            break;
                        case "emp_55572":
                            emp.Priority = 1.44800194210745;
                            break;
                        case "emp_58060":
                            emp.Priority = 0.873373561712621;
                            break;
                        case "emp_59270":
                            emp.Priority = 1.48002644441092;
                            break;
                        case "emp_59491":
                            emp.Priority = 0.698037130850385;
                            break;
                        case "emp_59950":
                            emp.Priority = 0.882572262260398;
                            break;
                        case "emp_60165":
                            emp.Priority = 0.707822396051056;
                            break;
                        case "emp_62091":
                            emp.Priority = 0.641742499145559;
                            break;
                        case "emp_64352":
                            emp.Priority = 1.43250981435762;
                            break;
                        case "emp_70485":
                            emp.Priority = 0.520972004635712;
                            break;
                        case "emp_70613":
                            emp.Priority = 1.25509908551122;
                            break;
                        case "emp_77752":
                            emp.Priority = 0.712535630544897;
                            break;
                        case "emp_77759":
                            emp.Priority = 1.34774539938557;
                            break;
                        case "emp_78145":
                            emp.Priority = 0.642267484284131;
                            break;
                        case "emp_78150":
                            emp.Priority = 0.682476825165784;
                            break;
                        case "emp_78394":
                            emp.Priority = 1.12439238448832;
                            break;
                        case "emp_78395":
                            emp.Priority = 0.535363074408548;
                            break;
                        case "emp_79381":
                            emp.Priority = 0.802692282620209;
                            break;
                        case "emp_79394":
                            emp.Priority = 1.01013691700536;
                            break;
                        case "emp_79919":
                            emp.Priority = 1.3457491401796;
                            break;
                        case "emp_79973":
                            emp.Priority = 1.31962806443666;
                            break;
                        case "emp_79979":
                            emp.Priority = 0.798806605068411;
                            break;
                        case "emp_80009":
                            emp.Priority = 0.655639637334104;
                            break;
                        case "emp_80344":
                            emp.Priority = 0.608821173249195;
                            break;
                        case "emp_80352":
                            emp.Priority = 0.9142250960759;
                            break;
                        case "emp_80353":
                            emp.Priority = 1.34392210647646;
                            break;
                        case "emp_81396":
                            emp.Priority = 0.979826417043724;
                            break;
                        case "emp_81979":
                            emp.Priority = 1.29649635068909;
                            break;
                        case "emp_82048":
                            emp.Priority = 0.940349386744364;
                            break;
                        case "emp_82212":
                            emp.Priority = 1.08669499148926;
                            break;
                        case "emp_82251":
                            emp.Priority = 0.514007658238526;
                            break;
                        case "emp_82426":
                            emp.Priority = 0.855117778459153;
                            break;
                        case "emp_83176":
                            emp.Priority = 1.19970991448486;
                            break;
                        case "emp_83178":
                            emp.Priority = 0.927647432511508;
                            break;
                        case "emp_85090":
                            emp.Priority = 0.997229958650298;
                            break;
                        case "emp_85925":
                            emp.Priority = 0.915166584036856;
                            break;
                        case "emp_86888":
                            emp.Priority = 1.49212288669875;
                            break;
                        case "emp_87074":
                            emp.Priority = 1.27313446336106;
                            break;
                        case "emp_88713":
                            emp.Priority = 1.22187774149788;
                            break;
                        case "emp_88884":
                            emp.Priority = 0.705515168237274;
                            break;
                        case "emp_90489":
                            emp.Priority = 1.4223125590581;
                            break;
                        case "emp_92140":
                            emp.Priority = 1.34345634088081;
                            break;
                        case "emp_92784":
                            emp.Priority = 1.42779373094802;
                            break;
                        case "emp_95524":
                            emp.Priority = 1.43601066616178;
                            break;
                        case "emp_96329":
                            emp.Priority = 1.36731070367028;
                            break;
                        case "emp_96941":
                            emp.Priority = 1.10613097464951;
                            break;
                        case "emp_97092":
                            emp.Priority = 0.936267458571246;
                            break;
                        case "emp_105327":
                            emp.Priority = 0.915092293832029;
                            break;
                        case "emp_106073":
                            emp.Priority = 0.559340854203022;
                            break;
                        case "emp_106291":
                            emp.Priority = 0.868851688862244;
                            break;
                        case "emp_106321":
                            emp.Priority = 0.958890496966844;
                            break;
                        case "emp_106351":
                            emp.Priority = 1.1531514588991;
                            break;
                        case "emp_109450":
                            emp.Priority = 1.34824694034096;
                            break;
                        case "emp_109833":
                            emp.Priority = 1.04588706164895;
                            break;
                        case "emp_109999":
                            emp.Priority = 1.22174092695198;
                            break;
                        case "emp_110763":
                            emp.Priority = 1.16728045356799;
                            break;
                        case "emp_124375":
                            emp.Priority = 1.29236230477335;
                            break;
                        case "emp_138281":
                            emp.Priority = 0.764552388463427;
                            break;
                        case "emp_140205":
                            emp.Priority = 1.06580134833502;
                            break;
                        case "emp_143514":
                            emp.Priority = 0.85411502437392;
                            break;
                        case "emp_145224":
                            emp.Priority = 1.40274584521667;
                            break;
                        case "emp_145226":
                            emp.Priority = 0.911326045361965;
                            break;
                        case "emp_146817":
                            emp.Priority = 1.2013931124012;
                            break;
                        case "emp_147741":
                            emp.Priority = 0.845814822868358;
                            break;
                        case "emp_149938":
                            emp.Priority = 0.506964346397186;
                            break;
                        case "emp_149968":
                            emp.Priority = 0.899981307052067;
                            break;
                        case "emp_150917":
                            emp.Priority = 1.01282571606004;
                            break;
                        case "emp_151124":
                            emp.Priority = 0.920097966874064;
                            break;
                        case "emp_151306":
                            emp.Priority = 1.14503752563383;
                            break;
                        case "emp_154319":
                            emp.Priority = 1.26731024112893;
                            break;
                        case "emp_154390":
                            emp.Priority = 1.13226949778957;
                            break;
                        case "emp_154443":
                            emp.Priority = 0.762228611047486;
                            break;
                        case "emp_156094":
                            emp.Priority = 1.08081454112232;
                            break;
                        case "emp_156952":
                            emp.Priority = 0.804621748768083;
                            break;
                        case "emp_158312":
                            emp.Priority = 1.4234365811215;
                            break;
                        case "emp_158425":
                            emp.Priority = 1.47617172355585;
                            break;
                        case "emp_158427":
                            emp.Priority = 0.871012874120387;
                            break;
                        case "emp_163171":
                            emp.Priority = 0.719628971172324;
                            break;
                        case "emp_166163":
                            emp.Priority = 0.74151046957891;
                            break;
                        case "emp_168177":
                            emp.Priority = 1.30809412142639;
                            break;
                        case "emp_169255":
                            emp.Priority = 0.907654647905219;
                            break;
                        case "emp_172377":
                            emp.Priority = 0.564734123211696;
                            break;
                        case "emp_181089":
                            emp.Priority = 1.23715549648607;
                            break;
                        case "emp_181220":
                            emp.Priority = 0.57149769788212;
                            break;
                        case "emp_181440":
                            emp.Priority = 0.627804494522421;
                            break;
                        case "emp_182229":
                            emp.Priority = 0.860856199339431;
                            break;
                        case "emp_182972":
                            emp.Priority = 1.00687083811819;
                            break;
                        case "emp_183593":
                            emp.Priority = 0.653822852835908;
                            break;
                        case "emp_184705":
                            emp.Priority = 1.20590650555953;
                            break;
                        case "emp_184926":
                            emp.Priority = 1.32994950508231;
                            break;
                        case "emp_185015":
                            emp.Priority = 1.12280427926351;
                            break;
                        case "emp_185030":
                            emp.Priority = 1.22757049823532;
                            break;
                        case "emp_186009":
                            emp.Priority = 0.707333115026044;
                            break;
                        case "emp_186743":
                            emp.Priority = 0.867762717123964;
                            break;
                        case "emp_191889":
                            emp.Priority = 0.8027693230206;
                            break;
                        case "emp_191904":
                            emp.Priority = 1.01098651369614;
                            break;
                        case "emp_192442":
                            emp.Priority = 0.642063228479616;
                            break;
                        case "emp_196123":
                            emp.Priority = 1.08197890807967;
                            break;
                        case "emp_201315":
                            emp.Priority = 1.42904631976459;
                            break;
                        case "emp_202471":
                            emp.Priority = 0.967329396618218;
                            break;
                        case "emp_202757":
                            emp.Priority = 0.593305258589473;
                            break;
                        case "emp_207927":
                            emp.Priority = 0.516169491697182;
                            break;
                        case "emp_208568":
                            emp.Priority = 1.2700547681982;
                            break;
                        case "emp_210026":
                            emp.Priority = 0.792030613074093;
                            break;
                        case "emp_215036":
                            emp.Priority = 1.23658219107268;
                            break;
                        case "emp_223877":
                            emp.Priority = 0.696661885919358;
                            break;
                        case "emp_224688":
                            emp.Priority = 0.572336917776771;
                            break;
                        case "emp_226523":
                            emp.Priority = 1.04362519157288;
                            break;
                        case "emp_233166":
                            emp.Priority = 1.12245048052745;
                            break;
                        case "emp_233168":
                            emp.Priority = 1.24556920339613;
                            break;
                        case "emp_234198":
                            emp.Priority = 0.796267579447603;
                            break;
                        case "emp_243776":
                            emp.Priority = 1.07273333360103;
                            break;
                        case "emp_244377":
                            emp.Priority = 0.523041918884517;
                            break;
                        case "emp_246987":
                            emp.Priority = 1.25770722690863;
                            break;
                        case "emp_247630":
                            emp.Priority = 1.4464603764687;
                            break;
                        case "emp_247631":
                            emp.Priority = 1.33801172200498;
                            break;
                        case "emp_249585":
                            emp.Priority = 1.1741705488759;
                            break;
                        case "emp_252714":
                            emp.Priority = 1.12989541451907;
                            break;
                        case "emp_253340":
                            emp.Priority = 0.718010328345937;
                            break;
                        case "emp_254866":
                            emp.Priority = 1.14610814705775;
                            break;
                        case "emp_258224":
                            emp.Priority = 1.39311046893388;
                            break;
                        case "emp_259748":
                            emp.Priority = 0.859002863224131;
                            break;
                        case "emp_259749":
                            emp.Priority = 1.21419146131454;
                            break;
                        case "emp_260407":
                            emp.Priority = 1.31508802055153;
                            break;
                        case "emp_261101":
                            emp.Priority = 0.644505961865422;
                            break;
                        case "emp_264330":
                            emp.Priority = 1.40469899955424;
                            break;
                        case "emp_264339":
                            emp.Priority = 0.554895496021442;
                            break;
                        case "emp_302221":
                            emp.Priority = 0.71305182399836;
                            break;
                        case "emp_303238":
                            emp.Priority = 0.501852425747482;
                            break;
                        case "emp_304548":
                            emp.Priority = 0.912450067425356;
                            break;
                        case "emp_304550":
                            emp.Priority = 1.33410849507624;
                            break;
                        case "emp_304554":
                            emp.Priority = 1.28903396604072;
                            break;
                        case "emp_307534":
                            emp.Priority = 0.790582651407729;
                            break;
                        case "emp_315382":
                            emp.Priority = 1.27418107296069;
                            break;
                        case "emp_327213":
                            emp.Priority = 1.21478886283691;
                            break;
                        case "emp_344146":
                            emp.Priority = 0.891485156208037;
                            break;
                        case "emp_346338":
                            emp.Priority = 0.794679355013501;
                            break;
                        case "emp_349771":
                            emp.Priority = 0.945124883411976;
                            break;
                        case "emp_356163":
                            emp.Priority = 0.834915506809445;
                            break;
                        case "emp_358633":
                            emp.Priority = 1.43114260860306;
                            break;
                        case "emp_369592":
                            emp.Priority = 0.78851928156266;
                            break;
                        case "emp_407818":
                            emp.Priority = 1.46324564654531;
                            break;
                        case "emp_413929":
                            emp.Priority = 1.03137237230845;
                            break;
                        case "emp_417727":
                            emp.Priority = 1.4603373021634;
                            break;
                        case "emp_429167":
                            emp.Priority = 1.03368192563471;
                            break;
                        case "emp_439599":
                            emp.Priority = 0.550070945662479;
                            break;
                        case "emp_441914":
                            emp.Priority = 1.20452857935081;
                            break;
                        case "emp_444441":
                            emp.Priority = 0.949625888117415;
                            break;
                        case "emp_447568":
                            emp.Priority = 0.921302340655263;
                            break;
                        case "emp_447714":
                            emp.Priority = 0.964757601015622;
                            break;
                        case "emp_455107":
                            emp.Priority = 1.33681596482024;
                            break;
                        case "emp_455151":
                            emp.Priority = 1.01216781396054;
                            break;
                        case "emp_461195":
                            emp.Priority = 0.86396857973373;
                            break;
                        case "emp_463446":
                            emp.Priority = 0.78293817270684;
                            break;
                        case "emp_492891":
                            emp.Priority = 1.07421892768434;
                            break;
                        case "emp_492893":
                            emp.Priority = 1.23430239536534;
                            break;
                        case "emp_498281":
                            emp.Priority = 0.801978030382645;
                            break;
                        case "emp_507191":
                            emp.Priority = 1.45496674764667;
                            break;
                        case "emp_517550":
                            emp.Priority = 0.647524651208671;
                            break;
                        case "emp_557013":
                            emp.Priority = 1.33188319151843;
                            break;
                        case "emp_562108":
                            emp.Priority = 0.641766389897916;
                            break;
                        case "emp_672269":
                            emp.Priority = 1.35928509377841;
                            break;
                        case "emp_678807":
                            emp.Priority = 1.0417727658254;
                            break;
                        case "emp_694540":
                            emp.Priority = 0.710000413102098;
                            break;
                        case "emp_694830":
                            emp.Priority = 1.41146070831989;
                            break;
                        case "emp_698525":
                            emp.Priority = 1.00723361340688;
                            break;
                        case "emp_699405":
                            emp.Priority = 0.782150682193297;
                            break;
                        case "emp_704761":
                            emp.Priority = 0.748860845923825;
                            break;
                        case "emp_706993":
                            emp.Priority = 0.542918364071715;
                            break;
                        case "emp_709328":
                            emp.Priority = 1.05497522026066;
                            break;
                        case "emp_711966":
                            emp.Priority = 1.04333236699148;
                            break;
                        case "emp_714938":
                            emp.Priority = 0.72904566546392;
                            break;
                        case "emp_717326":
                            emp.Priority = 0.79497990770963;
                            break;
                        case "emp_748045":
                            emp.Priority = 0.786867719742874;
                            break;
                        case "emp_777381":
                            emp.Priority = 0.857588865495095;
                            break;
                        case "emp_808130":
                            emp.Priority = 1.42986482238856;
                            break;
                        case "emp_828132":
                            emp.Priority = 1.32763049091568;
                            break;
                        case "emp_841178":
                            emp.Priority = 1.25385415915114;
                            break;
                        case "emp_850027":
                            emp.Priority = 0.78140609491682;
                            break;
                        case "emp_864194":
                            emp.Priority = 0.594435016202943;
                            break;
                        case "emp_879907":
                            emp.Priority = 0.700170420203437;
                            break;
                        case "emp_908655":
                            emp.Priority = 1.10526960790403;
                            break;
                        case "emp_890497":
                            emp.Priority = 1.2917494833431;
                            break;
                    }
                }
            }
            else if (portalName.Equals("helenekilde"))
            {

                foreach (EmployeeObject emp in employeeObjects)
                {

                    switch (emp.EmployeeId)
                    {
                        case "emp_57211":
                            emp.Priority = 0.874817733361767;
                            break;
                        case "emp_57214":
                            emp.Priority = 0.559814588194627;
                            break;
                        case "emp_60824":
                            emp.Priority = 0.585447193628851;
                            break;
                        case "emp_61064":
                            emp.Priority = 0.916638753570914;
                            break;
                        case "emp_64897":
                            emp.Priority = 0.760520620858539;
                            break;
                        case "emp_85698":
                            emp.Priority = 0.938988048322028;
                            break;
                        case "emp_142565":
                            emp.Priority = 0.511864605830919;
                            break;
                        case "emp_156450":
                            emp.Priority = 1.29441881915295;
                            break;
                        case "emp_208148":
                            emp.Priority = 1.29571779062772;
                            break;
                        case "emp_211210":
                            emp.Priority = 1.31114175534395;
                            break;
                        case "emp_218489":
                            emp.Priority = 0.891484989035635;
                            break;
                        case "emp_218821":
                            emp.Priority = 1.4101297682664;
                            break;
                        case "emp_218822":
                            emp.Priority = 1.44737861396157;
                            break;
                        case "emp_284213":
                            emp.Priority = 1.14344444761213;
                            break;
                        case "emp_292367":
                            emp.Priority = 1.12772111065114;
                            break;
                        case "emp_299185":
                            emp.Priority = 0.540144965071299;
                            break;
                        case "emp_316606":
                            emp.Priority = 1.22050510613271;
                            break;
                        case "emp_321535":
                            emp.Priority = 0.778077841865866;
                            break;
                        case "emp_321537":
                            emp.Priority = 0.593758232003897;
                            break;
                        case "emp_321538":
                            emp.Priority = 1.24716541904358;
                            break;
                        case "emp_344150":
                            emp.Priority = 0.623817483486523;
                            break;
                        case "emp_352710":
                            emp.Priority = 0.934635730662679;
                            break;
                        case "emp_398394":
                            emp.Priority = 0.586570506490101;
                            break;
                        case "emp_440647":
                            emp.Priority = 1.21285840343351;
                            break;
                        case "emp_489089":
                            emp.Priority = 0.553733557953375;
                            break;
                        case "emp_490655":
                            emp.Priority = 1.24051199096279;
                            break;
                        case "emp_503925":
                            emp.Priority = 1.29200608878956;
                            break;
                        case "emp_547800":
                            emp.Priority = 1.08171149416906;
                            break;
                        case "emp_552368":
                            emp.Priority = 1.40114471451433;
                            break;
                        case "emp_554799":
                            emp.Priority = 0.546368688832209;
                            break;
                        case "emp_563224":
                            emp.Priority = 1.21916935207284;
                            break;
                        case "emp_568755":
                            emp.Priority = 0.704657756818765;
                            break;
                        case "emp_568766":
                            emp.Priority = 1.0574541909422;
                            break;
                        case "emp_604021":
                            emp.Priority = 0.53110652418393;
                            break;
                        case "emp_618179":
                            emp.Priority = 1.27290222364147;
                            break;
                        case "emp_627085":
                            emp.Priority = 0.781374397352978;
                            break;
                        case "emp_679494":
                            emp.Priority = 0.625303401204433;
                            break;
                        case "emp_682476":
                            emp.Priority = 1.08864175043471;
                            break;
                        case "emp_709713":
                            emp.Priority = 1.26888798678708;
                            break;
                        case "emp_711985":
                            emp.Priority = 0.527965947998672;
                            break;
                        case "emp_749616":
                            emp.Priority = 0.823946921771368;
                            break;
                        case "emp_789581":
                            emp.Priority = 0.570647666729357;
                            break;
                        case "emp_793114":
                            emp.Priority = 0.880803660666944;
                            break;
                        case "emp_813762":
                            emp.Priority = 1.32339302581893;
                            break;
                        case "emp_828638":
                            emp.Priority = 1.01086051273665;
                            break;
                        case "emp_828643":
                            emp.Priority = 0.797475204941572;
                            break;
                        case "emp_835518":
                            emp.Priority = 1.17328766205967;
                            break;
                        case "emp_846693":
                            emp.Priority = 0.581812166181305;
                            break;
                        case "emp_879772":
                            emp.Priority = 0.720017623258763;
                            break;
                        case "emp_884344":
                            emp.Priority = 1.43415674657289;
                            break;
                        case "emp_892166":
                            emp.Priority = 0.515316508251855;
                            break;
                        case "emp_897160":
                            emp.Priority = 1.42035713089647;
                            break;
                        case "emp_906096":
                            emp.Priority = 0.682090404062574;
                            break;
                        case "emp_914536":
                            emp.Priority = 1.36109221487357;
                            break;
                        case "emp_921057":
                            emp.Priority = 1.06477891773208;
                            break;
                        case "emp_926056":
                            emp.Priority = 1.44018200269909;
                            break;
                        case "emp_926087":
                            emp.Priority = 1.47324408170453;
                            break;
                        case "emp_926907":
                            emp.Priority = 0.872588790195337;
                            break;
                        case "emp_927630":
                            emp.Priority = 0.862905195617538;
                            break;
                        case "emp_929142":
                            emp.Priority = 1.02000862989575;
                            break;
                        case "emp_929148":
                            emp.Priority = 1.14698195953247;
                            break;
                        case "emp_929184":
                            emp.Priority = 0.930153111661856;
                            break;
                        case "emp_930438":
                            emp.Priority = 1.39327410463862;
                            break;
                        case "emp_930449":
                            emp.Priority = 1.24934910179551;
                            break;
                        case "emp_931015":
                            emp.Priority = 0.591972403271111;
                            break;
                        case "emp_935978":
                            emp.Priority = 0.68682723221687;
                            break;
                        case "emp_937360":
                            emp.Priority = 0.852675577324198;
                            break;
                        case "emp_937363":
                            emp.Priority = 1.41627208977764;
                            break;
                        case "emp_937367":
                            emp.Priority = 1.37054222397066;
                            break;
                        case "emp_937369":
                            emp.Priority = 0.846346713298162;
                            break;
                        case "emp_939965":
                            emp.Priority = 1.41484156386687;
                            break;
                        case "emp_943864":
                            emp.Priority = 0.631863355698;
                            break;
                        case "emp_943878":
                            emp.Priority = 1.00918985507879;
                            break;
                        case "emp_950080":
                            emp.Priority = 0.565792284005225;
                            break;
                        case "emp_951615":
                            emp.Priority = 0.923218497272217;
                            break;
                        case "emp_57207":
                            emp.Priority = 0.553169816757166;
                            break;
                        case "emp_57216":
                            emp.Priority = 0.553832069995735;
                            break;
                        case "emp_57217":
                            emp.Priority = 0.763177480671172;
                            break;
                        case "emp_57218":
                            emp.Priority = 0.70199789069686;
                            break;
                        case "emp_57219":
                            emp.Priority = 1.2562583530118;
                            break;
                        case "emp_57220":
                            emp.Priority = 0.56722432890312;
                            break;
                        case "emp_57221":
                            emp.Priority = 1.21019392260825;
                            break;
                        case "emp_57222":
                            emp.Priority = 0.861693870910301;
                            break;
                        case "emp_57223":
                            emp.Priority = 1.46698796794144;
                            break;
                        case "emp_57224":
                            emp.Priority = 0.531052180580353;
                            break;
                        case "emp_57225":
                            emp.Priority = 1.29881222117637;
                            break;
                        case "emp_57226":
                            emp.Priority = 0.522567352756191;
                            break;
                        case "emp_57227":
                            emp.Priority = 1.19636197606864;
                            break;
                        case "emp_57228":
                            emp.Priority = 0.96632760645185;
                            break;
                        case "emp_57230":
                            emp.Priority = 1.33272022094239;
                            break;
                        case "emp_57237":
                            emp.Priority = 0.808130315648453;
                            break;
                        case "emp_57238":
                            emp.Priority = 1.2527146110091;
                            break;
                        case "emp_57239":
                            emp.Priority = 0.725736554817174;
                            break;
                        case "emp_57240":
                            emp.Priority = 0.748879356891326;
                            break;
                        case "emp_57242":
                            emp.Priority = 0.880983988466199;
                            break;
                        case "emp_57404":
                            emp.Priority = 1.39379381010951;
                            break;
                        case "emp_58615":
                            emp.Priority = 0.677373562090739;
                            break;
                        case "emp_58620":
                            emp.Priority = 1.13145455887143;
                            break;
                        case "emp_58621":
                            emp.Priority = 1.23142062254782;
                            break;
                        case "emp_59441":
                            emp.Priority = 0.824033280519784;
                            break;
                        case "emp_59660":
                            emp.Priority = 1.44479962761737;
                            break;
                        case "emp_60168":
                            emp.Priority = 1.25701557228203;
                            break;
                        case "emp_60772":
                            emp.Priority = 0.711269942210647;
                            break;
                        case "emp_60773":
                            emp.Priority = 1.3736709099606;
                            break;
                        case "emp_60774":
                            emp.Priority = 0.519315182706022;
                            break;
                        case "emp_60776":
                            emp.Priority = 1.38345315255386;
                            break;
                        case "emp_60855":
                            emp.Priority = 0.911167275817677;
                            break;
                        case "emp_60857":
                            emp.Priority = 0.616298120057349;
                            break;
                        case "emp_60859":
                            emp.Priority = 0.937873717601352;
                            break;
                        case "emp_60861":
                            emp.Priority = 1.01160910097491;
                            break;
                        case "emp_60862":
                            emp.Priority = 1.38634993270335;
                            break;
                        case "emp_60941":
                            emp.Priority = 1.21006660103335;
                            break;
                        case "emp_61293":
                            emp.Priority = 0.670590899498477;
                            break;
                        case "emp_61738":
                            emp.Priority = 1.10664684260574;
                            break;
                        case "emp_62399":
                            emp.Priority = 0.952784300992631;
                            break;
                        case "emp_62618":
                            emp.Priority = 1.43678803692422;
                            break;
                        case "emp_63759":
                            emp.Priority = 0.568459240751555;
                            break;
                        case "emp_65127":
                            emp.Priority = 1.42628613669718;
                            break;
                        case "emp_65971":
                            emp.Priority = 1.21829692121516;
                            break;
                        case "emp_67935":
                            emp.Priority = 0.793160182094742;
                            break;
                        case "emp_68442":
                            emp.Priority = 0.664259879460679;
                            break;
                        case "emp_68815":
                            emp.Priority = 1.15631360125556;
                            break;
                        case "emp_73673":
                            emp.Priority = 0.949944483325791;
                            break;
                        case "emp_76386":
                            emp.Priority = 0.537822003028273;
                            break;
                        case "emp_77615":
                            emp.Priority = 0.538216397649709;
                            break;
                        case "emp_81617":
                            emp.Priority = 0.66212695285777;
                            break;
                        case "emp_81849":
                            emp.Priority = 1.40612680088083;
                            break;
                        case "emp_81850":
                            emp.Priority = 0.760310498187463;
                            break;
                        case "emp_82896":
                            emp.Priority = 1.18480829553903;
                            break;
                        case "emp_83613":
                            emp.Priority = 1.02942468716271;
                            break;
                        case "emp_84092":
                            emp.Priority = 1.37579625466643;
                            break;
                        case "emp_84503":
                            emp.Priority = 0.922377511124302;
                            break;
                        case "emp_86531":
                            emp.Priority = 1.03175685812335;
                            break;
                        case "emp_87425":
                            emp.Priority = 1.37796461017708;
                            break;
                        case "emp_87426":
                            emp.Priority = 1.31145872539443;
                            break;
                        case "emp_87428":
                            emp.Priority = 0.810208756621093;
                            break;
                        case "emp_89158":
                            emp.Priority = 0.998923980397603;
                            break;
                        case "emp_90020":
                            emp.Priority = 0.9880229609497;
                            break;
                        case "emp_90066":
                            emp.Priority = 1.44767278523542;
                            break;
                        case "emp_91398":
                            emp.Priority = 0.647599028026498;
                            break;
                        case "emp_91818":
                            emp.Priority = 0.887644945358692;
                            break;
                        case "emp_94831":
                            emp.Priority = 1.40626923269884;
                            break;
                        case "emp_95441":
                            emp.Priority = 0.758488258467283;
                            break;
                        case "emp_97730":
                            emp.Priority = 1.45471850547694;
                            break;
                        case "emp_99137":
                            emp.Priority = 1.44637028823903;
                            break;
                        case "emp_99369":
                            emp.Priority = 1.0980637146151;
                            break;
                        case "emp_99430":
                            emp.Priority = 1.08212371151062;
                            break;
                        case "emp_104364":
                            emp.Priority = 1.11908971221144;
                            break;
                        case "emp_104367":
                            emp.Priority = 1.2960950558987;
                            break;
                        case "emp_138825":
                            emp.Priority = 0.944195951541977;
                            break;
                        case "emp_143769":
                            emp.Priority = 1.32533456935796;
                            break;
                        case "emp_144617":
                            emp.Priority = 0.751087425393559;
                            break;
                        case "emp_144962":
                            emp.Priority = 1.41315763765627;
                            break;
                        case "emp_146017":
                            emp.Priority = 0.938260440453077;
                            break;
                        case "emp_146661":
                            emp.Priority = 0.659773401059105;
                            break;
                        case "emp_148104":
                            emp.Priority = 0.788486026361811;
                            break;
                        case "emp_148212":
                            emp.Priority = 0.807071088956236;
                            break;
                        case "emp_149105":
                            emp.Priority = 0.673447939182375;
                            break;
                        case "emp_149106":
                            emp.Priority = 1.33545451231089;
                            break;
                        case "emp_149107":
                            emp.Priority = 1.35718822984825;
                            break;
                        case "emp_149108":
                            emp.Priority = 1.47732635167303;
                            break;
                        case "emp_149110":
                            emp.Priority = 0.650856777630214;
                            break;
                        case "emp_150670":
                            emp.Priority = 0.931489824518324;
                            break;
                        case "emp_151869":
                            emp.Priority = 1.40844903043865;
                            break;
                        case "emp_151872":
                            emp.Priority = 1.13581284630849;
                            break;
                        case "emp_156135":
                            emp.Priority = 0.96397242157905;
                            break;
                        case "emp_160756":
                            emp.Priority = 0.67830974291;
                            break;
                        case "emp_166466":
                            emp.Priority = 0.792626289321401;
                            break;
                        case "emp_166468":
                            emp.Priority = 1.29518811721131;
                            break;
                        case "emp_171786":
                            emp.Priority = 0.642575544371538;
                            break;
                        case "emp_172588":
                            emp.Priority = 0.93786405652662;
                            break;
                        case "emp_173412":
                            emp.Priority = 1.08043627980185;
                            break;
                        case "emp_173478":
                            emp.Priority = 1.47861335146176;
                            break;
                        case "emp_177455":
                            emp.Priority = 1.07069789318866;
                            break;
                        case "emp_182951":
                            emp.Priority = 1.40551523673605;
                            break;
                        case "emp_194132":
                            emp.Priority = 0.757990646761838;
                            break;
                        case "emp_201418":
                            emp.Priority = 0.89782534278828;
                            break;
                        case "emp_203841":
                            emp.Priority = 0.995225977848855;
                            break;
                        case "emp_203913":
                            emp.Priority = 0.59145171478924;
                            break;
                        case "emp_204694":
                            emp.Priority = 0.94015268303461;
                            break;
                        case "emp_204711":
                            emp.Priority = 1.08000324134715;
                            break;
                        case "emp_204713":
                            emp.Priority = 0.787037088669388;
                            break;
                        case "emp_208145":
                            emp.Priority = 0.964215442288767;
                            break;
                        case "emp_210922":
                            emp.Priority = 0.740612343997048;
                            break;
                        case "emp_214264":
                            emp.Priority = 1.20409011780475;
                            break;
                        case "emp_215338":
                            emp.Priority = 1.12470882927287;
                            break;
                        case "emp_218823":
                            emp.Priority = 1.00921987346803;
                            break;
                        case "emp_218824":
                            emp.Priority = 0.593496417670276;
                            break;
                        case "emp_226218":
                            emp.Priority = 1.21819120911797;
                            break;
                        case "emp_226752":
                            emp.Priority = 1.02297269903262;
                            break;
                        case "emp_233372":
                            emp.Priority = 0.503137667664856;
                            break;
                        case "emp_234894":
                            emp.Priority = 0.825476041215228;
                            break;
                        case "emp_245654":
                            emp.Priority = 1.15256844863881;
                            break;
                        case "emp_245701":
                            emp.Priority = 0.590484555387164;
                            break;
                        case "emp_246485":
                            emp.Priority = 0.67027267635347;
                            break;
                        case "emp_284215":
                            emp.Priority = 0.736788167728478;
                            break;
                        case "emp_292996":
                            emp.Priority = 0.974779408180518;
                            break;
                        case "emp_297446":
                            emp.Priority = 0.850039228028636;
                            break;
                        case "emp_297448":
                            emp.Priority = 0.818905659168449;
                            break;
                        case "emp_299934":
                            emp.Priority = 0.982397866659983;
                            break;
                        case "emp_313868":
                            emp.Priority = 0.919753971705099;
                            break;
                        case "emp_316601":
                            emp.Priority = 0.789497422189218;
                            break;
                        case "emp_316604":
                            emp.Priority = 1.32390159500013;
                            break;
                        case "emp_316605":
                            emp.Priority = 1.15351951152716;
                            break;
                        case "emp_318904":
                            emp.Priority = 0.506331895015357;
                            break;
                        case "emp_319941":
                            emp.Priority = 0.744898289556102;
                            break;
                        case "emp_323320":
                            emp.Priority = 0.772474073931796;
                            break;
                        case "emp_325857":
                            emp.Priority = 0.842459744467614;
                            break;
                        case "emp_328371":
                            emp.Priority = 1.03274520371703;
                            break;
                        case "emp_332693":
                            emp.Priority = 1.40178275429727;
                            break;
                        case "emp_338998":
                            emp.Priority = 1.39066068357353;
                            break;
                        case "emp_344450":
                            emp.Priority = 1.31184511110738;
                            break;
                        case "emp_344459":
                            emp.Priority = 0.581996224393135;
                            break;
                        case "emp_346739":
                            emp.Priority = 0.895301829276281;
                            break;
                        case "emp_352000":
                            emp.Priority = 0.777184988501102;
                            break;
                        case "emp_362792":
                            emp.Priority = 1.19028926300364;
                            break;
                        case "emp_371741":
                            emp.Priority = 1.18664133534145;
                            break;
                        case "emp_373619":
                            emp.Priority = 0.690877480521275;
                            break;
                        case "emp_378314":
                            emp.Priority = 0.704358912633899;
                            break;
                        case "emp_378317":
                            emp.Priority = 0.511104017035618;
                            break;
                        case "emp_391054":
                            emp.Priority = 1.45475254811102;
                            break;
                        case "emp_457072":
                            emp.Priority = 0.584813325239724;
                            break;
                        case "emp_485992":
                            emp.Priority = 1.07443508020343;
                            break;
                        case "emp_485995":
                            emp.Priority = 0.772215418178688;
                            break;
                        case "emp_496129":
                            emp.Priority = 0.639437876706681;
                            break;
                        case "emp_507329":
                            emp.Priority = 0.612388015311392;
                            break;
                        case "emp_507336":
                            emp.Priority = 1.42786783116305;
                            break;
                        case "emp_527298":
                            emp.Priority = 1.3881287960746;
                            break;
                        case "emp_527299":
                            emp.Priority = 0.90042521683519;
                            break;
                        case "emp_540222":
                            emp.Priority = 1.16872706900757;
                            break;
                        case "emp_547796":
                            emp.Priority = 1.28321123858132;
                            break;
                        case "emp_547798":
                            emp.Priority = 0.547786114759644;
                            break;
                        case "emp_551444":
                            emp.Priority = 0.676320318680406;
                            break;
                        case "emp_568760":
                            emp.Priority = 1.10905384812926;
                            break;
                        case "emp_569804":
                            emp.Priority = 0.520398711329512;
                            break;
                        case "emp_577412":
                            emp.Priority = 0.790505819157933;
                            break;
                        case "emp_577415":
                            emp.Priority = 0.963135493669256;
                            break;
                        case "emp_581342":
                            emp.Priority = 1.31069593076161;
                            break;
                        case "emp_594031":
                            emp.Priority = 0.734280448981691;
                            break;
                        case "emp_604007":
                            emp.Priority = 0.959191828248646;
                            break;
                        case "emp_604015":
                            emp.Priority = 0.852234755341073;
                            break;
                        case "emp_627074":
                            emp.Priority = 0.666760129000414;
                            break;
                        case "emp_627091":
                            emp.Priority = 1.06075121395325;
                            break;
                        case "emp_629449":
                            emp.Priority = 1.3164084548207;
                            break;
                        case "emp_646914":
                            emp.Priority = 1.13231201545909;
                            break;
                        case "emp_653280":
                            emp.Priority = 0.691292556557475;
                            break;
                        case "emp_706121":
                            emp.Priority = 0.743479816822093;
                            break;
                        case "emp_711967":
                            emp.Priority = 0.757266619362527;
                            break;
                        case "emp_749578":
                            emp.Priority = 1.31329956688606;
                            break;
                        case "emp_769593":
                            emp.Priority = 0.979983413349829;
                            break;
                        case "emp_770623":
                            emp.Priority = 1.05014683238703;
                            break;
                    }
                }
            }
            else if (portalName.Equals("andersons"))
            {
                //double priority = 1;
                foreach (EmployeeObject emp in employeeObjects)
                {
                    //priority = 0.5 + rand.NextDouble();
                    //emp.Priority = priority;
                    //Console.WriteLine("case \"" + emp.EmployeeId + "\":");
                    //Console.WriteLine("\temp.Priority = " + priority + ";");
                    //Console.WriteLine("\tbreak;");


                    switch (emp.EmployeeId)
                    {
                        case "emp_261525":
                            emp.Priority = 0.736776212806244;
                            break;
                        case "emp_261526":
                            emp.Priority = 1.45540441104928;
                            break;
                        case "emp_261529":
                            emp.Priority = 1.18747215889742;
                            break;
                        case "emp_261535":
                            emp.Priority = 1.31458774852314;
                            break;
                        case "emp_261539":
                            emp.Priority = 0.847220894110958;
                            break;
                        case "emp_261540":
                            emp.Priority = 0.857817419971254;
                            break;
                        case "emp_261542":
                            emp.Priority = 1.30363171072846;
                            break;
                        case "emp_261550":
                            emp.Priority = 0.758954628491287;
                            break;
                        case "emp_261551":
                            emp.Priority = 0.677041423123815;
                            break;
                        case "emp_261554":
                            emp.Priority = 1.04687641167402;
                            break;
                        case "emp_261560":
                            emp.Priority = 1.15290842421954;
                            break;
                        case "emp_261567":
                            emp.Priority = 1.02799041547253;
                            break;
                        case "emp_261568":
                            emp.Priority = 0.915642012569887;
                            break;
                        case "emp_261571":
                            emp.Priority = 1.17345275295593;
                            break;
                        case "emp_261576":
                            emp.Priority = 1.29466154277076;
                            break;
                        case "emp_261577":
                            emp.Priority = 0.530948421932267;
                            break;
                        case "emp_261578":
                            emp.Priority = 0.949918568809479;
                            break;
                        case "emp_261582":
                            emp.Priority = 0.851024639024876;
                            break;
                        case "emp_261584":
                            emp.Priority = 1.37779025262119;
                            break;
                        case "emp_261587":
                            emp.Priority = 1.32230876051928;
                            break;
                        case "emp_261589":
                            emp.Priority = 0.98994908737482;
                            break;
                        case "emp_261596":
                            emp.Priority = 0.679329698988856;
                            break;
                        case "emp_261601":
                            emp.Priority = 0.980325771253708;
                            break;
                        case "emp_261613":
                            emp.Priority = 0.781777053736978;
                            break;
                        case "emp_261614":
                            emp.Priority = 1.42441765448284;
                            break;
                        case "emp_261617":
                            emp.Priority = 0.70210940493369;
                            break;
                        case "emp_261620":
                            emp.Priority = 0.790618843534318;
                            break;
                        case "emp_261621":
                            emp.Priority = 1.4236567318084;
                            break;
                        case "emp_261622":
                            emp.Priority = 1.37545517872807;
                            break;
                        case "emp_261627":
                            emp.Priority = 0.743530899399673;
                            break;
                        case "emp_261646":
                            emp.Priority = 1.43308746206252;
                            break;
                        case "emp_261648":
                            emp.Priority = 0.51638401673007;
                            break;
                        case "emp_261650":
                            emp.Priority = 1.47487693511642;
                            break;
                        case "emp_261652":
                            emp.Priority = 1.30997409150469;
                            break;
                        case "emp_261662":
                            emp.Priority = 1.11077589709814;
                            break;
                        case "emp_261669":
                            emp.Priority = 1.10330797247743;
                            break;
                        case "emp_261671":
                            emp.Priority = 0.645162565701251;
                            break;
                        case "emp_261675":
                            emp.Priority = 1.21706486154211;
                            break;
                        case "emp_261679":
                            emp.Priority = 0.851454839274033;
                            break;
                        case "emp_261682":
                            emp.Priority = 1.48421274730201;
                            break;
                        case "emp_261685":
                            emp.Priority = 1.42855099631872;
                            break;
                        case "emp_261686":
                            emp.Priority = 0.816955478543861;
                            break;
                        case "emp_261687":
                            emp.Priority = 0.562931190274158;
                            break;
                        case "emp_261689":
                            emp.Priority = 0.921581824506438;
                            break;
                        case "emp_261693":
                            emp.Priority = 1.48543508955531;
                            break;
                        case "emp_261696":
                            emp.Priority = 0.779090428854847;
                            break;
                        case "emp_261697":
                            emp.Priority = 1.33435890722757;
                            break;
                        case "emp_261700":
                            emp.Priority = 0.547347996871615;
                            break;
                        case "emp_261701":
                            emp.Priority = 1.15285128292295;
                            break;
                        case "emp_261702":
                            emp.Priority = 0.797959917363692;
                            break;
                        case "emp_261703":
                            emp.Priority = 1.09277049060574;
                            break;
                        case "emp_261711":
                            emp.Priority = 0.744628412762949;
                            break;
                        case "emp_261712":
                            emp.Priority = 1.205517226693;
                            break;
                        case "emp_261713":
                            emp.Priority = 0.593082355844361;
                            break;
                        case "emp_261714":
                            emp.Priority = 0.528619593954002;
                            break;
                        case "emp_261716":
                            emp.Priority = 0.557446513817388;
                            break;
                        case "emp_261720":
                            emp.Priority = 0.975078639795575;
                            break;
                        case "emp_261725":
                            emp.Priority = 0.905695105160444;
                            break;
                        case "emp_261726":
                            emp.Priority = 1.3901700940403;
                            break;
                        case "emp_261750":
                            emp.Priority = 0.645111489177268;
                            break;
                        case "emp_261756":
                            emp.Priority = 0.567198576436936;
                            break;
                        case "emp_261775":
                            emp.Priority = 1.37997497892006;
                            break;
                        case "emp_261777":
                            emp.Priority = 0.883499449763214;
                            break;
                        case "emp_261782":
                            emp.Priority = 1.43351052372414;
                            break;
                        case "emp_261785":
                            emp.Priority = 1.1137889496115;
                            break;
                        case "emp_261791":
                            emp.Priority = 1.13652440748947;
                            break;
                        case "emp_297089":
                            emp.Priority = 1.05311348035611;
                            break;
                        case "emp_297092":
                            emp.Priority = 1.1056679210652;
                            break;
                        case "emp_301461":
                            emp.Priority = 0.562676855857799;
                            break;
                        case "emp_308768":
                            emp.Priority = 0.691353570293334;
                            break;
                        case "emp_308772":
                            emp.Priority = 1.38578585623102;
                            break;
                        case "emp_308773":
                            emp.Priority = 1.23285370726737;
                            break;
                        case "emp_344015":
                            emp.Priority = 1.49956979975084;
                            break;
                        case "emp_360479":
                            emp.Priority = 1.39357750531918;
                            break;
                        case "emp_388134":
                            emp.Priority = 1.39375776420057;
                            break;
                        case "emp_388135":
                            emp.Priority = 0.672993608830959;
                            break;
                        case "emp_388136":
                            emp.Priority = 0.616398508714698;
                            break;
                        case "emp_388138":
                            emp.Priority = 0.558743946747269;
                            break;
                        case "emp_388144":
                            emp.Priority = 0.796341964181672;
                            break;
                        case "emp_391775":
                            emp.Priority = 1.145327225628;
                            break;
                        case "emp_397571":
                            emp.Priority = 0.867750497706118;
                            break;
                        case "emp_397956":
                            emp.Priority = 0.743270846662703;
                            break;
                        case "emp_397960":
                            emp.Priority = 0.770805448885451;
                            break;
                        case "emp_430500":
                            emp.Priority = 1.07749526136438;
                            break;
                        case "emp_430502":
                            emp.Priority = 1.15076040879393;
                            break;
                        case "emp_430503":
                            emp.Priority = 1.18845904929957;
                            break;
                        case "emp_430504":
                            emp.Priority = 0.810866790037075;
                            break;
                        case "emp_430505":
                            emp.Priority = 1.38179457927206;
                            break;
                        case "emp_434286":
                            emp.Priority = 1.28135449755069;
                            break;
                        case "emp_436261":
                            emp.Priority = 1.05332938328075;
                            break;
                        case "emp_436291":
                            emp.Priority = 0.628229332681852;
                            break;
                        case "emp_443132":
                            emp.Priority = 1.23946746054081;
                            break;
                        case "emp_473059":
                            emp.Priority = 1.32689476750181;
                            break;
                        case "emp_473062":
                            emp.Priority = 0.706343350096766;
                            break;
                        case "emp_473065":
                            emp.Priority = 1.41701417086507;
                            break;
                        case "emp_485845":
                            emp.Priority = 0.548576017398655;
                            break;
                        case "emp_491906":
                            emp.Priority = 1.43345602878065;
                            break;
                        case "emp_533823":
                            emp.Priority = 0.629420666550016;
                            break;
                        case "emp_533824":
                            emp.Priority = 1.30779287489494;
                            break;
                        case "emp_533890":
                            emp.Priority = 0.848910682065837;
                            break;
                        case "emp_533891":
                            emp.Priority = 1.22597694849874;
                            break;
                        case "emp_533892":
                            emp.Priority = 0.728690986162373;
                            break;
                        case "emp_541958":
                            emp.Priority = 1.48467114101382;
                            break;
                        case "emp_559349":
                            emp.Priority = 0.961497712629613;
                            break;
                        case "emp_559352":
                            emp.Priority = 0.912174061132676;
                            break;
                        case "emp_559353":
                            emp.Priority = 1.35991678333837;
                            break;
                        case "emp_559354":
                            emp.Priority = 0.745058613012106;
                            break;
                        case "emp_559355":
                            emp.Priority = 1.31193972137381;
                            break;
                        case "emp_559356":
                            emp.Priority = 0.699324591643794;
                            break;
                        case "emp_559357":
                            emp.Priority = 1.35562598512304;
                            break;
                        case "emp_566945":
                            emp.Priority = 1.44104800510269;
                            break;
                        case "emp_639674":
                            emp.Priority = 0.916334693048305;
                            break;
                        case "emp_639685":
                            emp.Priority = 0.609353140978773;
                            break;
                        case "emp_639816":
                            emp.Priority = 0.744842868412306;
                            break;
                        case "emp_743157":
                            emp.Priority = 1.27736099147115;
                            break;
                        case "emp_750780":
                            emp.Priority = 1.32392772977423;
                            break;
                        case "emp_750782":
                            emp.Priority = 1.10916953003461;
                            break;
                        case "emp_750784":
                            emp.Priority = 1.30600418839883;
                            break;
                        case "emp_750785":
                            emp.Priority = 0.782750114930212;
                            break;
                        case "emp_750787":
                            emp.Priority = 1.42532990031193;
                            break;
                        case "emp_769107":
                            emp.Priority = 0.825657617452395;
                            break;
                        case "emp_844941":
                            emp.Priority = 1.17131890108405;
                            break;
                        case "emp_844996":
                            emp.Priority = 1.32431342351451;
                            break;
                        case "emp_844997":
                            emp.Priority = 1.00934747257705;
                            break;
                        case "emp_846701":
                            emp.Priority = 0.563124237611482;
                            break;
                        case "emp_846702":
                            emp.Priority = 0.646318395690209;
                            break;
                        case "emp_846703":
                            emp.Priority = 1.40595893976556;
                            break;
                        case "emp_846704":
                            emp.Priority = 1.29322644965408;
                            break;
                        case "emp_847236":
                            emp.Priority = 1.47656333445411;
                            break;
                        case "emp_847237":
                            emp.Priority = 1.34518174680191;
                            break;
                    }

                }
            }





            else
            {
                foreach (EmployeeObject emp in employeeObjects)
                {
                    if (Math.Abs(emp.Priority) < 0.01)
                    {
                        emp.Priority = 1;
                    }
                }
            }
        }

        //Method for taking a dataTable and make it into a "stringbuild" file used for CSV export 
        internal StringBuilder CSVexportPreparation(DataTable dataTable)
        {
            Console.WriteLine("Starting export.");
            IEnumerable<string> columnNames = dataTable.Columns.Cast<DataColumn>().
                                  Select(column => column.ColumnName);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SEP=,");

            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dataTable.Rows)
            {
                string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                ToArray();
                sb.AppendLine(string.Join(",", fields));
            }
            return sb;
        }
        //Method to create database connection string based on staging name
        internal string GenerateConnectionString(string stagingName)
        {
            String[] initialCatalog = {"PlandayUTC", "PlandayUTC-Marketing","PlandayUTC-Templates",
                "PlandayUTC-Batch-0001", "PlandayUTC-FW", "PlandayUTC-Batch-0002", "PlandayUTC-Batch-0003",
                "PlandayUTC-Batch-0004", "PlandayUTC-Batch-0005", "PlandayUTC-Batch-0007", "PlandayUTC-Batch-0008",
                "PlandayUTC-Batch-0009","PlandayUTC-Batch-0010", "PlandayUTC-Batch-0011", "PlandayUTC-Batch-0012", "PlandayUTC-Batch-0025", "PlandayUTC-Batch-0029"};
            int catalogSelector = 99;
            string initialCatalogString = "";
            bool useCatString = false;
            // Pt er det kun portaler der er nævnt på Confluence under Environment access og staging
            //Der er flere i arket "Database connection overview"
            //ConnectionStringId = 6
            if (stagingName.Equals("alma") || stagingName.Equals("oak"))
            {
                catalogSelector = 5;
            }
            //ConnectionStringId = 7
            else if (stagingName.Equals("byensbilpleje") || stagingName.Equals("courtyardbrusselseu")
                      )
            {
                catalogSelector = 6;
                //ConnectionStringId = 8
            }
            else if (stagingName.Equals("bit") || stagingName.Equals("ostehuset") || stagingName.Equals("bof"))
            {
                catalogSelector = 7;
            }
            else if (stagingName.Equals("falck-globalassistance") || stagingName.Equals("marienlyst") || stagingName.Equals("admiralhotel") || stagingName.Equals("viktoriagade") || stagingName.Equals("koed") || stagingName.Equals("olivia") || stagingName.Equals("42raw") || stagingName.Equals("helenekilde") || stagingName.Equals("moxyoslox"))
            {
                catalogSelector = 16;
            }
            else if (stagingName.Equals("andersons"))
            {
                initialCatalogString = "Planday-16739";
                useCatString = true;
            }
            //else if (stagingName.Equals("falck-globalassistance"))
            //{
            //    Console.WriteLine(stagingName + "does currently not work, exitting...");
            //    Environment.Exit(0);
            //}
            else
            {
                Console.WriteLine("Wrong staging name \"" + stagingName + "\" entered, exitting...");
                Environment.Exit(0);
            }

            //string connectionString = "data source=db01.planday.lan;" +
            //   "initial catalog=" +
            //   initialCatalog[catalogSelector] + ";" +
            //   "persist security info=True;" +
            //"user id=services;" +
            //"password=JGUHh874Nfkqy6UQGX8dbP9DnLvYwzCy;" +
            //"Max Pool Size=1000;";
            string connectionString = "";
            if (useCatString)
            {
                connectionString = "Server=tcp:testenv-westeurope.database.windows.net,1433;Initial Catalog=" + initialCatalogString + ";Persist Security Info=False;User ID=planday-webportal;Password=JGUHh874Nfkqy6UQGX8dbP9DnLvYwzCy;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            }
            else
            {
                connectionString = "Server=tcp:testenv-westeurope.database.windows.net,1433;Initial Catalog=" + initialCatalog[catalogSelector] + ";Persist Security Info=False;User ID=planday-webportal;Password=JGUHh874Nfkqy6UQGX8dbP9DnLvYwzCy;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            }
            // string connectionString = "Server=tcp:testenv-westeurope.database.windows.net,1433;Initial Catalog="+initialCatalog[catalogSelector]+";Persist Security Info=False;User ID=planday-webportal;Password=JGUHh874Nfkqy6UQGX8dbP9DnLvYwzCy;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            //string connectionString = "Server=tcp:testenv-westeurope.database.windows.net,1433;" +
            //   "Initial Catalog = " +
            //   "PlandayMaster; " +
            ////   initialCatalog[catalogSelector] + ";" +
            //"Persist Security Info = False; " +
            //"User ID = services; " +
            //"Password = JGUHh874Nfkqy6UQGX8dbP9DnLvYwzCy; " +
            //"MultipleActiveResultSets = False; " +
            //"Encrypt = True; " +
            //"TrustServerCertificate = False; " +
            //"Connection Timeout = 30;";


            return connectionString;
        }

        //Method to find the portalID based on URL
        internal int SQLFindPortalID(string stagingName, SqlConnection conn)
        {
            SqlCommand command = new SqlCommand("select Id from Portal where URL like '%" + stagingName + "%'", conn);
            int portalId = 0;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (stagingName.Equals("olivia"))
                    {
                        portalId = 11329;
                        break;
                    }
                    if (stagingName.Equals("koed"))
                    {
                        portalId = 11399;
                        break;

                    }
                    if (stagingName.Equals("viktoriagade"))
                    {
                        portalId = 34965;
                        break;
                    }
                    portalId = Convert.ToInt32(reader[0].ToString());
                }
            }
            return portalId;
        }

        //Method to create dataTable for easier export
        internal DataTable CreateDataTable(SqlCommand command)
        {
            var dataTable = new DataTable();
            using (var da = new SqlDataAdapter(command))
            {
                da.Fill(dataTable);
            }
            return dataTable;

        }
        internal string FixTrueEnd(string endTimeObj)
        {
            string time = "";

            DateTime fixedTime = DateTime.Parse(endTimeObj);
            time = fixedTime.Day.ToString().PadLeft(2, '0');

            return time;
        }
        internal string FixHourLayout(string timeVal)
        {
            string time = "";

            DateTime fixedTime = DateTime.Parse(timeVal);
            DateTime userTime = TimeZoneInfo.ConvertTimeFromUtc(fixedTime, TimeZoneInfo.Local);
            time = userTime.Hour.ToString().PadLeft(2, '0');

            return time;
        }



        internal string GetDateTimeHourMin(string v)
        {
            string time = "";

            if (!v.Equals(""))
            {
                string hour = FixHourLayout(v);
                string min = FixMinuteLayout(v);
                time = hour + "." + min;
            }

            return time;
        }
        internal string FixMonthLayout(string startTimeObj)
        {
            string time = "";

            DateTime fixedTime = DateTime.Parse(startTimeObj);
            time = fixedTime.Month.ToString().PadLeft(2, '0');

            return time;
        }

        internal string FixYearLayout(string startTimeObj)
        {
            string time = "";

            DateTime fixedTime = DateTime.Parse(startTimeObj);
            time = fixedTime.Year.ToString();

            return time;
        }

        internal string FixMinuteLayout(string timeVal)
        {
            string time = "";

            DateTime fixedTime = DateTime.Parse(timeVal);
            time = fixedTime.Minute.ToString().PadLeft(2, '0');

            return time;
        }
        internal string FindFirstMondayInRange(string startOfPeriod)
        {
            string start = "";
            DateTime fixedTime = DateTime.Parse(startOfPeriod);
            string dayOfWeek = fixedTime.DayOfWeek.ToString();

            if (!dayOfWeek.Equals("Monday"))
            {
                while (!fixedTime.DayOfWeek.ToString().Equals("Monday"))
                {
                    fixedTime = fixedTime.AddDays(-1);
                }
            }

            start = fixedTime.Year.ToString() + "-" + fixedTime.Month.ToString() + "-" + fixedTime.Day.ToString();
            return start;
        }
        internal Tuple<ArrayList, ArrayList, ArrayList> GetAllDepartmentsOfPortalFromDataBase(int portalID, SqlConnection conn)
        {
            SqlCommand command = new SqlCommand("select Id, Name, FallbackHourlySalary from [Hrm.Department] where PortalId like " + portalID + " and IsDeleted like 0", conn);
            ArrayList departmentList = new ArrayList();
            ArrayList departmentNameList = new ArrayList();
            ArrayList departmentFallBackSalaryList = new ArrayList();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    departmentList.Add(reader.GetValue(0).ToString());
                    departmentNameList.Add(reader.GetValue(1).ToString());
                    if (reader.GetValue(2).ToString().Equals(null))
                    {
                        departmentFallBackSalaryList.Add("0.0");
                    }
                    else
                    {
                        departmentFallBackSalaryList.Add(reader.GetValue(2).ToString());
                    }
                }
            }
            return Tuple.Create(departmentList, departmentNameList, departmentFallBackSalaryList);
        }

        internal string FixDayLayout(string timeValue)
        {
            string time = "";

            DateTime period = DateTime.Parse(timeValue);
            time = "d_" + period.Year + "_" + FixTimeString(period.Month) + "_" + FixTimeString(period.Day);

            return time;
        }

        //Method to format minutes
        //internal string FormatMinutes(string v)
        //{
        //    string newMinutes = "";
        //    string newVal = "";
        //    if(!v.Equals("00")){
        //        decimal dec = Convert.ToDecimal(TimeSpan.Parse("00:"+v).TotalHours);
        //        //string val = (Double.Parse(v) / 60.0 * 10000.0).ToString();
        //        newVal = dec.ToString();
        //        if(newVal.Length > 6){
        //            newVal = dec.ToString().Substring(2, 4);      
        //        }else if(newVal.Length > 4){
        //            newVal = dec.ToString().Substring(2, 2);    
        //        }else{
        //            newVal = dec.ToString().Substring(2, 1);    
        //        }

        //     //   newVal = dec.ToString();
        //    //    Console.WriteLine("done");
        //    }else{
        //        newVal = "00";
        //    }

        //    newMinutes = newVal;

        //    return newMinutes;
        //}
        //internal DateTime FixMonthLook(string[] v)
        //{
        //    DateTime firstDayDate = DateTime.Parse(v[0] + "/" + v[1] + "/" + v[2]);

        //    return firstDayDate;
        //}
        //Method to seperate datetime strings//Unused
        //internal string[] StringSeperator(string dateTime)
        //{
        //    string[] seperators = { "/", ":", " ", "-" };
        //    string[] dateSplit = dateTime.Split(seperators, StringSplitOptions.RemoveEmptyEntries);

        //    return dateSplit;
        //}

        //Method to calculate the duration of a shift
        internal string CalculateDuration(TimeSheetObject curTimeSheet)
        {
            string duration = "";

            //string startTime = curTimeSheet.StartYear + "-" + curTimeSheet.StartMonth + "-" + curTimeSheet.StartDayTrue + " " + curTimeSheet.StartHour + ":" + curTimeSheet.StartMinute;
            //string endTime = curTimeSheet.EndYear + "-" + curTimeSheet.EndMonth + "-" + curTimeSheet.EndDayTrue + " " + curTimeSheet.EndHour + ":" + curTimeSheet.EndMinute;
            //string format = "yyyy-MM-dd HH:mm";
            DateTime startDate = curTimeSheet.TrueStartTime;// DateTime.ParseExact(startTime,format , null);
            DateTime endDate = curTimeSheet.TrueEndTime;// DateTime.ParseExact(endTime, format, null);

            TimeSpan durationVal = endDate.Subtract(startDate);

            if (durationVal.TotalHours < 0)
            {
                DateTime endTimeDateTime = endDate.AddDays(1);
                durationVal = endTimeDateTime.Subtract(startDate);
            }

            duration = durationVal.Hours + "." + FormatBreakMinutes(durationVal.Minutes.ToString());

            return duration;
        }


        //Method to calculate if a shift overlaps two days 
        internal int CalculateOverlap(TimeSheetObject sheet)
        {
            int overLap = 0;

            DateTime startTime = sheet.TrueStartTime;
            DateTime endTime = sheet.TrueEndTime;

            if (startTime.Date < endTime.Date)
            {
                overLap = 1;
            }
            else
            {
                overLap = 0;
            }
            return overLap;
        }

        //Method to seperate month
        //internal string[] StringSeperatorTwo(string dateTime)
        //{
        //    string[] seperators = { "d_", "_"};
        //    string[] dateSplit = dateTime.Split(seperators, StringSplitOptions.RemoveEmptyEntries);

        //    return dateSplit;
        //}

        internal string FormatBreakMinutes(string v)
        {
            string newMinutes = "";
            string newVal = "";
            double timeV = Int32.Parse(v);

            if (timeV < 60 && timeV > 0)
            {

                decimal dec = Convert.ToDecimal(TimeSpan.Parse("00:" + v).TotalHours);

                //string val = (Double.Parse(v) / 60.0 * 10000.0).ToString();
                newVal = dec.ToString();
                if (newVal.Length > 6)
                {
                    newVal = dec.ToString().Substring(2, 4);
                }
                else if (newVal.Length > 4)
                {
                    newVal = dec.ToString().Substring(2, 2);
                }
                else
                {
                    newVal = dec.ToString().Substring(2, 1);
                }

                //   newVal = dec.ToString();
                //    Console.WriteLine("done");
                newMinutes = newVal;

            }
            else if (timeV >= 60)
            {
                int nrHours = (int)timeV / 60;

                int minVal = (int)timeV % 60;
                decimal dec = Convert.ToDecimal(TimeSpan.Parse(nrHours + ":" + minVal).TotalHours);
                newVal = dec.ToString();
                //   Console.WriteLine("done"); 
            }
            else
            {
                newMinutes = "00";
            }

            return newMinutes;
        }

        //Getting month val as an int
        //internal int FixStartMonth(string monthVal)
        //{
        //    string[] month = StringSeperatorTwo(monthVal);
        //        //monthVal.Substring(2, 2);

        //    int newMonth = Int32.Parse(month[1]);
        //    return newMonth;
        //}

        //Getting year val as and int
        //internal int FixStartYear(string yearVal)
        //{
        //    string year = yearVal.Substring(yearVal.Length - 4);
        //    int newYear = Int32.Parse(year);
        //    return newYear;
        //}

        //Getting the day value as an int
        //internal int FixStartDayVal(string dayVal)
        //{
        //    string[] day = StringSeperatorTwo(dayVal);
        //    int newDay = Int32.Parse(day[2]);
        //    return newDay;
        //}

        //Prepares the emp id to be returned to the db
        internal string FixEmpId(string empId)
        {
            //Console.WriteLine(empId);
            string newId = "";
            if (empId.Length <= 4)
            {
                newId = "";
            }
            else
            {
                newId = empId.Substring(4);
            }

            return newId;
        }

        internal string CalcEndRange(string startRanges, string nrWeeks)
        {
            string endRangeRe = "";
            DateTime endRange;


            DateTime startRange = Convert.ToDateTime(startRanges);
            int weeksToDays = Int32.Parse(nrWeeks) * 7;
            endRange = startRange.AddDays(weeksToDays - 1);
            endRangeRe = endRange.Year.ToString() + "-" + FixTimeString(endRange.Month) + "-" + FixTimeString(endRange.Day);
            return endRangeRe;

        }

        //Prepares the sheet id to be returned to the db
        internal string FixSheetId(string empId)
        {
            string newId = "";
            if (empId.Length <= 3)
            {
                newId = "";
            }
            else
            {
                newId = empId.Substring(3);
            }

            return newId;
        }

        //internal string RandomNormHour()
        //{
        //    string normHour = "";
        //    Random r = new Random();
        //    int val = r.Next(5,20);
        //    normHour = val.ToString();

        //    return normHour;
        //}

        internal string FixTimeString(int month)
        {
            string time = "";
            if (month.ToString().Length < 2)
            {
                time = "0" + month.ToString();
            }
            else
            {
                time = month.ToString();
            }
            return time;

        }

        internal double FindShiftDuration(DateTime TrueStartTime, DateTime TrueEndTime)
        {
            double duration = 0;

            //string startTime =  StartYear + "-" +  StartMonth + "-" +  StartDayTrue + " " +  StartHour + ":" +  StartMinute;
            //string endTime =  EndYear + "-" +  EndMonth + "-" +  EndDayTrue + " " +  EndHour + ":" +  EndMinute;
            //string format = "yyyy-MM-dd HH:mm";
            DateTime startDate = TrueStartTime;
            DateTime endDate = TrueEndTime;

            //string startTime = startHour + "." + startMinute;
            //string endTime = endHour + "." + endMinute;

            TimeSpan durationVal = endDate.Subtract(startDate);
            duration = durationVal.TotalHours;

            if (duration < 0)
            {
                double afterTwelve = endDate.TimeOfDay.TotalHours;
                double beforeTwelve = 24 - startDate.TimeOfDay.TotalHours;
                duration = beforeTwelve + afterTwelve;
            }
            // Console.WriteLine(duration);

            return duration;
        }

        internal List<DateTime> CloneDaySetList(List<DateTime> dayset)
        {
            List<DateTime> newList = new List<DateTime>();
            foreach (DateTime date in dayset)
            {
                // newList.Add(CloneEmp(emp));
                newList.Add(CloneDatesWithJSON(date));
            }
            return newList;
        }

        internal DateTime CloneDatesWithJSON(DateTime date)
        {
            string output = JsonConvert.SerializeObject(date);

            DateTime newSheet = JsonConvert.DeserializeObject<DateTime>(output);

            return newSheet;
        }
    }
}
