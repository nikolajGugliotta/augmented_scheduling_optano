﻿
using AugmentedSchedulingOptano.Library.Models;
using AugmentedSchedulingOptano.Library.OldTools;
using AugmentedSchedulingOptano.Library.OptanoSetObjects;
using OPTANO.Modeling.Optimization;
using System;
using System.Collections.Generic;
using System.Text;

namespace AugmentedSchedulingOptano.Library.ModelTools
{
    class Tools
    {
        ToolsOld oldTool = new ToolsOld();
        //Method to find break duration
        private Tuple<string, double, bool> FindBreakDuration(TimeSheetObject curTimeSheet, BreakObject curBreak)
        {
            Tuple<string, double, bool> breakDuration;
            string overlap = "";
            double duration = 0.0;
            bool used = false;

            string curDay = curTimeSheet.StartDay;

            if (curBreak.DaysActive.Contains(curDay))
            {
                if (curDay.Equals(curTimeSheet.EndDay))
                {
                    overlap = "before_mid";
                    duration = double.Parse(oldTool.FormatBreakMinutes(curBreak.PauseLength.ToString()));
                    used = true;
                }
                else if (curTimeSheet.EndHour.Equals("00"))
                {
                    overlap = "before_mid";
                    duration = double.Parse(oldTool.FormatBreakMinutes(curBreak.PauseLength.ToString()));
                    used = true;
                }
                else
                {   //in minutes
                    int reqShiftLength = curBreak.DutyLength;

                    //TODO OBS HER ER DET FORKERT!
                    double timeBeforeMid = 60 * Double.Parse(oldTool.CalculateDuration(curTimeSheet));
                    double timeAfterMid = 60 * Double.Parse(oldTool.CalculateDuration(curTimeSheet));

                    if (timeBeforeMid >= reqShiftLength)
                    {
                        overlap = "before_mid";
                    }
                    else
                    {
                        overlap = "after_mid";
                    }

                    duration = double.Parse(oldTool.FormatBreakMinutes(curBreak.PauseLength.ToString()));
                    used = true;
                }
            }

            breakDuration = new Tuple<string, double, bool>(overlap, duration, used);

            return breakDuration;
        }
        private string CreateParaBreakLength(DataObject theDataObjects, List<TimeSheetObject> timeSheetObj)
        {
            string curString = "Parameters break_duration(j, t, b_p) 'The total break time of position j on day t in break position b_p' / /;\n\n";
            TimeSheetObject curTimeSheet;

            List<BreakObject> theBreaks = theDataObjects.BreakObjects;

            for (int i = 0; i < timeSheetObj.Count; i++)
            {
                curTimeSheet = timeSheetObj[i];

                for (int j = 0; j < theBreaks.Count; j++)
                {
                    Tuple<string, double, bool> breakDuration = FindBreakDuration(curTimeSheet, theBreaks[j]);

                    if (breakDuration.Item3)
                    {
                        string pos = curTimeSheet.PositionId;
                        //Test wheter the break applies to the current position/employee group.
                        //if (dicEmpGroupInPosition.TryGetValue(pos, out string empGroup))
                        //{
                        //    if (theBreaks[j].EmployeeGroups.Contains(empGroup))
                        //    {
                        //        //curString += "break_duration('" + pos + "','" + curTimeSheet.StartDay + 
                        //        //"','" + breakDuration.Item1 + "') = " + breakDuration.Item2 + ";\n";

                        //    }

                        //}

                    }

                }
            }

            return curString;
        }


        //Method to create the weekend set
        public void CreateWeekendSet(SetObject setObj, List<WeekendsSet> weekendSets)
        {
            //   string createdSet = "";
            //   createdSet += "W_E 'Set of weekends' /";
            string setInput = "weekend_";
            int weekendCount = 1;

            TimeSpan days = setObj.LastDayInRange.AddDays(1).Subtract(setObj.FirstDayInRange);

            int targetI = days.Days / 7;

            for (int i = 0; i < targetI; i++)
            {
                if (i == targetI - 1)
                {
                    if (targetI > 1)
                    {
                        // createdSet += "," + setInput + weekendCount + "/";
                        weekendSets.Add(new WeekendsSet { weekendId = setInput + weekendCount, idVal = weekendCount });
                    }
                    else
                    {
                        //  createdSet += "" + setInput + weekendCount + "/";
                        weekendSets.Add(new WeekendsSet { weekendId = setInput + weekendCount, idVal = weekendCount });
                    }

                }
                else if (weekendCount == 1)
                {
                    //   createdSet += "" + setInput + weekendCount + "";
                    weekendSets.Add(new WeekendsSet { weekendId = setInput + weekendCount, idVal = weekendCount });
                }
                else
                {
                    weekendSets.Add(new WeekendsSet { weekendId = setInput + weekendCount, idVal = weekendCount });
                    //  createdSet += "," + setInput + weekendCount + "";
                }
                weekendCount++;
            }
            if (setObj.DaySet.Count == 0)
            {
                //  createdSet += " /";
            }
            // Console.WriteLine(createdSet);
            //  return createdSet;
        }

        public int FindCompetences(string employee, string position, Dictionary<string, string> dicEmpGroupInPosition, List<EmployeeObject> employeesObj)
        {
            int competence = 0;
            List<string> temp = new List<string>();
            EmployeeObject currentEmployee = employeesObj.Find(x => x.EmployeeId == employee);

            if (dicEmpGroupInPosition.TryGetValue(position, out string groupId))
            {
                if (currentEmployee.EmployeeGroups.Contains(groupId))
                {
                    competence = 1;
                }
                else
                {
                    competence = 0;
                }
            }
            else
            {
                competence = 0;
            }

            return competence;
        }

        public void CreateDictionaries(List<TimeSheetObject> timeSheetObj, Dictionary<string, string> dicEmpGroupInPosition)
        {
            for (int i = 0; i < timeSheetObj.Count; i++)
            {
                string key = timeSheetObj[i].PositionId;
                if (!dicEmpGroupInPosition.ContainsKey(key))
                {
                    dicEmpGroupInPosition.Add(key, timeSheetObj[i].EmployeeGroupId);
                }
            }
        }

        internal void SplitWeekendSets(List<WeekendsSet> allWeekendSets, List<WeekendsSet> weekendSets, int weekCount)
        {
            for (int i = allWeekendSets.Count; i >= weekCount; i--)
            {
                weekendSets.Add(allWeekendSets[i - 1]);
            }
        }


        internal Tuple<int, int, int> FindWeekendFreq(List<UnionRuleObject> unionRuleObj)
        {
            Tuple<int, int, int> weekendFreq;
            string typeEnum = "6";
            int weekendVal = 0;
            int weekendLength = 0;
            int activate = 0;

            if (unionRuleObj.Exists(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false))
            {
                UnionRuleObject theRule = unionRuleObj.Find(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false);
                //TODO skal testes om nogle portaler burger days eller hvad. Evt bare være sikker på at det kan håndteres
                if (theRule.TimeConditionTypeEnum.Equals("4"))
                {
                    weekendVal = int.Parse(theRule.Value);
                    weekendLength = int.Parse(theRule.TimeConditionValue);
                    activate = 1;
                }
                unionRuleObj.Find(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false).UsedForScalars = true;
            }
            weekendFreq = new Tuple<int, int, int>(weekendVal, weekendLength, activate);

            return weekendFreq;
        }

        internal Tuple<int, int, int> FindTenB(List<UnionRuleObject> unionRuleObj)
        {
            int activate = 0;
            int length = 0;
            int maxDays = 0;
            string typeEnum = "4";
            Tuple<int, int, int> workRuleDays;

            if (unionRuleObj.Exists(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false))
            {
                UnionRuleObject theRule = unionRuleObj.Find(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false);

                //dage
                //TODO SKal nok laves til at håndtere alle tilfælde 
                if (theRule.TimeConditionTypeEnum.Equals("3"))
                {
                    length = int.Parse(theRule.TimeConditionValue);

                    //uger
                }
                else if (theRule.TimeConditionTypeEnum.Equals("4"))
                {
                    int period = Int32.Parse(theRule.TimeConditionValue);
                    period = period * 7;
                    length = period;
                }
                activate = 1;
                maxDays = int.Parse(theRule.Value);
                unionRuleObj.Find(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false).UsedForScalars = true;
            }
            workRuleDays = new Tuple<int, int, int>(activate, length, maxDays);

            return workRuleDays;
        }

        internal Tuple<int, int, int> FindTenA(List<UnionRuleObject> unionRuleObj)
        {
            int activate = 0;
            int length = 0;
            int maxHours = 0;
            string typeEnum = "3";
            Tuple<int, int, int> workRuleHours;

            if (unionRuleObj.Exists(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false))
            {
                UnionRuleObject theRule = unionRuleObj.Find(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false);

                activate = 1;
                //dage
                if (theRule.TimeConditionTypeEnum.Equals("3"))
                {
                    length = int.Parse(theRule.TimeConditionValue);
                    //uger
                }
                else if (theRule.TimeConditionTypeEnum.Equals("4"))
                {
                    int period = Int32.Parse(theRule.TimeConditionValue);
                    period = period * 7;
                    length = period;
                }
                maxHours = int.Parse(theRule.Value);
                unionRuleObj.Find(x => x.TypeEnum.Equals(typeEnum) && x.UsedForScalars == false).UsedForScalars = true;
                workRuleHours = new Tuple<int, int, int>(activate, length, maxHours);
            }
            else
            {
                workRuleHours = new Tuple<int, int, int>(activate, length, maxHours);
            }


            return workRuleHours;
        }


        internal int FindNUmberOfDaysOff(List<UnionRuleObject> unionRuleObj)
        {
            int daysOff = 0;
            int val = 7 - FindMaxConseqDays(unionRuleObj).Item1;
            daysOff = val;
            return daysOff;
        }
        //TODO HER ER NOGET MÆRKELIGT! 
        internal Tuple<int, int> FindMaxWorkDaysPrwk(List<UnionRuleObject> unionRuleObj)
        {

            Tuple<int, int> maxWork;
            int maxWorkVal = 0;
            int activate = 0;
            for (int i = 0; i < unionRuleObj.Count; i++)
            {
                if (unionRuleObj[i].TypeEnum.Equals("55"))
                {
                    maxWorkVal = int.Parse(unionRuleObj[i].Value);
                    activate = 1;
                    break;
                }
            }

            maxWork = new Tuple<int, int>(maxWorkVal, activate);

            return maxWork;
        }

        internal int FindFirstDayOfPeriod(SetObject setObj)
        {
            //int day = tool.FixStartDayVal(setObj.DaySet[0]);
            //int month = tool.FixStartMonth(setObj.DaySet[0]);
            //int year = tool.FixStartYear(setObj.StartYear[0]);

            DateTime dateValue = setObj.TrueDaySet[0];// new DateTime(year, month, day);

            int firstDay = (int)dateValue.DayOfWeek;
            return firstDay;
        }

        internal int FindNumberOfDaysInPeriod(SetObject setObj)
        {
            int daysInPeriod = setObj.DaySet.Count;
            return daysInPeriod;
        }

        internal Tuple<int, int> FindMaxConseqDays(List<UnionRuleObject> unionRuleObj)
        {
            //Title,TypeEnum,Value,TimeConditionValue,TimeConditionTypeEnum
            //Max arbeidsdager p√• rad = 6 dager,5,6,,
            Tuple<int, int> maxWork;
            int maxWorkVal = 0;
            int activate = 0;
            for (int i = 0; i < unionRuleObj.Count; i++)
            {
                if (unionRuleObj[i].TypeEnum.Equals("5"))
                {
                    maxWorkVal = int.Parse(unionRuleObj[i].Value);
                    activate = 1;
                    break;
                }
            }

            maxWork = new Tuple<int, int>(maxWorkVal, activate);

            return maxWork;
        }

        internal Tuple<int, int> FindMaxWorkPrDay(List<UnionRuleObject> unionRuleObj)
        {
            //Title,TypeEnum,Value,TimeConditionValue,TimeConditionTypeEnum
            //max 10 hours per 24 hours,3,10,24,2
            Tuple<int, int> maxWork;

            int activate = 0;
            int maxWorkVal = 0;
            for (int i = 0; i < unionRuleObj.Count; i++)
            {
                if (unionRuleObj[i].TypeEnum.Equals("1"))
                {
                    maxWorkVal = int.Parse(unionRuleObj[i].Value);
                    activate = 1;
                    break;
                }
                else if (unionRuleObj[i].TypeEnum.Equals("3"))
                {
                    if (unionRuleObj[i].TimeConditionValue.Equals("24") && unionRuleObj[i].TimeConditionTypeEnum.Equals("2"))
                    {
                        maxWorkVal = int.Parse(unionRuleObj[i].Value);
                        activate = 1;
                        break;
                    }
                }
            }
            maxWork = new Tuple<int, int>(maxWorkVal, activate);


            return maxWork;
        }

        internal Tuple<int, int> FindMaxWorkPrWk(List<UnionRuleObject> unionRuleObj)
        {

            //max 10 hours per 24 hours,3,10,24,2
            //Max 50 hrs for 7 days, 3, 50, 7, 3
            //Max 48 hours per week, 3, 48, 1, 4
            //Min hours between shifts, 2, 11,,
            Tuple<int, int> maxWork;
            int maxWorkVal = 0;
            int activate = 0;
            for (int i = 0; i < unionRuleObj.Count; i++)
            {
                if (unionRuleObj[i].TypeEnum.Equals("3"))
                {
                    if (unionRuleObj[i].TimeConditionValue.Equals("7") && unionRuleObj[i].TimeConditionTypeEnum.Equals("3"))
                    {
                        maxWorkVal = int.Parse(unionRuleObj[i].Value);
                        activate = 1;
                        break;
                    }
                    else if (unionRuleObj[i].TimeConditionValue.Equals("1") && unionRuleObj[i].TimeConditionTypeEnum.Equals("4"))
                    {
                        maxWorkVal = int.Parse(unionRuleObj[i].Value);
                        activate = 1;
                        break;
                    }
                    else if (unionRuleObj[i].TimeConditionValue.Equals("4") && unionRuleObj[i].TimeConditionTypeEnum.Equals("4"))
                    {
                        int temp = Int32.Parse(unionRuleObj[i].Value);
                        temp = temp / 4;
                        maxWorkVal = temp;
                        activate = 1;
                        break;

                    }
                }

            }
            maxWork = new Tuple<int, int>(maxWorkVal, activate);
            return maxWork;
        }

        internal Tuple<int, int> FindMinBreak(List<UnionRuleObject> unionRuleObj)
        {
            Tuple<int, int> breakk;
            int breakVal = 0;
            int activate = 0;
            for (int i = 0; i < unionRuleObj.Count; i++)
            {
                if (unionRuleObj[i].TypeEnum.Equals("2"))
                {
                    breakVal = int.Parse(unionRuleObj[i].Value);
                    activate = 1;
                    break;
                }
            }
            breakk = new Tuple<int, int>(breakVal, activate);


            return breakk;
        }


        internal int GetPreviousMid(Dictionary<string, int> midParameter, string positionId, DateTime dayId, int v)
        {
            int midVal = 0;

            DateTime newDayId = dayId.AddDays(-v);
            string newDayIdString = "d_" + newDayId.Year + "_" + newDayId.Month + "_" + newDayId.Day;


            string newKey = positionId + newDayIdString;

            if (midParameter.ContainsKey(newKey))
            {
                if (midParameter[newKey] == 1)
                {
                    midVal = 1;
                }
            }

            return midVal;
        }

        internal DaySet GetPreviousDay(List<DaySet> daySets, DaySet dateTrue, int v)
        {
            DaySet daySet = dateTrue;
            DateTime newDayId = dateTrue.DateTimeTrue.AddDays(-v);
            string newDayIdString = "d_" + newDayId.Year + "_" + newDayId.Month + "_" + newDayId.Day;

            foreach (DaySet t in daySets)
            {
                if (t.DayId.Equals(newDayIdString))
                {
                    daySet = t;
                    break;
                }
            }

            return daySet;

        }

        internal double GetPreviousE(Dictionary<string, double> eParameter, string positionId, DaySet t, int v)
        {
            double midVal = 0;

            DateTime newDayId = t.DateTimeTrue.AddDays(-v);
            string newDayIdString = "d_" + newDayId.Year + "_" + newDayId.Month + "_" + newDayId.Day;


            string newKey = positionId + newDayIdString;

            if (eParameter.ContainsKey(newKey))
            {
                midVal = eParameter[newKey];

            }

            return midVal;
        }

        internal double GetPreviousBreakPara(Dictionary<string, double> breakParameter, string positionId, DaySet t, int v1, string v)
        {
            double midVal = 0;

            DateTime newDayId = t.DateTimeTrue.AddDays(-v1);
            string newDayIdString = "d_" + newDayId.Year + "_" + newDayId.Month + "_" + newDayId.Day;


            string newKey = positionId + newDayIdString + v;

            if (breakParameter.ContainsKey(newKey))
            {
                midVal = breakParameter[newKey];

            }

            return midVal;
        }
    }
}
