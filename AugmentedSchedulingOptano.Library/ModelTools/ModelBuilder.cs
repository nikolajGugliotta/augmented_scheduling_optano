﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AugmentedSchedulingOptano.Library.Models;
using AugmentedSchedulingOptano.Library.OptanoSetObjects;
using OPTANO.Modeling.Common;
using OPTANO.Modeling.Optimization;
using OPTANO.Modeling.Optimization.Enums;
using OPTANO.Modeling.Optimization.Solver.Gurobi80;
using OPTANO.Modeling.Optimization.Solver.GLPK;
using OPTANO.Modeling.Optimization.Solver.MipCL141;
using OPTANO.Modeling.Optimization.Solver.Cplex128;
using OPTANO.Modeling.Optimization.Solver.FrontLineXpressSDK2016;
using AugmentedSchedulingOptano.Library.OldTools;
using OPTANO.Modeling.Optimization.Configuration;
using OPTANO.Modeling.Optimization.Solver;
using System.IO;
using System.Diagnostics;

namespace AugmentedSchedulingOptano.Library.ModelTools
{
    public class ModelBuilder
    {
        private Variable zObj { get; set; }
        private VariableCollection<EmployeeSet, PositionSet, DaySet> Xvariables { get; set; }
        private VariableCollection<EmployeeSet, DaySet> WorkDayVariables { get; set; }
        private VariableCollection<PositionSet, DaySet> Svariables { get; set; }
        private VariableCollection<EmployeeSet, DaySet> Yvariables { get; set; }
        private VariableCollection<EmployeeSet, WeekendsSet> Uvariables { get; set; }
        private VariableCollection<EmployeeSet, WeekendsSet> NomHoursPlusL1 { get; set; }
        private VariableCollection<EmployeeSet, WeekendsSet> NomHoursMinusL1 { get; set; }
        private VariableCollection<EmployeeSet, WeekendsSet> NomHoursPlusL2 { get; set; }
        private VariableCollection<EmployeeSet, WeekendsSet> NomHoursMinusL2 { get; set; }


        //  private List<string> empIds = new List<string>();
        // private List<string> posIds = new List<string>();
        //private List<DateTime> startTimes = new List<DateTime>();

        Model model;
        DataObject dataObject;

        private List<EmployeeSet> employeeSets = new List<EmployeeSet>();
        private List<EmployeeGroupSet> employeeGroupSets = new List<EmployeeGroupSet>();
        private List<PositionSet> positionSets = new List<PositionSet>();
        private List<DaySet> daySets = new List<DaySet>();
        private List<DaySet> allDaysSets = new List<DaySet>();
        private List<WeekendsSet> weekendSets = new List<WeekendsSet>();
        private List<WeekendsSet> allWeekendSets = new List<WeekendsSet>();
        private List<BreaksSet> breakPositionsSets = new List<BreaksSet>();

        //husk weekend og break pos


        //Creating the dictionary with key = position  and value = empgroup  
        Dictionary<string, string> dicEmpGroupInPosition = new Dictionary<string, string>();


        private Dictionary<string, int> tempParameter = new Dictionary<string, int>();
        private Dictionary<string, int> pParameter = new Dictionary<string, int>();
        private Dictionary<string, double> bParameter = new Dictionary<string, double>();
        private Dictionary<string, double> eParameter = new Dictionary<string, double>();
        private Dictionary<string, double> durationParameter = new Dictionary<string, double>();
        private Dictionary<string, int> midParameter = new Dictionary<string, int>();
        private Dictionary<string, int> competenceParameter = new Dictionary<string, int>();
        private Dictionary<string, double> contractedHoursParameter = new Dictionary<string, double>();
        private Dictionary<string, int> fix_nomParameter = new Dictionary<string, int>();
        private Dictionary<string, int> requestsParameter = new Dictionary<string, int>();
        private Dictionary<string, double> breakParameter = new Dictionary<string, double>();
        private Dictionary<string, int> shiftUnavailability = new Dictionary<string, int>();
        private Dictionary<string, double> availhoursParameter = new Dictionary<string, double>();
        private Dictionary<string, double> priorityParamter = new Dictionary<string, double>();




        //scalars
        private int hd;//    'Maximum working hours per day' / 0 / 
        private int hw;//    'Maximum working hours pr week' / 0 / 
        private int m;//     'Minimum break between 2 working shifts' / 11 / 
        private int wd;//     'Maximum working days pr week' / 0 / 
        private int lp;//    'Length of a period' / 7 / 
        private int pd;//    'Number of working days in a period' / 5 / 
        private int od;//    'Number of days off in a period' / 1 / 
        private int cd;//    'Length of maximum consecutive working days' /  6  / 
        private int sd;//    'First day in period (1=mon, 2=tue, 30=wed...)' / 1 /  
        private int nrWeOff;//     'Freq. weekends off: Number of weekends off' / 0 /  
        private int periodWeOff;// 'Freq. weekends off: In period of periodWeOff' / 0 /  
        private int daysInPeriod;// 'Number of days in the period considered'  / 27 /  
        private int activate_ten_a;//  '1 to activate const. ten_a, 0 otherwise' /  0 / 
        private int activate_ten_a_2;// '1 to activate const. ten_a2, 0 otherwise' / 0 / 
        private int activate_ten_a_3;// '1 to activate const. ten_a3, 0 otherwise' / 0 / 
        private int activate_ten_b;//  '1 to activate const. ten_b, 0 otherwise' / 0 / 
        private int activate_ten_b_2;// '1 to activate const. ten_b2, 0 otherwise' / 0 / 
        private int activate_ten_b_3;// '1 to activate const. ten_b3, 0 otherwise' / 0 / 
        private int lp_ten_a;//   'Length of range in ten_a' / 0 / 
        private int lp_ten_a_2;// 'Length of range in ten_a_2' / 0 / 
        private int lp_ten_a_3;// 'Length of range in ten_a_3' / 0 / 
        private int lp_ten_b;//   'Length of range in ten_b' / 0 / 
        private int lp_ten_b_2;// 'Length of range in ten_b_2' / 0 / 
        private int lp_ten_b_3;// 'Length of range in ten_b_3' / 0 / 
        private int hd_ten_a;//   'Max hours in range' / 0 / 
        private int hd_ten_a_2;// 'Max hours in range' / 0 / 
        private int hd_ten_a_3;// 'Max hours in range' / 0 / 
        private int pd_ten_b;//   'max working days in range' / 0 / 
        private int pd_ten_b_2;// 'max working days in range' / 0 / 
        private int pd_ten_b_3;// 'max working days in range' / 0 / 
        private int FirstPlanDay;// 'First day of the actual schedule' / 8 / 
        private int FirstPlanWeek;// 'First weekend of the actual schedule' / 2 / 
        private int activate_three;//           '1 to activate const. three, 0 otherwise' / 0 / 
        private int activate_four;//           '1 to activate const. four, 0 otherwise' / 1 / 
        private int activate_seven;//           '1 to activate const. four, 0 otherwise' / 0 / 
        private int activate_eight;//           '1 to activate const. eight, 0 otherwise' / 0 / 
        private int activate_nine;//            '1 to activate const. nine, 0 otherwise' / 1 / 
        private int activate_fourteen_b;//      '1 to activate const. fourteen_b, 0 otherwise' / 0 / 
        private int weekendStart;// 'First day of what is considered weekend (1-7)' / 6 /
        private int weekendEnd;// 'Last day of what is considered weekend (1-7)' / 7 /

        //objective coefficients
        private double alpha;// 'Obj coeff for accepted availability'                 / 0.5 /
        private int beta;//  'Obj coeff for demand fulfillment'                    / 20 /
                         // private int gamma;// 'Obj coeff for salary'                                / 0 /
        private int deltaPlusL1;//'Obj coeff for plus nominal hours level 1'       / 1 /
        private int deltaMinusL1;// 'Obj coeff for minus nominal hours level 1'    / 1 /
        private int deltaPlusL2;//'Obj coeff for plus nominal hours level 2'       / 2 /
        private int deltaMinusL2;// 'Obj coeff for minus nominal hours level 2'    / 2 /
        private int epsilon;// 'Objective coefficient for shift requests'          / 4 /

        private int tol_hours_min;// 'How much a time-constrained worker can work below nom.' / 4 /
        private int tol_hours_max;// 'How much a time-constrained worker can work above nom.' / 4 /

        private int LimitPlusL1;// 'How many hours above norm given level 1 penalty'   /5/
        private int LimitMinusL1;// 'How many hours above norm given level 1 penalty'  /5/


        Tools tool;
        ToolsOld oldTool;

        public void StartOptano(DataObject dataObject, string startDate)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            tool = new Tools();
            oldTool = new ToolsOld();
            this.dataObject = dataObject;

            Console.WriteLine("Initializing data...");
            InitializeData(startDate);

            Console.WriteLine("Building model...");
            BuildModel();

            Console.WriteLine("Solving model...");
            SolveModel();
            sw.Stop();
            Console.WriteLine("Time: " + sw.Elapsed);
        }

        public void BuildModel()
        {

            using (var scope = new ModelScope())
            {
                model = new Model();

                CreateVariables();

                CreateConstraints();

                CreateObjective();
            }
        }
        public ModelStatus ModelStatus { get; }
        public void SolveModel()
        {
            ////object scopeSettings = null;
            // use default settings
            //   var scopeSettings = new ScopeSetting();
            // scopeSettings.ModelElement.EnableFullNames = true;

            using (var solver = new GurobiSolver())
            {
                var solution = solver.Solve(model);
            }

            foreach(EmployeeSet i in employeeSets)
            {
                foreach(PositionSet j in positionSets)
                {
                    foreach(DaySet t in daySets)
                    {
                        if (Xvariables[i, j, t].Value > 0)
                        {
                            //Code for returning the solution to Planday goes here. 
                            Console.WriteLine("x(" + i.EmpId + "," + j.PositionId + "," + t.DayId + ") = " + Xvariables[i, j, t].Value);
                        }
                    }
                }
            }


            ////  ModelStatus status = new ModelStatus();
            ////ModelStatus.GetType();
            ////List<Tuple<EmployeeSet, PositionSet, DaySet>> indiceSol = new List<Tuple<EmployeeSet, PositionSet, DaySet>>();
            //model.VariableCollections.ForEach(vc => vc.SetVariableValues(solution.VariableValues));
            ////      model.VariableCollections.ForEach(vc => Console.WriteLine(vc.ValidIndices.ToString()));

            ////print objective and variable decisions
            //Console.WriteLine($"{solution.ObjectiveValues.Single()}");
            //List<Tuple<double, string>> solutionXs = new List<Tuple<double, string>>();
            //model.Variables.ForEach(x => solutionXs.Add(new Tuple<double, string>(x.Value, x.Name)));
            //foreach (Tuple<double, string> val in solutionXs)
            //{
            //    if (val.Item1 > 0)
            //    {
            //        Console.WriteLine("x: " + val.Item1 + " val: " + val.Item2);
            //    }
            //}
            //   model.VariableStatistics.WriteCSV(AppDomain.CurrentDomain.BaseDirectory);
            // Console.WriteLine("done");

            //    model.Variables.ForEach(x => Console.WriteLine($"{x.ToString().PadRight(36)}: {x.Value}"));
            //    designModel.y.Variables.ForEach(y => Console.WriteLine($"{y.ToString().PadRight(36)}: {y.Value}"));
        }





        private void CreateVariables()
        {
            zObj = new Variable("zObj", 
                double.NegativeInfinity, 
                double.PositiveInfinity, 
                VariableType.Continuous);

            Xvariables = new VariableCollection<EmployeeSet, PositionSet, DaySet>(
                  model,
                  employeeSets,
                  positionSets,
                  daySets,
                  "xVariables",
                  (i, j, t) => $"x_i{i}_j{j}_t{t}",
                  (i, j, t) => 0,
                  (i, j, t) => 1,
                  (i, j, t) => VariableType.Binary);

            WorkDayVariables = new VariableCollection<EmployeeSet, DaySet>(
                model,
                employeeSets,
                daySets,
                "workDayVariables",
                (i, t) => $"workDay_i{i}_t{t}",
                (i, t) => 0,
                (i, t) => 1,
                (i, t) => VariableType.Binary
                );

            Svariables = new VariableCollection<PositionSet, DaySet>(
                model,
                positionSets,
                daySets,
                "sVariables",
                (j, t) => $"s_j{j}_t{t}",
                (j, t) => 0,
                (j, t) => 1,
                (i, t) => VariableType.Binary
                );
            Yvariables = new VariableCollection<EmployeeSet, DaySet>(
                model,
                employeeSets,
                daySets,
                "yVariables",
                (i, t) => $"y_i{i}_t{t}",
                (i, t) => 0,
                (i, t) => 1,
                (i, t) => VariableType.Binary);

            Uvariables = new VariableCollection<EmployeeSet, WeekendsSet>(
                model,
                employeeSets,
                weekendSets,
                "uVariables",
                (i, w) => $"u_i{i}_w{w}",
                (i, w) => 0,
                (i, w) => 1,
                (i, w) => VariableType.Binary);

            NomHoursPlusL1 = new VariableCollection<EmployeeSet, WeekendsSet>(
                model,
                employeeSets,
                weekendSets,
                "NomHoursPlusL1",
                (i, w) => $"h1p_i{i}_w{w}",
                (i, w) => 0,
                (i, w) => LimitPlusL1,
                (i, w) => VariableType.Continuous
                );
            NomHoursMinusL1 = new VariableCollection<EmployeeSet, WeekendsSet>(
                model,
                employeeSets,
                weekendSets,
                "NomHoursMinusL1",
                (i, w) => $"h1m_i{i}_w{w}",
                (i, w) => 0,
                (i, w) => LimitMinusL1,
                (i, w) => VariableType.Continuous
                );
            NomHoursPlusL2 = new VariableCollection<EmployeeSet, WeekendsSet>(
                model,
                employeeSets,
                weekendSets,
                "NomHoursPlusL2",
                (i, w) => $"h2p_i{i}_w{w}",
                (i, w) => 0,
                (i, w) => double.PositiveInfinity,
                (i, w) => VariableType.Continuous
                );
            NomHoursMinusL2 = new VariableCollection<EmployeeSet, WeekendsSet>(
                model,
                employeeSets,
                weekendSets,
                "NomHoursMinusL2",
                (i, w) => $"h2M_i{i}_w{w}",
                (i, w) => 0,
                (i, w) => double.PositiveInfinity,
                (i, w) => VariableType.Continuous
                );
        }

        private void CreateObjective()
        {

            model.AddObjective(new Objective(zObj, "SolutionValue", ObjectiveSense.Minimize));



        }

        private void CreateConstraints()
        {

            //Constraints "forall" (i,j)
            //constraint 1 (zero)
            StringBuilder sb = new StringBuilder();
            foreach (EmployeeSet i in employeeSets)
            {
                foreach (PositionSet j in positionSets)
                {
                    string competenceKey = i.EmpId + j.PositionId;
                    if (competenceParameter.ContainsKey(competenceKey))
                    {
                        if (competenceParameter[competenceKey] == 0)
                        {
                            model.AddConstraint(
                                 Expression.Sum(daySets.Select(t => Xvariables[i, j, t])) == 0);
                        }
                    }
                }
            }
            string path = "C:\\Users\\Gugli\\Desktop\\gamsOutPutFiles\\cSharpOut\\constraintZero.log";
            File.WriteAllText(path, sb.ToString());

            //Constraints "forall" (i,j,t)
            foreach (EmployeeSet i in employeeSets)
            {
                foreach (PositionSet j in positionSets)
                {
                    foreach (DaySet t in daySets)
                    {




                        //constraint 3
                        string pKey = i.EmpId + j.PositionId + t.DayId;
                        if (pParameter.ContainsKey(pKey))
                        {
                            if (pParameter[pKey] == 1)
                            {
                                //    model.AddConstraint(Xvariables[i, j, t] == 1);
                            }

                        }

                        //constraint ? (new version eleven) BUGGED!
                        string shiftKey = i.EmpId + j.PositionId + t.DayId;
                        if (shiftUnavailability.ContainsKey(shiftKey))
                        {
                            if (shiftUnavailability[shiftKey] == 1)
                            {
                                //      model.AddConstraint(Xvariables[i, j, t] == 0);
                            }
                        }

                    }
                }
            }

            //Constraints "forall" (j,t)

            foreach (PositionSet j in positionSets)
            {
                foreach (DaySet t in daySets)
                {
                    //constraint 2 (one_a)
                    string tempKey = j.PositionId + t.DayId;

                    model.AddConstraint(
                        Expression.Sum(employeeSets.Select(i => Xvariables[i, j, t])) <= tempParameter[tempKey]);
                    //if(tempParameter[j.PositionId + t.DayId] > 0)
                    //{
                    //    Console.WriteLine("temp: " + tempParameter[j.PositionId + t.DayId]);
                    //}
                    //constraint ? (thirteen)
                    model.AddConstraint(Svariables[j, t] == tempParameter[j.PositionId + t.DayId] - Expression.Sum(employeeSets.Select(i => Xvariables[i, j, t])));

                }

            }

            //Constraints "forall" (i,t)
            foreach (EmployeeSet i in employeeSets)
            {
                //TODO

                // int counter = 1;
                foreach (DaySet t in daySets)
                {
                    if (activate_seven == 1)
                    {
                        if ((t.DayVal - 1) % 7 == 0)
                        {
                            //constraint 14 (seven)((ikke implementeret helt ordentligt, virker kun for 1 uge)
                            //       Console.WriteLine("constraint ikke implementeret ordentligt");
                            //    model.AddConstraint(Expression.Sum(daySets.Where(tm => t.DayVal <= tm.DayVal && tm.DayVal <= t.DayVal + 6).Select(tm => WorkDayVariables[i, tm])) <= wd);
                        }
                        //    counter++;
                    }


                    if (activate_ten_a == 1)
                    {
                        //constraint 15 (ten_a)
                        //    Console.WriteLine("constraint ikke implementeret");
                    }

                    if (activate_ten_b == 1)
                    {
                        //constraint 16 (ten_b)
                        //     Console.WriteLine("constraint ikke implementeret");
                    }
                    if (activate_nine == 1)
                    {
                        //constraint 17 (nine) (ikke implementeret ordentligt, virker kun for 1 uge
                        //    Console.WriteLine("constraint ikke implementeret ordentligt");
                        //   model.AddConstraint(Expression.Sum(daySets.Select(tm => Yvariables[i, tm])) <= cd);
                    }

                    //constraint (two)
                    model.AddConstraint(Expression.Sum(positionSets.Select(j => Xvariables[i, j, t])) <= 8 * WorkDayVariables[i, t]);




                }
            }

            //Constraints "forall" (i,j,k,t)
            foreach (EmployeeSet i in employeeSets)
            {
                foreach (PositionSet j in positionSets)
                {
                    foreach (PositionSet k in positionSets)
                    {
                        for (int t = 0; t < daySets.Count; t++)
                        {
                            string jKey = j.PositionId + daySets[t].DayId;
                            string kKey = k.PositionId + daySets[t].DayId;

                            if (bParameter.ContainsKey(jKey) && bParameter.ContainsKey(kKey) && midParameter.ContainsKey(jKey) && midParameter.ContainsKey(kKey))
                            {
                                double bjt = bParameter[jKey];
                                double bkt = bParameter[kKey];
                                double ejt = eParameter[jKey];
                                double ekt = eParameter[kKey];

                                int mid_jt = midParameter[jKey];
                                int mid_kt = midParameter[kKey];

                                DaySet tm1 = daySets[t];
                                //  bool usingTm1 = false;
                                bool midM1 = false;
                                int mid_jt_new = 0;
                                double ekt_new = 0.0;
                                double ejt_new = 0.0;
                                string newMidKeyj, newMidKeyk;
                                if (t > 0)
                                {
                                    tm1 = daySets[t - 1];
                                    //  usingTm1 = true;
                                    newMidKeyj = j.PositionId + tm1.DayId;
                                    newMidKeyk = k.PositionId + tm1.DayId;
                                    if (midParameter.ContainsKey(newMidKeyj))
                                    {
                                        midM1 = true;
                                        mid_jt_new = midParameter[newMidKeyj];

                                    }
                                    if (eParameter.ContainsKey(newMidKeyk))
                                    {
                                        ekt_new = eParameter[newMidKeyk];
                                    }
                                    if (eParameter.ContainsKey(newMidKeyj))
                                    {
                                        ejt_new = eParameter[newMidKeyj];
                                    }
                                }

                                //constraint4 (six)
                                if (j != k)
                                {
                                    if ((bjt <= bkt && bkt < ejt))
                                    {
                                        model.AddConstraint(Xvariables[i, j, daySets[t]] + Xvariables[i, k, daySets[t]] <= 1);
                                    }
                                    if (mid_jt == 1 && mid_kt == 0 && bjt < ekt)
                                    {
                                        model.AddConstraint(Xvariables[i, j, daySets[t]] + Xvariables[i, k, daySets[t]] <= 1);
                                    }
                                    if (midM1 && mid_jt_new == 1 && bkt < ejt_new)
                                    {
                                        model.AddConstraint(Xvariables[i, j, tm1] + Xvariables[i, k, daySets[t]] <= 1);
                                    }
                                    if (mid_jt == 1 && mid_kt == 1)
                                    {
                                        model.AddConstraint(Xvariables[i, j, daySets[t]] + Xvariables[i, k, daySets[t]] <= 1);
                                    }
                                }

                                //constraint 13 (four)
                                if (activate_four == 1 && j != k)
                                {
                                    if (mid_jt == 0 && mid_kt == 0 && ((ejt - bkt < m) || (ekt - bjt < m)))
                                    {
                                        //     model.AddConstraint(Xvariables[i, j, daySets[t]] + Xvariables[i, k, daySets[t]] <= 1);
                                    }
                                    if (midM1 && mid_jt == 1 && mid_kt == 0 && bjt - ekt < m)
                                    {
                                        if (t > 0)
                                        {
                                            //      model.AddConstraint(Xvariables[i, j, daySets[t]] + Xvariables[i, k, daySets[t]] <= 1);
                                        }
                                    }
                                    if (midM1 && mid_jt_new == 1 && bkt - ejt_new < m)
                                    {
                                        if (t > 0)
                                        {
                                            //     model.AddConstraint(Xvariables[i, j, daySets[t - 1]] + Xvariables[i, k, daySets[t]] <= 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //"forall" (i,w)
            foreach (EmployeeSet i in employeeSets)
            {
                foreach (WeekendsSet w in weekendSets)
                {
                    //Constraint 5 (fifteen_a_plus)
                    //model.AddConstraint(NomHoursPlusL1[i, w] + NomHoursPlusL2[i, w] >=
                    //   Expression.Sum(positionSets.SelectMany(j => daySets.Where(t => t.DayVal > (w.idVal - 1) * 7 && t.DayVal <= w.idVal * 7).Select(t => (durationParameter[j.PositionId + t.DayId] -
                    //            Expression.Sum(breakPositionsSets.Select(bp => breakParameter[j.PositionId + t.DayId + bp.BreakPosId]))) * Xvariables[i, j, t]))) - contractedHoursParameter[i.EmpId]

                    //   );

                    //  fifteen_a_plus(i, w_e)$(ord(w_e) >= FirstPlanWeek)..
                    //nomHoursPlusL1(i, w_e) + nomHoursPlusL2(i, w_e) = g =
                    //        // Middle of period
                    //        sum((j, t)$(ord(t) > (ord(w_e) - 1) * 7 and ord(t) <= ord(w_e) * 7),
                    //       (duration(j, t) - sum(b_p, break_duration(j, t, b_p))) * x(i, j, t) )
                    //       -nominalHours(i);



                    //constraint 6 (fifteen_a_minus)
                    //model.AddConstraint(NomHoursMinusL1[i, w] + NomHoursMinusL2[i, w] >= contractedHoursParameter[i.EmpId] -
                    //    Expression.Sum(positionSets.SelectMany(j => daySets.Where(t => t.DayVal > (w.idVal - 1) * 7 && t.DayVal <= w.idVal * 7).Select(t => (durationParameter[j.PositionId + t.DayId] -
                    //             Expression.Sum(breakPositionsSets.Select(bp => breakParameter[j.PositionId + t.DayId + bp.BreakPosId]))) * Xvariables[i, j, t]))) 

                    //    );

                    //      fifteen_a_minus(i, w_e)$(ord(w_e) >= FirstPlanWeek)..
                    //nomHoursMinusL1(i, w_e) + nomHoursMinusL2(i, w_e) = g =
                    //        nominalHours(i)
                    //- (
                    //       // Middle of period
                    //       sum((j, t)$(ord(t) > (ord(w_e) - 1) * 7 and ord(t) <= ord(w_e) * 7),
                    //        (duration(j, t) - sum(b_p, break_duration(j, t, b_p))) * x(i, j, t))

                    //     );







                    //Constraint 7
                    //  model.AddConstraint(NomHoursPlusL1[i, w] <= LimitPlusL1);

                    //Constraint 8
                    //   model.AddConstraint(NomHoursMinusL1[i, w] <= LimitMinusL1);


                    string empKey = i.EmpId;
                    if (fix_nomParameter.ContainsKey(empKey))
                    {
                        if (fix_nomParameter[empKey] == 1)
                        {
                            //constraint 9 (sixteen_min)
                            //  model.AddConstraint(NomHoursMinusL1[i, w] + NomHoursMinusL2[i, w] <= tol_hours_min);
                            //constraint 10 (sixteen_max)
                            //  model.AddConstraint(NomHoursPlusL1[i, w] + NomHoursPlusL2[i, w] <= tol_hours_max);
                        }
                    }
                    //constraint 11 (fourteen_a)
                    // model.AddConstraint(7 * Uvariables[i, w] >= Expression.Sum(positionSets.SelectMany(j => daySets.Where(t => ((w.idVal - 1) * 7 + weekendStart < t.DayVal && t.DayVal <= (w.idVal - 1) * 7 + weekendEnd)).Select(t => Xvariables[i, j, t]))));


                    // model.AddConstraint(Expression.Sum(daySets.Where(t => (w.idVal - 1) * 7 + weekendStart == (w.idVal) * 7 + weekendEnd).Select(t =>
                    //   Yvariables[i, t])) <= 7 * Uvariables[i, w]);


                    //constraint 18
                    if (activate_fourteen_b == 1)
                    {
                        Console.WriteLine("constraint ikke implementeret");
                    }

                    //constraint ? (eight)
                    if (activate_eight == 1)
                    {
                        Console.WriteLine("constraint ikke implementeret");
                    }
                }
            }

            //forall (i,t)
            foreach (EmployeeSet i in employeeSets)
            {
                foreach (DaySet t in daySets)
                {

                    //constraint 12 (three)
                    if (activate_three == 1)
                    {
                        //     model.AddConstraint(Expression.Sum(positionSets.Select(j => (1 - midParameter[j.PositionId + t.DayId]) * Xvariables[i, j, t] * (durationParameter[j.PositionId + t.DayId] - breakParameter[j.PositionId + t.DayId + "1"])
                        //+ midParameter[j.PositionId + t.DayId] * Xvariables[i, j, t] * (24 - bParameter[j.PositionId + t.DayId] - breakParameter[j.PositionId + t.DayId + "1"])
                        //+ tool.GetPreviousMid(midParameter, j.PositionId, t.DateTimeTrue, 1) * Xvariables[i, j, tool.GetPreviousDay(daySets, t, 1)] * (tool.GetPreviousE(eParameter, j.PositionId, t, 1) - tool.GetPreviousBreakPara(breakParameter, j.PositionId, t, 1, "2")))) <= hd);
                    }

                }
            }

            //zObj

            model.AddConstraint(zObj ==
                 -alpha * Expression.Sum(
                    employeeSets.SelectMany(i =>
                    positionSets.SelectMany(j =>
                    daySets.Select(t =>
                    availhoursParameter[i.EmpId + j.PositionId + t.DayId] * Xvariables[i, j, t])))
                    )
                 + beta * Expression.Sum(
                     positionSets.SelectMany(j =>
                     daySets.Select(t =>
                     Svariables[j, t]))
                     )
                 - epsilon * Expression.Sum(
                     employeeSets.SelectMany(i =>
                     positionSets.SelectMany(j =>
                     daySets.Select(t =>
                     requestsParameter[i.EmpId + j.PositionId + t.DayId] * Xvariables[i, j, t])))
                     )
              );


            //     + deltaPlusL1 * Expression.Sum(employeeSets.Select(i => priorityParamter[i.EmpId] * Expression.Sum(weekendSets.Select(w => NomHoursPlusL1[i, w]))))
            //   + deltaPlusL2 * Expression.Sum(employeeSets.Select(i => priorityParamter[i.EmpId] * Expression.Sum(weekendSets.Select(w => NomHoursPlusL2[i, w]))))
            //  + deltaMinusL1 * Expression.Sum(employeeSets.Select(i => priorityParamter[i.EmpId] * Expression.Sum(weekendSets.Select(w => NomHoursMinusL1[i, w]))))
            // + deltaMinusL2 * Expression.Sum(employeeSets.Select(i => priorityParamter[i.EmpId] * Expression.Sum(weekendSets.Select(w => NomHoursMinusL2[i, w]))))

            // - epsilon * sum((i,j,t)$(ord(t) >= FirstPlanDay), shiftRequests(i,j,t)*x(i,j,t))


        }

        internal void InitializeData(string startdate)
        {
            InitializeScalars();
            InitializeObjectiveCoefficients();
            tool.CreateWeekendSet(dataObject.SetObjects, allWeekendSets);
            breakPositionsSets.Add(new BreaksSet { BreakPosId = "1" });
            breakPositionsSets.Add(new BreaksSet { BreakPosId = "2" });




            DateTime startTime = DateTime.Parse(startdate);

            tool.SplitWeekendSets(allWeekendSets, weekendSets, dataObject.WeekCount);
            int dayCounter = 1;
            foreach (TimeSheetObject sheet in dataObject.TimesheetObjects)
            {
                string posId = sheet.PositionId;
                // DateTime dayId = sheet.TrueStartTime;

                PositionSet posSetTest = positionSets.Find(x => x.PositionId.Equals(posId));
                if (posSetTest == null)
                {
                    positionSets.Add(new PositionSet { PositionId = posId });
                    //   Console.WriteLine("posId: " + posId);
                }

                bool startDateInRange = false;
                if (sheet.TrueStartTime >= startTime)
                {
                    startDateInRange = true;
                }

                string dayId = sheet.StartDay;//"d_" + sheet.TrueStartTime.Year + "_" + sheet.TrueStartTime.Month + "_" + sheet.TrueStartTime.Day;
                //  posIds.Add(sheet.PositionId);

                //  startTimes.Add(sheet.TrueStartTime);
                DaySet daySetTest = daySets.Find(x => x.DayId.Equals(dayId));
                if (daySetTest == null && startDateInRange)
                {
                    daySets.Add(new DaySet { DayId = dayId, DateTimeTrue = sheet.TrueStartTime, DayVal = dayCounter });
                    dayCounter++;

                }
                else if (daySetTest == null)
                {
                    allDaysSets.Add(new DaySet { DayId = dayId, DateTimeTrue = sheet.TrueStartTime });
                }


                string tempKey = posId + dayId;
                if (!tempParameter.ContainsKey(tempKey) && startDateInRange)
                {
                    tempParameter.Add(tempKey, 1);
                }

                if (!bParameter.ContainsKey(tempKey) && startDateInRange)
                {
                    bParameter.Add(tempKey, Convert.ToDouble(TimeSpan.Parse(sheet.TrueStartTime.Hour + ":" + sheet.TrueStartTime.Minute).TotalHours));
                }

                if (!eParameter.ContainsKey(tempKey) && startDateInRange)
                {
                    eParameter.Add(tempKey, Convert.ToDouble(TimeSpan.Parse(sheet.TrueEndTime.Hour + ":" + sheet.TrueEndTime.Minute).TotalHours));
                }

                if (!midParameter.ContainsKey(tempKey) && startDateInRange)
                {
                    int overlap = oldTool.CalculateOverlap(sheet);
                    midParameter.Add(tempKey, overlap);
                }

                if (!durationParameter.ContainsKey(tempKey) && startDateInRange)
                {
                    durationParameter.Add(tempKey, sheet.Duration);
                }

                //Console.WriteLine("sheetID: " + sheet.SheetId + " requestcount: " + sheet.Requests.Count);
                if (sheet.Requests.Count > 0)
                {
                    for (int i = 0; i < sheet.Requests.Count; i++)
                    {
                        string requestKey = sheet.Requests[i].EmployeeId + posId + dayId;
                        Console.WriteLine("reqID: " + requestKey);
                        if (!requestsParameter.ContainsKey(requestKey) && startDateInRange)
                        {
                            requestsParameter.Add(requestKey, 1);
                        }
                    }

                }

                if (!sheet.EmployeeId.Equals("Open Shift"))
                {
                    string pKey = sheet.EmployeeId + posId + dayId;
                    if (!pParameter.ContainsKey(pKey))
                    {
                        pParameter.Add(pKey, 1);
                    }
                }
                //    StringBuilder sb = new StringBuilder();
                foreach (EmployeeObject emp in dataObject.EmployeeObjects)
                {
                    string availKey = emp.EmployeeId + posId + dayId;
                    foreach (AvailabilityObject unAvail in emp.Availabilities)
                    {
                        if (sheet.TrueStartTime < unAvail.TrueEndTime && unAvail.TrueStartTime < sheet.TrueEndTime)
                        {
                            if (!shiftUnavailability.ContainsKey(availKey) && startDateInRange)
                            {
                                shiftUnavailability.Add(availKey, 1);
                            }
                        }
                    }

                    foreach (VacationObject vaca in emp.Vacations)
                    {
                        if (sheet.TrueStartTime.Date == vaca.VacaDateTrue.Date && vaca.Status.Equals("3"))
                        {
                            if (!shiftUnavailability.ContainsKey(availKey) && startDateInRange)
                            {
                                shiftUnavailability.Add(availKey, 1);
                            }
                        }
                    }
                }

            }


            tool.CreateDictionaries(dataObject.TimesheetObjects, dicEmpGroupInPosition);
            StringBuilder sbe = new StringBuilder();
            foreach (EmployeeObject emp in dataObject.EmployeeObjects)
            {
                sbe.Append("empid: " + emp.EmployeeId + "\n");
                foreach (PositionSet pos in positionSets)
                {
                    string key = emp.EmployeeId + pos.PositionId;
                    int value = tool.FindCompetences(emp.EmployeeId, pos.PositionId, dicEmpGroupInPosition, dataObject.EmployeeObjects);
                    if (!competenceParameter.ContainsKey(key))
                    { //create competence parameter
                        competenceParameter.Add(key, value);
                    }

                    foreach (DaySet t in daySets)
                    {
                        string availKey = emp.EmployeeId + pos.PositionId + t.DayId;
                        //double hours = oldTool.GetAvailabilityOverlap(emp.Availabilities, sheet);

                        //if (!availhoursParameter.ContainsKey(availKey))
                        //{
                        //    availhoursParameter.Add(availKey, hours);
                        //}
                        if (!requestsParameter.ContainsKey(availKey))
                        {
                            requestsParameter.Add(availKey, 0);
                        }
                    }

                }

                string empKey = emp.EmployeeId;
                if (!priorityParamter.ContainsKey(empKey))
                {
                    priorityParamter.Add(empKey, emp.Priority);
                }
                if (!fix_nomParameter.ContainsKey(empKey))
                {
                    //TODO hardcoded
                    fix_nomParameter.Add(empKey, 0);
                }
            }
            string path = "C:\\Users\\Gugli\\Desktop\\gamsOutPutFiles\\cSharpOut\\empIds.log";
            File.WriteAllText(path, sbe.ToString());
            foreach (PositionSet pos in positionSets)
            {
                foreach (DaySet day in daySets)
                {
                    string key = pos.PositionId + day.DayId;
                    if (!tempParameter.ContainsKey(key))
                    {
                        tempParameter.Add(key, 0);
                    }

                    foreach (BreaksSet k in breakPositionsSets)
                    {
                        string breakKey = pos.PositionId + day.DayId + k.BreakPosId;
                        if (!breakParameter.ContainsKey(breakKey))
                        {
                            //TODO hardcoded
                            breakParameter.Add(breakKey, 0.0);
                        }
                    }
                    if (!durationParameter.ContainsKey(key))
                    {
                        durationParameter.Add(key, 0.0);
                    }
                }
            }

            foreach (EmployeeObject emp in dataObject.EmployeeObjects)
            {
                // empIds.Add(emp.EmployeeId);
                employeeSets.Add(new EmployeeSet { EmpId = emp.EmployeeId });
                foreach (string empGroup in emp.EmployeeGroups)
                {
                    EmployeeGroupSet egs = employeeGroupSets.Find(x => x.EmployeeGroupId.Equals(empGroup));
                    if (egs == null)
                    {
                        employeeGroupSets.Add(new EmployeeGroupSet { EmployeeGroupId = empGroup });
                    }
                }

                if (!contractedHoursParameter.ContainsKey(emp.EmployeeId))
                {
                    contractedHoursParameter.Add(emp.EmployeeId, emp.ContractedHours);
                }
            }

            allDaysSets.AddRange(daySets);

            StringBuilder sb = new StringBuilder();

            foreach (TimeSheetObject sheet in dataObject.TimesheetObjects)
            {
                foreach (EmployeeObject i in dataObject.EmployeeObjects)
                {

                    //foreach (PositionSet j in positionSets)
                    //{
                    //    foreach (DaySet t in daySets)
                    //    {
                    string availKey = i.EmployeeId + sheet.PositionId + sheet.StartDay;

                    //  EmployeeObject emp = dataObject.EmployeeObjects.Find(x => x.EmployeeId.Equals(i.EmpId));
                    double hours = oldTool.GetAvailabilityOverlap(i.Availabilities, sheet);


                    if (!availhoursParameter.ContainsKey(availKey))
                    {
                        // Console.WriteLine("inde. Hours: "+hours);
                        if (hours > 0)
                        {
                            //  Console.WriteLine("hours: " + hours);
                            availhoursParameter.Add(availKey, hours);
                            sb.Append("key: " + availKey + " hours: " + hours + "\n");
                        }

                    }
                    //    }
                    //}
                }
            }
            path = "C:\\Users\\Gugli\\Desktop\\gamsOutPutFiles\\cSharpOut\\avilHours.log";
            File.WriteAllText(path, sb.ToString());

            foreach (EmployeeSet i in employeeSets)
            {
                foreach (PositionSet j in positionSets)
                {
                    foreach (DaySet t in daySets)
                    {
                        string availKey = i.EmpId + j.PositionId + t.DayId;

                        //  EmployeeObject emp = dataObject.EmployeeObjects.Find(x => x.EmployeeId.Equals(i.EmpId));
                        if (!availhoursParameter.ContainsKey(availKey))
                        {
                            availhoursParameter.Add(availKey, 0);
                        }
                    }
                }
            }
        }

        internal void InitializeObjectiveCoefficients()
        {
            alpha = 0.5;
            beta = 20;
            // gamma = 0;
            deltaPlusL1 = 1;
            deltaMinusL1 = 1;
            deltaPlusL2 = 2;
            deltaMinusL2 = 2;
            epsilon = 4;

            tol_hours_min = 4;
            tol_hours_max = 4;
            LimitPlusL1 = 5;
            LimitMinusL1 = 5;
        }

        internal void InitializeScalars()
        {
            List<UnionRuleObject> unionRuleObject = dataObject.UnionRuleObjects;
            SetObject setObjects = dataObject.SetObjects;

            Tuple<int, int, int> tenA = tool.FindTenA(unionRuleObject);
            Tuple<int, int, int> tenA2 = tool.FindTenA(unionRuleObject);
            Tuple<int, int, int> tenA3 = tool.FindTenA(unionRuleObject);

            Tuple<int, int, int> tenB = tool.FindTenB(unionRuleObject);
            Tuple<int, int, int> tenB2 = tool.FindTenB(unionRuleObject);
            Tuple<int, int, int> tenB3 = tool.FindTenB(unionRuleObject);

            Tuple<int, int, int> weekendFreq = tool.FindWeekendFreq(unionRuleObject);

            Tuple<int, int> maxWorkPrDay = tool.FindMaxWorkPrDay(unionRuleObject);
            Tuple<int, int> breakBetweenShifts = tool.FindMinBreak(unionRuleObject);
            Tuple<int, int> workDaysPrWeek = tool.FindMaxWorkDaysPrwk(unionRuleObject);
            Tuple<int, int> maxWorkHoursPrWeek = tool.FindMaxWorkPrWk(unionRuleObject);
            Tuple<int, int> maxConsecWorkDays = tool.FindMaxConseqDays(unionRuleObject);

            hd = maxWorkPrDay.Item1;
            hw = maxWorkHoursPrWeek.Item1;
            m = breakBetweenShifts.Item1;
            wd = workDaysPrWeek.Item1;
            lp = 7;
            pd = 5;
            od = tool.FindNUmberOfDaysOff(unionRuleObject);
            // Console.WriteLine("od: " + od);
            cd = maxConsecWorkDays.Item1;
            sd = tool.FindFirstDayOfPeriod(setObjects);
            /// Console.WriteLine("sd: " + sd);
            nrWeOff = weekendFreq.Item1;
            periodWeOff = weekendFreq.Item2;

            daysInPeriod = tool.FindNumberOfDaysInPeriod(setObjects);
            // Console.WriteLine("daysInPeriod: " + daysInPeriod);
            activate_ten_a = tenA.Item1;
            activate_ten_a_2 = tenA2.Item1;
            activate_ten_a_3 = tenA3.Item1;

            activate_ten_b = tenB.Item1;
            activate_ten_b_2 = tenB2.Item1;
            activate_ten_b_3 = tenB3.Item1;


            lp_ten_a = tenA.Item2;
            lp_ten_a_2 = tenA2.Item2;
            lp_ten_a_3 = tenA3.Item2;

            lp_ten_b = tenB.Item2;
            lp_ten_b_2 = tenB2.Item2;
            lp_ten_b_3 = tenB3.Item2;


            hd_ten_a = tenA.Item3;
            hd_ten_a_2 = tenA2.Item3;
            hd_ten_a_3 = tenA3.Item3;

            pd_ten_b = tenB.Item3;
            pd_ten_b_2 = tenB2.Item3;
            pd_ten_b_3 = tenB3.Item3;

            FirstPlanDay = int.Parse(dataObject.FirstPlanDay);
            // Console.WriteLine("FirstPlanDay: " + FirstPlanDay);
            FirstPlanWeek = int.Parse(dataObject.FirstPlanWeek);
            // Console.WriteLine("FirstPlanWeek: " + FirstPlanWeek);


            activate_three = maxWorkPrDay.Item2;
            activate_four = breakBetweenShifts.Item2;
            activate_seven = workDaysPrWeek.Item2;
            activate_eight = maxWorkHoursPrWeek.Item2;
            activate_nine = maxConsecWorkDays.Item2;
            activate_fourteen_b = weekendFreq.Item3;

            //TODO find ud af hvordan first day of weekend er fundet. 
            weekendStart = 6;
            weekendEnd = 7;
        }
    }
}
